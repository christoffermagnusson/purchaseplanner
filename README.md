# README #

PurchasePlanner or Plnner is an application which allows the user to take notes of anything and share it with other users. The original use case was to share a grocery list between 2 persons, but
the application could also be used to share a ToDo list or a wishlist. Plnner uses AWS services as its "back-end". Cognito User Pools and Federated Identities are used to manage users and their access for other services on AWS. 
DynamoDB is used as a distributed database to enable the sharing of lists between the users. 



Screenshots from Plnner. Left is the login screen and the right is the overview screen where all lists are gathered


![Scheme](images/splash_min.png)
![Scheme](images/overview_min.png)




Version 4.0 ------------ 27/01/18

DynamoDB sharing solution fully implemented. App now works as I first intended. The UI has been getting alot of updates as well.
Quality of life features such as being able to add a user to an existing list is also implemented. Been using Java 8 features such as 
lambda and streams. Builder pattern are now used for object creation.

Next steps --------------
* Continue polishing UI



package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;

import com.magnusson_dev.purchaseplanner.ActivityType;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.BaseList;

import org.json.JSONObject;
import org.junit.Test;

import java.io.File;
import java.util.List;
import android.test.mock.MockContext;
import android.content.Context;

import static org.junit.Assert.*;


public class JSONParserImplTest {

    private class MockCallback implements ICallback{

        @Override
        public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {

        }

        @Override
        public Context onRefreshContext() {
            return null;
        }
    }
    private final JSONService jsonService = JSONServiceFactory.getJSONServiceInstance(new MockCallback());
    private final JSONParserImpl jsonParserImpl = new JSONParserImpl();
    private List<BaseList> list;

    Context context = new MockContext();

    // "/data/user/0/com.magnusson_dev.purchaseplanner/files/JSONTest4/json_test_19.json"
    File rootFile;
    File fileToManage;
    String jsonTestStr = "{lists:[{\"category\":\"SHOPPING\",\"categoryImageRef\":20,\"tag\":\"list_3\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":21,\"tag\":\"list_4\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"},{\"category\":\"SHOPPING\",\"categoryImageRef\":22,\"tag\":\"list_5\"}]}".replaceAll("\\\\","");
    String jsonBasic = "{lists:[{\"category\":\"SHOPPING\",\"categoryImageRef\":20,\"tag\":\"list_3\"}]}";


    public void setUp() throws Exception{
        rootFile = new File(context.getFilesDir(), "JSONTest4");
        fileToManage = new File(rootFile, "json_test_19.json");
    }

    @Test
    public void parseTotalLists_nullReturnsEmptyArray() throws Exception {
        list = jsonService.parseTotalLists(null);
        System.out.println(list.isEmpty());
        assertTrue(list.isEmpty());
    }

    @Test
    public void parseTotalLists_valid() throws Exception{

        JSONObject jsonObject = new JSONObject(jsonBasic);
        System.out.println("Contents of jsonObject = "+jsonObject);
        list = jsonParserImpl.parseTotalLists(jsonTestStr);
        System.out.println(list);
        assertFalse(list.isEmpty());

    }

    @Test
    public void parseListByTag() throws Exception {

    }

}
package com.magnusson_dev.purchaseplanner.Handlers;

import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerImpl;


public class ShoppingListHandlerTest {
    private static final String TAG = "ShoppingListHandlerTest";
    private ListHandlerImpl handler;



  /*  @Test(expected = StorageFetchException.class)
    public void getListByTagTest_emptyStr() throws Exception {
        ListHandlerImpl.getListByTag("");
    }
    @Test(expected = StorageFetchException.class)
    public void getListByTagTest_nullArg() throws Exception{
        ListHandlerImpl.getListByTag(null);
    }
    @Test(expected = StorageFetchException.class)
    public void getListByTagTest_nonExistant() throws Exception{
        ListHandlerImpl.addListToStorage(new ShoppingList("tag1"));
        ListHandlerImpl.addListToStorage(new ShoppingList("tag2"));
        ListHandlerImpl.getListByTag("tag3");
    }
    @Test
    public void getListByTagTest_existing() throws Exception{
        ShoppingList shoppingList = new ShoppingList("tag4");
        ListHandlerImpl.addListToStorage(shoppingList);
        ListHandlerImpl.addListToStorage(new ShoppingList("tag5"));
        Assert.assertEquals(ListHandlerImpl.getListByTag("tag4"),shoppingList);
    }

    @Test(expected = StorageEditException.class)
    public void addListToStorageTest_nonValid() throws Exception {
        ListHandlerImpl.addListToStorage(null);
    }
    @Test
    public void addListToStorageTest_valid() throws Exception{
        Assert.assertEquals(true,ListHandlerImpl.addListToStorage(new ShoppingList("tag6")));

    }
    @Test(expected = StorageEditException.class)
    public void addListToStorageTest_duplicate() throws Exception{
        ListHandlerImpl.addListToStorage(new ShoppingList("tag7"));
        ListHandlerImpl.addListToStorage(new ShoppingList("tag7"));
    }
    @Test(expected = StorageEditException.class)
    public void editListToStorage_null()throws Exception{
        ListHandlerImpl.addListToStorage(new ShoppingList("tag8"));
        ListHandlerImpl.editListToStorage(null);
    }
    @Test
    public void editListToStorage_valid() throws Exception{
        ShoppingList list = new ShoppingList("tag9");
        List<ListItem> listOfItems = new ArrayList<>();
        listOfItems.add(new ShoppingListItem("Fish"));
        list.setListItems(listOfItems);
        ListHandlerImpl.addListToStorage(list);
        list.getListItems().add(new ShoppingListItem("Potatoes"));
        Assert.assertEquals(true,ListHandlerImpl.editListToStorage(list));
    }
    @Test(expected = StorageEditException.class)
    public void editListToStorage_nullList()throws Exception{
        ShoppingList list = new ShoppingList("tag10");
        ListHandlerImpl.addListToStorage(list);
        ListHandlerImpl.editListToStorage(list);
    }

    @Test
    public void deleteListInStorage_valid()throws Exception{
        ShoppingList list = new ShoppingList("tag11");
        ListHandlerImpl.addListToStorage(list);
        Assert.assertTrue(ListHandlerImpl.deleteListInStorage(list));
    }

    @Test
    public void deleteListInStorage_null()throws Exception{
        Assert.assertFalse(ListHandlerImpl.deleteListInStorage(null));
    }

    @Test(expected = StorageEditException.class)
    public void deleteListInStorage_nonExisting()throws Exception{
        ShoppingList nonExisting = new ShoppingList("nonExistant");
        Assert.assertFalse(ListHandlerImpl.deleteListInStorage(nonExisting));
    }
*/
}
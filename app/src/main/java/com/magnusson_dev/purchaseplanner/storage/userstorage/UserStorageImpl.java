package com.magnusson_dev.purchaseplanner.storage.userstorage;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConnection;
import com.magnusson_dev.purchaseplanner.storage.dbutils.QueryBuilder;
import com.magnusson_dev.purchaseplanner.storage.orm.CachedUserORM;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of UserStorage interface using SQLite
 */
public class UserStorageImpl implements UserStorage {

    private static final String TAG = "UserStorageImpl";
    private static UserStorageImpl instance = null;

    SQLiteDatabase db;
    Cursor cachedUserCursor;
    DBConnection dbHelper;

    private UserStorageImpl(Context context){
        dbHelper = DBConnection.getInstance(context);
        //db = dbHelper.getWritableDatabase();
        setDB();
        Log.d(TAG, "UserStorageImpl: db status = "+db.isOpen());
    }

    static UserStorageImpl getInstance(Context context){
        if(instance==null){
            instance = new UserStorageImpl(context);
        }
        return instance;
    }

    private void setDB(){
        db = dbHelper.getDatabase();
    }

    @Override
    public List<User> getAllCachedUsers() throws StorageFetchException {
        setDB();
        cachedUserCursor = db.rawQuery(QueryBuilder.getAllCachedUsers(),
                null);
        return buildUserList();
    }

    @Override
    public List<User> getAllCachedUsersByName(String filter) throws StorageFetchException {
        setDB();
        cachedUserCursor = db.rawQuery(QueryBuilder.getAllCachedUsersByName(filter),
                null);
        return buildUserList();
    }

    @Override
    public void storeUser(User user) throws StorageEditException {
        setDB();
        if(validateExisting(user.getUsername())){
           // Edit user instead..
            updateUser(user);
        }else{
            long id = db.insert(CachedUserORM.TABLE_NAME,
                    null,
                    QueryBuilder.addCachedUserValues(user));
        }
    }

    @Override
    public void updateUser(User user) throws StorageEditException {
        setDB();
        if(user==null){
            throw new StorageEditException("User cannot be NULL");
        }else{
            db.update(CachedUserORM.TABLE_NAME,
                    QueryBuilder.addCachedUserValues(user),
                    "username='"+user.getUsername()+"'",
                    null);
        }

    }

    @Override
    public User getUserByName(String username) throws StorageFetchException {
        setDB();
        if(username==null){
            throw new StorageFetchException("Input cannot be NULL");
        }else if(username.equalsIgnoreCase("")){
            throw new StorageFetchException("Empty input not allowed");
        }

        cachedUserCursor = db.query(CachedUserORM.TABLE_NAME,
                QueryBuilder.getCachedUserByNameProjection(),
                QueryBuilder.getCachedUserByNameSelection(),
                QueryBuilder.getCachedUserByNameSelectionArgs(username),
                null,
                null,
                null);
        User user = buildUser();
        cachedUserCursor.close();
        return user;

    }

    /**
     * Builds a list of User objects from the current point in
     * cachedUserCursor.
     * @return
     */
    private List<User> buildUserList(){
        List<User> users = new ArrayList<>();
        int usernameIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_USERNAME);
        int givenNameIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_GIVENNAME);
        int emailIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_EMAIL);

        while(cachedUserCursor.moveToNext()){
            String username = cachedUserCursor.getString(usernameIndex);
            String givenName = cachedUserCursor.getString(givenNameIndex);
            String email = cachedUserCursor.getString(emailIndex);

            User.Builder userBuilder = new User.Builder().username(username)
                    .givenName(givenName)
                    .email(email);

            User user = FactoryMaker.getUserFactory().buildUser(userBuilder);
            users.add(user);
        }
        cachedUserCursor.close();
        return users;
    }

    /**
     * Builds a User object from the current point in cachedUserCursor
     * @return
     */
    private User buildUser() {
        int usernameIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_USERNAME);
        int givenNameIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_GIVENNAME);
        int emailIndex = cachedUserCursor.getColumnIndex(CachedUserORM.COLUMN_EMAIL);

        User user = null;

        while(cachedUserCursor.moveToNext()){
            String username = cachedUserCursor.getString(usernameIndex);
            String givenName = cachedUserCursor.getString(givenNameIndex);
            String email = cachedUserCursor.getString(emailIndex);

            User.Builder userBuilder = new User.Builder().username(username)
                    .givenName(givenName)
                    .email(email);

            user = FactoryMaker.getUserFactory().buildUser(userBuilder);

        }
        return user;
    }


    /**
     * Validates if a User exists in storage or not.
     * @param username
     * @return
     */
    private boolean validateExisting(String username){
        try {
            if (getUserByName(username) != null) {
                return true;
            }
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        return false;
    }


}

package com.magnusson_dev.purchaseplanner.storage.userstorage;


import android.content.Context;

public class UserHandlerFactory {

    private static UserHandler instance = null;

    public static UserHandler getUserHandler(Context context){
        if (instance==null){
            instance = new UserHandlerImpl(context);
        }
        return instance;
    }

}


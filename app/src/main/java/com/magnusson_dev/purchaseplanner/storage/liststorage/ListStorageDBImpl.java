package com.magnusson_dev.purchaseplanner.storage.liststorage;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.orm.ItemORM;
import com.magnusson_dev.purchaseplanner.storage.orm.ListORM;
import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConnection;
import com.magnusson_dev.purchaseplanner.storage.dbutils.QueryBuilder;
import com.magnusson_dev.purchaseplanner.storage.orm.UserORM;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.utils.DateFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * An implementation of the ListStorage interface using an SQLite database.
 */
public class ListStorageDBImpl implements ListStorage {


    // TODO
    // Continue working on the methods needed for the User class


    private static final String TAG = "ListStorageDBImpl";


    private static ListStorageDBImpl instance;


    private Cursor listCursor;
    private Cursor itemCursor;
    private Cursor userCursor;
    private SQLiteDatabase db;
    private DBConnection dbHelper;

    private ListStorageDBImpl(Context context){
        dbHelper = DBConnection.getInstance(context);
        setDB();
        Log.d(TAG, "ListStorageDBImpl: db status "+"="+db.isOpen());
    }

    static ListStorageDBImpl getInstance(Context context){
        if(instance==null){
            instance = new ListStorageDBImpl(context);
        }
        return instance;
    }

    private void setDB(){
        db = dbHelper.getDatabase();
    }


    @Override
    public BaseList getListByTag(String tag) throws StorageFetchException {
        setDB();
        if(tag==null){
            throw new StorageFetchException("Input cannot be NULL");
        } else if(tag.equalsIgnoreCase("")){
            throw new StorageFetchException("Empty input for tag");
        }

        listCursor = db.query(ListORM.TABLE_NAME,
                QueryBuilder.getListByTagProjection(),
                QueryBuilder.getListByTagSelection(),
                QueryBuilder.getListByTagSelectionArgs(tag),
                null,
                null,
                null
        );
        BaseList list = buildList();
        listCursor.close();

        return list;
    }

    /**
     * Builds a list starting from where the listCursor is at currently.
     * Attaches users and list items.
     * @return
     */
    private BaseList buildList() {
        int tagIndex = listCursor.getColumnIndex(ListORM.COLUMN_TAG);
        int categoryIndex = listCursor.getColumnIndex(ListORM.COLUMN_CATEGORY);
        int dateCreatedIndex = listCursor.getColumnIndex(ListORM.COLUMN_DATECREATED);
        int dateUpdatedIndex = listCursor.getColumnIndex(ListORM.COLUMN_DATEUPDATED);
        int idIndex = listCursor.getColumnIndex(ListORM.COLUMN_ID);
        int listIdIndex = listCursor.getColumnIndex(ListORM.COLUMN_LISTID);
        int invitesSentIndex = listCursor.getColumnIndex(ListORM.COLUMN_INVITES_SENT);

        BaseList list = null;

        while(listCursor.moveToNext()){
            // Create attributes
            String tag = listCursor.getString(tagIndex);
            String category = listCursor.getString(categoryIndex);
            String dateCreated = listCursor.getString(dateCreatedIndex);
            String dateUpdated = listCursor.getString(dateUpdatedIndex);
            int listId = 0;
            try{
                listId = listCursor.getInt(listIdIndex);
            }catch(NumberFormatException nfe){
                nfe.printStackTrace();
            }

            boolean invitesSent = listCursor.getInt(invitesSentIndex) == 1; // 1 = true, else = false


            // Create list items
            long dbIndex = listCursor.getLong(idIndex);
            List<ListItem> items = getAllListItemsAssociated(dbIndex, category);

            // Create users
            List<User> users = getAllUsersAssociated(dbIndex);

            // Build list
            BaseList.Builder listBuilder = new BaseList.Builder().id(listId)
                    .tag(tag)
                    .category(category)
                    .dateCreated(DateFormatter.parseToDate(dateCreated))
                    .dateUpdated(DateFormatter.parseToDate(dateUpdated))
                    .invitesSent(invitesSent)
                    .listItems(items)
                    .users(users);

            list = FactoryMaker.getListFactory().buildList(listBuilder);
        }
        return list;

    }


    /**
     * Used to validate if a list exists in storage and therefore
     * decide if the list should be added to storage or edited.
     * @param tag
     * @return
     */
    private boolean validateExistingList(String tag){
        try {
            BaseList list = getListByTag(tag);
            if (list.getTag().equalsIgnoreCase(tag)) {
                return true;
            }
        }catch(StorageFetchException sfe){
            Log.d(TAG, "validateExistingList: "+sfe.getMessage());
        }catch(NullPointerException npe){
            npe.printStackTrace();
            return false;
        }
        return false;

    }

    @Override
    public boolean addListToStorage(BaseList list) throws StorageEditException {
        Log.d(TAG, "addListToStorage: Adding list : "+list);
        setDB();
        if(list==null){
            throw new StorageEditException("List cannot be of value NULL");
        }else if(validateExistingList(list.getTag())){
            editListInStorage(list);
            throw new StorageEditException("List already exists in storage, applying editList instead");
        }
        long listId = db.insert(ListORM.TABLE_NAME,
                null,
                QueryBuilder.addListValues(list));

        addAssociatedItems(list, listId);
        addAssociatedUsers(list, listId);
        return true;
    }

    /**
     * Add associated items of specific lists to their respective tables in
     * DB
     * @param list
     * @param listId
     */
    private void addAssociatedItems(BaseList list , long listId){
        list.getListItems().forEach(item -> {
            db.insert(ItemORM.TABLE_NAME,
                    null,
                    QueryBuilder.addListItemValues(item, listId));
        });
    }

    /**
     * Add associated users of specific lists to their respective tables in
     * DB
     * @param list
     * @param listId
     */
    private void addAssociatedUsers(BaseList list, long listId){
        if(list.getSharedUsers().size()>0) {
            list.getSharedUsers().forEach(user -> {
                if(user!=null){
                db.insert(UserORM.TABLE_NAME,
                        null,
                        QueryBuilder.addUserValues(user, listId));
                }
            });

        }
    }

    @Override
    public List<BaseList> getTotalList() {
        setDB();
        List<BaseList> totalLists = new ArrayList<>();

        listCursor = db.rawQuery(QueryBuilder.getAllLists(),null);
        int tagIndex = listCursor.getColumnIndex(ListORM.COLUMN_TAG);
        int categoryIndex = listCursor.getColumnIndex(ListORM.COLUMN_CATEGORY);
        int dateCreatedIndex = listCursor.getColumnIndex(ListORM.COLUMN_DATECREATED);
        int dateUpdatedIndex = listCursor.getColumnIndex(ListORM.COLUMN_DATEUPDATED);
        int idIndex = listCursor.getColumnIndex(ListORM.COLUMN_ID);
        int listIdIndex = listCursor.getColumnIndex(ListORM.COLUMN_LISTID);
        int invitesSentIndex = listCursor.getColumnIndex(ListORM.COLUMN_INVITES_SENT);


        while(listCursor.moveToNext()){
            // Create attributes
            String tag = listCursor.getString(tagIndex);
            String category = listCursor.getString(categoryIndex);
            String dateCreated = listCursor.getString(dateCreatedIndex);
            String dateUpdated = listCursor.getString(dateUpdatedIndex);
            int listId;
            // Sets it to 0. just active when using S3 solution. when using DynamoDB, this will have a number
            try {
                listId = listCursor.getInt(listIdIndex);
            }catch(NumberFormatException nfe){
                listId = 0;
            }

            boolean invitesSent = listCursor.getInt(invitesSentIndex) == 1; // 1 = true, else = false

            // Create list items
            long id = listCursor.getLong(idIndex);
            List<ListItem> items = getAllListItemsAssociated(id, category);

            // Create users
            List<User> users = getAllUsersAssociated(id);

            // Build list
            BaseList.Builder listBuilder = new BaseList.Builder().id(listId)
                    .tag(tag)
                    .category(category)
                    .invitesSent(invitesSent)
                    .dateCreated(DateFormatter.parseToDate(dateCreated))
                    .dateUpdated(DateFormatter.parseToDate(dateUpdated))
                    .listItems(items)
                    .users(users);

            BaseList list = FactoryMaker.getListFactory().buildList(listBuilder);
            totalLists.add(list);

        }
        listCursor.close();
        return totalLists;
    }

    private void logTotalLists(List<BaseList> lists) {
        lists.forEach(list -> Log.d(TAG, "logTotalLists: Fetched from db : "+list.getListItems()));
    }

    /**
     * Fetch all users in DB related to a specific list by listID
     * @param listId
     * @return
     */
    private List<User> getAllUsersAssociated(long listId){
        userCursor = db.query(UserORM.TABLE_NAME,
                QueryBuilder.getAllAssociatedUsersProjection(),
                QueryBuilder.getAllAssociatedUsersSelection(),
                QueryBuilder.getAllAssociatedUsersSelectionArgs(listId),
                null,
                null,
                null);

        int usernameIndex = userCursor.getColumnIndex(UserORM.COLUMN_USERNAME);
        int givenNameIndex = userCursor.getColumnIndex(UserORM.COLUMN_GIVENNAME);
        int emailIndex = userCursor.getColumnIndex(UserORM.COLUMN_EMAIL);

        List<User> users = new ArrayList<>();

        while(userCursor.moveToNext()){
            String username = userCursor.getString(usernameIndex);
            String givenName = userCursor.getString(givenNameIndex);
            String email = userCursor.getString(emailIndex);

            User.Builder builder = new User.Builder().username(username)
                    .email(email)
                    .givenName(givenName);
            User user = FactoryMaker.getUserFactory().buildUser(builder);
            users.add(user);
        }
        userCursor.close();
        return users;
    }

    private void logUsers(List<User> users){
        users.forEach(u -> Log.d(TAG, "logUsers: Content in db users : "+u.getUsername()));
    }

    /**
     * Fetch all associated list items of a specific list based on a list id
     * @param listId
     * @param category
     * @return
     */
    private List<ListItem> getAllListItemsAssociated(long listId, String category){
        itemCursor = db.query(ItemORM.TABLE_NAME,
                                QueryBuilder.getAllAssociatedItemsProjection(),
                                QueryBuilder.getAllAssociatedItemsSelection(),
                                QueryBuilder.getAllAssociatedItemsSelectionArgs(listId),
                                null,
                                null,
                                null
                                );

        int nameIndex = itemCursor.getColumnIndex(ItemORM.COLUMN_NAME);
        int isCheckedIndex = itemCursor.getColumnIndex(ItemORM.COLUMN_ISCHECKED);
        int lastUpdatedByIndex = itemCursor.getColumnIndex(ItemORM.COLUMN_LASTUPDATEDBY);

        List<ListItem> items = new ArrayList<>();

        while(itemCursor.moveToNext()){
            String name = itemCursor.getString(nameIndex);
            int isCheckedValue = itemCursor.getInt(isCheckedIndex);
            boolean isChecked = isCheckedValue == 1; // 1 == true : 0 == false : from DB
            String lastUpdatedBy = itemCursor.getString(lastUpdatedByIndex);

            ListItem.Builder itemBuilder = new ListItem.Builder().name(name)
                    .isChecked(isChecked)
                    .lastUpdatedBy(lastUpdatedBy);
            ListItem item = FactoryMaker.getListItemFactory().buildListItem(itemBuilder, category);
            items.add(item);
        }
        itemCursor.close();
        return items;
    }

    private void logItems(List<ListItem> items) {
        items.forEach(i -> Log.d(TAG, "logItems: Content in db items : "+i));
    }


    @Override
    public boolean editListInStorage(BaseList list) throws StorageEditException {
        setDB();
        String tag = list.getTag();
        if(list==null){throw new StorageEditException("BaseList cannot be NULL");}
        if(list.getListItems()==null){throw new StorageEditException("No items in this shoppinglist yet. List=NULL");}
        long listId = getListId(tag);
        if(listId>0){
            Log.d(TAG, "editListInStorage: List exists, starting edit");
            db.update(ListORM.TABLE_NAME,
                    QueryBuilder.updateListValues(list),
                    (ListORM.COLUMN_TAG+" = ?"),
                    new String[]{tag});
            removeAllAssociatedItems(listId);
            addAssociatedItems(list, listId);
            removeAllAssociatedUsers(listId);
            addAssociatedUsers(list, listId);
        }
        return false;
    }

    private long getListId(String tag){
        setDB();
        listCursor = db.query(ListORM.TABLE_NAME,
                QueryBuilder.getListIdProjection(),
                QueryBuilder.getListIdSelection(),
                QueryBuilder.getListIdSelectionArgs(tag),
                null,
                null,
                null);
        long id = 0;
        int idIndex = listCursor.getColumnIndex(ListORM.COLUMN_ID);
        while(listCursor.moveToNext()){
            id = listCursor.getLong(idIndex);
        }
        listCursor.close();
        return id;
    }



    @Override
    public boolean deleteListInStorage(BaseList list) throws StorageEditException {
        setDB();
        long listId = getListId(list.getTag()); // IMPORTANT TO DO BEFORE DELETION OF LIST (TO BE ABLE TO DELETE ITEMS AND USERS)
        int deletionState = db.delete(ListORM.TABLE_NAME,
                QueryBuilder.deleteListInStorageSelection(),
                QueryBuilder.deleteListInStorageSelectionArgs(list.getTag()));
        if(deletionState > 0){
            removeAllAssociatedItems(listId);
            removeAllAssociatedUsers(listId);
            return true;
        }
        return false;
    }


    /**
     * Remove all associated list items of a specific list based on a listID
     * @param listId
     */
    private void removeAllAssociatedItems(long listId){
        int deletionState = db.delete(ItemORM.TABLE_NAME,
                QueryBuilder.removeAllAssociatedItemsSelection(),
                QueryBuilder.removeAllAssociatedItemsSelectionArgs(listId));
        if(deletionState > 0){
            //Log.d(TAG, "removeAllAssociatedItems: Removing items was successfully done!");
        }
    }

    /**
     * Remove all associated users of a specific list based on a listID
     * @param listId
     */
    private void removeAllAssociatedUsers(long listId) {
        int deletionState = db.delete(UserORM.TABLE_NAME,
                QueryBuilder.removeAllAssociatedUsersSelection(),
                QueryBuilder.removeAllAssociatedUsersSelectionArgs(listId)
        );
        if(deletionState > 0){
            //Log.d(TAG, "removeAllAssociatedUsers: Removing users was successfully done");
        }
    }






}

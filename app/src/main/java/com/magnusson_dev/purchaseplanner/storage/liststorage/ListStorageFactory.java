package com.magnusson_dev.purchaseplanner.storage.liststorage;


import android.content.Context;

public class ListStorageFactory {

    public static ListStorage getListStorage(){
        return ListStorageImpl.getInstance();
    }

    public static ListStorage getListStorageDB(Context context) { return ListStorageDBImpl.getInstance(context);}
}

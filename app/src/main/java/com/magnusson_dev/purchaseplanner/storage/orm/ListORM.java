package com.magnusson_dev.purchaseplanner.storage.orm;


import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConstants;

/**
 * Mapping class between a BaseList object to SQLite
 */
public class ListORM {


    public static final String TABLE_NAME = DBConstants.LIST_TABLE_NAME;


    // Identifier
    public static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    public static final String COLUMN_ID = "id";


    // Attributes
    public static final String COLUMN_LISTID_TYPE = "INTEGER";
    public static final String COLUMN_LISTID = "list_id";

    public static final String COLUMN_TAG_TYPE = "TEXT";
    public static final String COLUMN_TAG = "tag";

    public static final String COLUMN_DATECREATED_TYPE = "TEXT";
    public static final String COLUMN_DATECREATED = "date_created";

    public static final String COLUMN_DATEUPDATED_TYPE = "TEXT";
    public static final String COLUMN_DATEUPDATED = "date_updated";

    public static final String COLUMN_CATEGORY_TYPE = "TEXT";
    public static final String COLUMN_CATEGORY = "category";

    public static final String COLUMN_INVITES_SENT_TYPE = "INTEGER NOT NULL";
    public static final String COLUMN_INVITES_SENT = "invites_sent";


    // Create table
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + " ("+
            COLUMN_ID + " " + COLUMN_ID_TYPE + DBConstants.COMMA_SEP +
            COLUMN_LISTID + " " + COLUMN_LISTID_TYPE + DBConstants.COMMA_SEP +
            COLUMN_TAG + " " + COLUMN_TAG_TYPE + DBConstants.COMMA_SEP +
            COLUMN_CATEGORY + " " + COLUMN_CATEGORY_TYPE + DBConstants.COMMA_SEP +
            COLUMN_DATECREATED + " " + COLUMN_DATECREATED_TYPE + DBConstants.COMMA_SEP +
            COLUMN_DATEUPDATED + " " + COLUMN_DATEUPDATED_TYPE + DBConstants.COMMA_SEP +
            COLUMN_INVITES_SENT + " " + COLUMN_INVITES_SENT_TYPE +
            " )";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS "+ TABLE_NAME;
}

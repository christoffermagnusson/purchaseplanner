package com.magnusson_dev.purchaseplanner.storage.liststorage;


import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

/**
 * Interface used by the ListHandler interface. Reflects the same methods but abstracts the
 * access one step further.
 */
public interface ListStorage {


        BaseList getListByTag(String tag) throws StorageFetchException;

        boolean addListToStorage(BaseList list) throws StorageEditException;

        List<BaseList> getTotalList();

        boolean editListInStorage(BaseList list)throws StorageEditException;

        boolean deleteListInStorage(BaseList list) throws StorageEditException;



}


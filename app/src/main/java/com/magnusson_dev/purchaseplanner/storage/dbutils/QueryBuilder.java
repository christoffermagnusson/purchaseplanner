package com.magnusson_dev.purchaseplanner.storage.dbutils;


import android.content.ContentValues;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.orm.ItemORM;
import com.magnusson_dev.purchaseplanner.storage.orm.ListORM;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.storage.orm.CachedUserORM;
import com.magnusson_dev.purchaseplanner.storage.orm.UserORM;

/**
 * Class used to abstract and remove as much fluff code from
 * the storage implementation class. Also increases readability
 * when placing all these necessities in a separate class.
 */
public class QueryBuilder {

    private static final String TAG = "QueryBuilder";


    public static String getAllLists(){
        return String.format("SELECT * FROM %s", ListORM.TABLE_NAME);
    }

    public static String [] getListIdProjection(){
        return new String []  {
                ListORM.COLUMN_ID
        };

    }

    public static String getListIdSelection(){
        return ListORM.COLUMN_TAG + " = ?";
    }

    public static String [] getListIdSelectionArgs(String tag){
        return new String [] {tag};
    }

    public static String [] getAllAssociatedItemsProjection(){
        return new String [] {
                ItemORM.COLUMN_NAME,
                ItemORM.COLUMN_ISCHECKED,
                ItemORM.COLUMN_LASTUPDATEDBY
        };

    }

    public static String getAllAssociatedItemsSelection(){
        return ItemORM.COLUMN_LISTID + " = ?";
    }

    public static String [] getAllAssociatedItemsSelectionArgs(long listId){
        return new String[] {Long.toString(listId)};
    }

    public static String [] getAllAssociatedUsersProjection(){
        return new String [] {
                UserORM.COLUMN_USERNAME,
                UserORM.COLUMN_GIVENNAME,
                UserORM.COLUMN_EMAIL
        };
    }

    public static String getAllAssociatedUsersSelection(){ return UserORM.COLUMN_LISTID + " = ?";}

    public static String [] getAllAssociatedUsersSelectionArgs(long listId){
        return new String [] {Long.toString(listId)};
    }

    public static ContentValues addListValues(BaseList list){
        ContentValues values = new ContentValues();

        values.put(ListORM.COLUMN_TAG, list.getTag());
        values.put(ListORM.COLUMN_DATECREATED, list.getDateCreated());
        values.put(ListORM.COLUMN_DATEUPDATED, list.getDateUpdated());
        values.put(ListORM.COLUMN_CATEGORY, list.getCategoryType().toString());
        values.put(ListORM.COLUMN_LISTID, list.getId());
        int invitesSent = list.isInvitesSent() ? 1 : 0;
        values.put(ListORM.COLUMN_INVITES_SENT, invitesSent);

        Log.d(TAG, "addListValues: "+values);

        return values;
    }

    public static ContentValues addListItemValues(ListItem item, long listId){
        ContentValues values = new ContentValues();

        values.put(ItemORM.COLUMN_NAME, item.getName());
        int itemChecked = item.isChecked() ? 1 : 0; // 1 = true, 0 = false
        values.put(ItemORM.COLUMN_ISCHECKED, itemChecked);

        values.put(ItemORM.COLUMN_LISTID, listId);
        values.put(ItemORM.COLUMN_LASTUPDATEDBY, item.getLastupdatedBy());

        return values;
    }

    public static ContentValues updateListValues(BaseList list){
        ContentValues values = new ContentValues();

        values.put(ListORM.COLUMN_LISTID, list.getId());
        values.put(ListORM.COLUMN_DATEUPDATED, list.getDateUpdated());
        int invitesSent = list.isInvitesSent() ? 1 : 0;
        values.put(ListORM.COLUMN_INVITES_SENT, invitesSent);

        return values;
    }

    public static ContentValues addUserValues(User user, long listId){
        ContentValues values = new ContentValues();

        values.put(UserORM.COLUMN_USERNAME, user.getUsername());
        values.put(UserORM.COLUMN_GIVENNAME, user.getGivenName());
        values.put(UserORM.COLUMN_EMAIL, user.getEmail());
        values.put(UserORM.COLUMN_LISTID, listId);

        return values;
    }


    public static String [] getListByTagProjection(){
        return new String [] {ListORM.COLUMN_TAG,
                ListORM.COLUMN_LISTID,
                ListORM.COLUMN_CATEGORY,
                ListORM.COLUMN_DATECREATED,
                ListORM.COLUMN_DATEUPDATED,
                ListORM.COLUMN_ID,
                ListORM.COLUMN_INVITES_SENT};
    }

    public static String getListByTagSelection(){
        return ListORM.COLUMN_TAG + " = ?";
    }

    public static String [] getListByTagSelectionArgs(String tag){
        return new String [] {tag};
    }


    public static String removeAllAssociatedItemsSelection(){
        return ItemORM.COLUMN_LISTID + " = ?";
    }

    public static String [] removeAllAssociatedItemsSelectionArgs(long listId){
        return new String [] {String.valueOf(listId)};
    }

    public static String removeAllAssociatedUsersSelection(){
        return UserORM.COLUMN_LISTID + " = ?";
    }

    public static String [] removeAllAssociatedUsersSelectionArgs(long listId){
        return new String [] {String.valueOf(listId)};
    }

    public static String deleteListInStorageSelection(){
        return ListORM.COLUMN_TAG + " = ?";
    }

    public static String [] deleteListInStorageSelectionArgs(String tag){
        return new String [] {tag};
    }

    public static ContentValues addCachedUserValues(User user){
        ContentValues values = new ContentValues();
        values.put(CachedUserORM.COLUMN_USERNAME, user.getUsername());
        values.put(CachedUserORM.COLUMN_GIVENNAME, user.getGivenName());
        values.put(CachedUserORM.COLUMN_EMAIL, user.getEmail());

        return values;
    }

    public static String getAllCachedUsers(){
        return String.format("SELECT * FROM %s", CachedUserORM.TABLE_NAME);
    }

    public static String [] getCachedUserByNameProjection(){
        return new String [] {CachedUserORM.COLUMN_USERNAME,
                CachedUserORM.COLUMN_GIVENNAME,
                CachedUserORM.COLUMN_EMAIL};
    }

    public static String getCachedUserByNameSelection(){
        return CachedUserORM.COLUMN_USERNAME + " = ?";
    }

    public static String [] getCachedUserByNameSelectionArgs(String username){
        return new String [] {username};
    }

    public static String getAllCachedUsersByName(String filter){
        return String.format("SELECT * FROM %s WHERE username LIKE '%s%%'",CachedUserORM.TABLE_NAME, filter);
    }


}

package com.magnusson_dev.purchaseplanner.storage.liststorage;


import android.util.Log;

import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing class using ArrayLists instead of DB. Tested with JUnit.
 */
public class ListStorageImpl implements ListStorage {

    private ArrayList<BaseList> lists = new ArrayList<>();
    private static final String TAG = "BaseListStorageImpl";
    private static ListStorageImpl instance = null;

    private ListStorageImpl(){}

    static ListStorageImpl getInstance(){
        if(instance==null){
            //Log.d(TAG, "getInstance: Initiating storage component");
            instance = new ListStorageImpl();
        }
        //Log.d(TAG, "getInstance: Returning existing storage component");
        return instance;
    }



    @Override
    public BaseList getListByTag(String tag) throws StorageFetchException {
        BaseList listToReturn = null;
        boolean listFound = false;
        for(BaseList sl : lists){
            if(sl.getTag().equalsIgnoreCase(tag)){
                listFound = true;
                listToReturn = sl;
            }
        }
        if(tag==null){
            throw new StorageFetchException("Argument was NULL");
        }
        if(!listFound){

            throw new StorageFetchException("Shopping list not found in storage for TAG: "+tag);
        }
    System.out.println("FETCHING SHOPPING LIST WITH TAG = "+listToReturn.getTag());
    return listToReturn;

    }

    @Override
    public boolean addListToStorage(BaseList list) throws StorageEditException {
        if(list==null){
            throw new StorageEditException("Trying to insert a NULL value object");
        }else if(!validateExisting(list)){
            System.out.println("ADDING TO STORAGE : "+list.getTag());
            lists.add(list);
            return true;
        }else if(validateExisting(list)){
            System.out.println("DUPLICATE!");
            throw new StorageEditException("Trying to add already existing list; Use editListInStorage instead");
        }
        return false;

    }

    @Override
    public List<BaseList> getTotalList(){
        return this.lists;
    }

    @Override
    public boolean editListInStorage(BaseList list) throws StorageEditException {
        if(list==null){throw new StorageEditException("BaseList cannot be NULL");}
        if(list.getListItems()==null){throw new StorageEditException("No items in this shoppinglist yet. List=NULL");}
        else if(validateExisting(list)){ // shoppinglist exists
            System.out.println("EDITING LIST="+list.getTag());
            List<ListItem> listOfItems = list.getListItems();
            list = pickList(list); // fetch this instance
            list.setListItems(listOfItems);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteListInStorage(BaseList list) throws StorageEditException {
        BaseList toRemove = null;
        try {
            for (BaseList bl : lists) {
                if (bl.getTag().equals(list.getTag())) {
                    toRemove = bl;
                }
            }
            if(toRemove!=null) {
                lists.remove(toRemove);
            }else{
                throw new StorageEditException("List with tag "+list.getTag()+" does not exist in storage for deletion");
            }
        }catch(NullPointerException npe) {
            Log.d(TAG, "deleteListInStorage: No defined list to remove");
            return false;
        }
        return true;
    }


    private boolean validateExisting(BaseList list) {

        for(BaseList sl : lists){
            if(sl.getTag().equals(list.getTag())){
                return true;
            }
        }
        return false;
    }

    private BaseList pickList(BaseList list){
        for(BaseList sl : lists){
            if(sl.getTag().equals(list.getTag())){
                return sl;
            }
        }
        return null;
    }
}

package com.magnusson_dev.purchaseplanner.storage.liststorage;



import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

public class ListHandlerImpl implements ListHandler{

    private static final String TAG = "ShoppingListHandler";

    private ListStorage storageDB;

    private Context context;


    public ListHandlerImpl(Context context){
        this.context=context;
        storageDB = ListStorageFactory.getListStorageDB(context);
        //storageDB = ListStorageFactory.getListStorage();
    }

    public BaseList getListByTag(String tag)throws StorageFetchException{
            return storageDB.getListByTag(tag);
    }

    public void addListToStorage(BaseList list)throws StorageEditException{
            storageDB.addListToStorage(list);
    }

    public List<BaseList> getTotalList(){
        return storageDB.getTotalList();
    }

    public void editListToStorage(BaseList list)throws StorageEditException{
            storageDB.editListInStorage(list);
    }

    public void deleteListInStorage(BaseList list)throws StorageEditException{
            storageDB.deleteListInStorage(list);
    }



    @Override
    public void setContext(Context context) {
        this.context=context;
        storageDB = ListStorageFactory.getListStorageDB(context);
    }

    @Override
    public Context getContext() {
        return this.context;
    }
}

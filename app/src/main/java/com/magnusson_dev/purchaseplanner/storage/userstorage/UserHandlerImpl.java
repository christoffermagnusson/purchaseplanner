package com.magnusson_dev.purchaseplanner.storage.userstorage;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

public class UserHandlerImpl implements UserHandler {

    private Context context;

    private UserStorage userStorage;

    UserHandlerImpl(Context context){
        this.context=context;
        userStorage = UserStorageFactory.getUserStorage(context);
    }

    @Override
    public List<User> getAllCachedUsers() throws StorageFetchException {
        return userStorage.getAllCachedUsers();
    }

    @Override
    public List<User> getAllCachedUsersByName(String filter) throws StorageFetchException {
        return userStorage.getAllCachedUsersByName(filter);
    }

    @Override
    public void storeUser(User user) throws StorageEditException {
        userStorage.storeUser(user);
    }



    @Override
    public User getUserByName(String username) throws StorageFetchException {
        return userStorage.getUserByName(username);
    }

    @Override
    public void updateUser(User user) throws StorageEditException {
        userStorage.updateUser(user);
    }

    @Override
    public void setContext(Context context) {
        this.context=context;
    }

    @Override
    public Context getContext() {
        return this.context;
    }
}

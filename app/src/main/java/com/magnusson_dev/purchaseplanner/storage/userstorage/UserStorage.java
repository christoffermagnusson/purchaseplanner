package com.magnusson_dev.purchaseplanner.storage.userstorage;


import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

/**
 * An interface used by the UserHandler interface. Used to abstract the storage component.
 */
public interface UserStorage {

    List<User> getAllCachedUsers() throws StorageFetchException;

    List<User> getAllCachedUsersByName(String filter)throws StorageFetchException;

    void storeUser(User user)throws StorageEditException;

    void updateUser(User user)throws StorageEditException;

    User getUserByName(String username) throws StorageFetchException;
}

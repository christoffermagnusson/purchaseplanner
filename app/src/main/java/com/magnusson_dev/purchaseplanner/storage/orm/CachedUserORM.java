package com.magnusson_dev.purchaseplanner.storage.orm;


import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConstants;

/**
 * Used to represent a user that is cached to avoid unnecessary DynamoDB traffic
 * when searching for users to share lists with. This class maps a User object to SQLite
 */
public class CachedUserORM {

    public static final String TABLE_NAME = DBConstants.USER_CACHE_TABLE_NAME;

    public static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_USERNAME_TYPE = "TEXT";
    public static final String COLUMN_USERNAME = "username";

    public static final String COLUMN_GIVENNAME_TYPE = "TEXT";
    public static final String COLUMN_GIVENNAME = "given_name";

    public static final String COLUMN_EMAIL_TYPE = "TEXT";
    public static final String COLUMN_EMAIL = "email";


    public static final String SQL_CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + " ("+
            COLUMN_ID + " " + COLUMN_ID_TYPE + DBConstants.COMMA_SEP +
            COLUMN_USERNAME + " " +COLUMN_USERNAME_TYPE + DBConstants.COMMA_SEP +
            COLUMN_GIVENNAME + " " + COLUMN_GIVENNAME_TYPE + DBConstants.COMMA_SEP +
            COLUMN_EMAIL + " " + COLUMN_EMAIL_TYPE +
            " )";
}

package com.magnusson_dev.purchaseplanner.storage.userstorage;


import android.content.Context;

public class UserStorageFactory {


    public static UserStorage getUserStorage(Context context){
        return UserStorageImpl.getInstance(context);
    }
}

package com.magnusson_dev.purchaseplanner.storage.orm;


import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConstants;

/**
 * Mapping class between a regular User object to SQLite
 */
public class UserORM {

    public static final String TABLE_NAME = DBConstants.USER_TABLE_NAME;

    public static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_LISTID_TYPE = "INTEGER";
    public static final String COLUMN_LISTID = "list_id";

    public static final String COLUMN_USERNAME_TYPE = "TEXT";
    public static final String COLUMN_USERNAME = "username";

    public static final String COLUMN_GIVENNAME_TYPE = "TEXT";
    public static final String COLUMN_GIVENNAME = "given_name";

    public static final String COLUMN_EMAIL_TYPE = "TEXT";
    public static final String COLUMN_EMAIL = "email";


    // Create table
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + " ("+
            COLUMN_ID + " " + COLUMN_ID_TYPE + DBConstants.COMMA_SEP +
            COLUMN_LISTID + " " +COLUMN_LISTID_TYPE + DBConstants.COMMA_SEP +
            COLUMN_USERNAME + " " +COLUMN_USERNAME_TYPE + DBConstants.COMMA_SEP +
            COLUMN_GIVENNAME + " " + COLUMN_GIVENNAME_TYPE + DBConstants.COMMA_SEP +
            COLUMN_EMAIL + " " + COLUMN_EMAIL_TYPE +
            " )";
}
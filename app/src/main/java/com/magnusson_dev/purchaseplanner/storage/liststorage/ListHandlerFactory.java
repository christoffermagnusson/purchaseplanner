package com.magnusson_dev.purchaseplanner.storage.liststorage;


import android.content.Context;

public class ListHandlerFactory {

    private static ListHandler instance;

    public static ListHandler getListHandler(Context context){
        if(instance==null){
            instance = new ListHandlerImpl(context);
        }
        if(instance.getContext()==null){
            instance.setContext(context);
        }
        return instance;
    }
}

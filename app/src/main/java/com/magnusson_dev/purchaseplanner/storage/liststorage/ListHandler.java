package com.magnusson_dev.purchaseplanner.storage.liststorage;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

/**
 * Interface that is used by the rest of the application. Functions as
 * a layer before the Storage interface to abstract that it is a storage.
 */
public interface ListHandler {

    /**
     * Returns a list from storage based on a tag.
     * @param tag
     * @return
     * @throws StorageFetchException
     */
    BaseList getListByTag(String tag)throws StorageFetchException;

    /**
     * Adds the specified list in parameter to storage
     * @param list
     * @throws StorageEditException
     */
    void addListToStorage(BaseList list)throws StorageEditException;

    /**
     * Returns all lists currently existing in storage
     * @return
     */
    List<BaseList> getTotalList();

    /**
     * Edit specified list in parameter
     * @param list
     * @throws StorageEditException
     */
    void editListToStorage(BaseList list)throws StorageEditException;

    /**
     * Delete specified list in parameter from storage
     * @param list
     * @throws StorageEditException
     */
    void deleteListInStorage(BaseList list)throws StorageEditException;


    void setContext(Context context);

    Context getContext();
}

package com.magnusson_dev.purchaseplanner.storage.dbutils;



public class DBConstants {

    public static final String DB_NAME_TEST = "plnner_test_v4.db";
    public static final String DB_NAME_PROD = "plnner_prod_v4.db";
    public static final int DB_VERSION = 1;


    public static final String LIST_TABLE_NAME = "lists";
    public static final String ITEM_TABLE_NAME = "items";
    public static final String USER_TABLE_NAME = "users";
    public static final String USER_CACHE_TABLE_NAME = "user_cache";


    //utils
    public static final String COMMA_SEP = ", ";

}

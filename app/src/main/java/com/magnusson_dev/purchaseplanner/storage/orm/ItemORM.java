package com.magnusson_dev.purchaseplanner.storage.orm;


import com.magnusson_dev.purchaseplanner.storage.dbutils.DBConstants;

/**
 * Mapping class between a ListItem object to SQLite
 */
public class ItemORM {

    public static final String TABLE_NAME = DBConstants.ITEM_TABLE_NAME;


    // Identidiers
    public static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_LISTID_TYPE = "INTEGER";
    public static final String COLUMN_LISTID = "list_id";

    // Attributes
    public static final String COLUMN_NAME_TYPE = "TEXT";
    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_ISCHECKED_TYPE = "INTEGER NOT NULL";
    public static final String COLUMN_ISCHECKED = "is_checked";

    public static final String COLUMN_LASTUPDATEDBY_TYPE = "TEXT";
    public static final String COLUMN_LASTUPDATEDBY = "last_updated_by";



    // Create table stmnt
    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE "+ TABLE_NAME + " ("+
                    COLUMN_ID + " " + COLUMN_ID_TYPE + DBConstants.COMMA_SEP +
                    COLUMN_NAME + " " + COLUMN_NAME_TYPE + DBConstants.COMMA_SEP +
                    COLUMN_ISCHECKED + " " + COLUMN_ISCHECKED_TYPE + DBConstants.COMMA_SEP +
                    COLUMN_LISTID + " " + COLUMN_LISTID_TYPE + DBConstants.COMMA_SEP +
                    COLUMN_LASTUPDATEDBY + " " + COLUMN_LASTUPDATEDBY_TYPE +
                    " )";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS "+ TABLE_NAME;


}

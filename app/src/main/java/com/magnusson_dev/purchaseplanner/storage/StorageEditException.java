package com.magnusson_dev.purchaseplanner.storage;

/**
 * Exception created to represent faults occurring when
 * editing in local storage
 */
public class StorageEditException extends Exception {

    public StorageEditException(String message) {
        super(message);
    }

    public StorageEditException(String message, Throwable cause) {
        super(message, cause);
    }
}

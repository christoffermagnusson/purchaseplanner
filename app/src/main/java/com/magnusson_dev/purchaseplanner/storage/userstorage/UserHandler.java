package com.magnusson_dev.purchaseplanner.storage.userstorage;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;

import java.util.List;

/**
 * Interface used by the rest of the app to gain access to storage layer
 * for Users.
 */
public interface UserHandler {


    /**
     * Get all cached user objects existing in database.
     * @return
     * @throws StorageFetchException
     */
    List<User> getAllCachedUsers() throws StorageFetchException;

    /**
     * Returns all existing instances that matches the specified search filter
     * @param filter
     * @return
     * @throws StorageFetchException
     */
    List<User> getAllCachedUsersByName(String filter) throws StorageFetchException;

    /**
     * Stores a cached user in storage
     * @param user
     * @throws StorageEditException
     */
    void storeUser(User user)throws StorageEditException;

    /**
     * Returns a user if exsiting from storage
     * @param username
     * @return
     * @throws StorageFetchException
     */
    User getUserByName(String username)throws StorageFetchException;

    /**
     * Update an existing user in storage with new values
     * @param user
     * @throws StorageEditException
     */
    void updateUser(User user)throws StorageEditException;

    void setContext(Context context);

    Context getContext();
}

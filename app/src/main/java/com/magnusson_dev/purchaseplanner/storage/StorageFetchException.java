package com.magnusson_dev.purchaseplanner.storage;


/**
 * Exception created to represent faults occurring when
 * fetching from storage
 */
public class StorageFetchException extends Exception {

    public StorageFetchException(String message) {
        super(message);
    }

    public StorageFetchException(String message, Throwable cause) {
        super(message, cause);
    }
}

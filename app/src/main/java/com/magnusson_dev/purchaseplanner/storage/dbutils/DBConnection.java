package com.magnusson_dev.purchaseplanner.storage.dbutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.magnusson_dev.purchaseplanner.storage.orm.ItemORM;
import com.magnusson_dev.purchaseplanner.storage.orm.ListORM;
import com.magnusson_dev.purchaseplanner.storage.orm.CachedUserORM;
import com.magnusson_dev.purchaseplanner.storage.orm.UserORM;


/**
 * Class that initiates the connection to a SQLite database and creates its tables.
 * Extends the standard SQLiteOpenHelper.
 */
public class DBConnection extends SQLiteOpenHelper {

    private static DBConnection instance = null;

    private static SQLiteDatabase db = null;

    public static DBConnection getInstance(Context context){
        if (instance == null){
            instance = new DBConnection(context);
        }
        return instance;
    }

    public SQLiteDatabase getDatabase(){
        if(db==null){
            db = getWritableDatabase();
        }
        while(db.isDbLockedByCurrentThread() || db.isDbLockedByOtherThreads()){
            // DB is locked, waiting for release.
        }
        return db;
    }

    private DBConnection(Context context){
        super(context,DBConstants.DB_NAME_TEST, null , DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ListORM.SQL_CREATE_TABLE);
        sqLiteDatabase.execSQL(ItemORM.SQL_CREATE_TABLE);
        sqLiteDatabase.execSQL(UserORM.SQL_CREATE_TABLE);
        sqLiteDatabase.execSQL(CachedUserORM.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

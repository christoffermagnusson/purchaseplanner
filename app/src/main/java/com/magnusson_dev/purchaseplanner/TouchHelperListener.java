package com.magnusson_dev.purchaseplanner;

public interface TouchHelperListener {

    void onSwiped(int position);

}

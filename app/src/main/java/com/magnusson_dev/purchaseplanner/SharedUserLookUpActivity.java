package com.magnusson_dev.purchaseplanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSUserService;
import com.magnusson_dev.purchaseplanner.adapters.SharedUserLookUpRecyclerAdapter;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBLoadUserFinishedEvent;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandler;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandlerFactory;
import com.magnusson_dev.purchaseplanner.utils.KeyboardUtils;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class SharedUserLookUpActivity extends AppCompatActivity implements ShareListAddHandler, ShareListRemoveHandler {

    private static final String TAG = "SharedUserLookUpActivit";

    private UserHandler userHandler;
    private List<User> searchedUsers;

    private AWSUserService awsUserService;

    private SharedUserLookUpRecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;
    private List<User> pickedUsers = new ArrayList<>();
    private CoordinatorLayout snackbarCoordinatorLayout;

    private AutoCompleteTextView userSearchInput;
    private TextView searchInputResult_tv;
    private static final String USER_INPUT = "USER_INPUT";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_user_lookup);
        EventBus.getDefault().register(this);
        userHandler = UserHandlerFactory.getUserHandler(this);
        awsUserService = AWSServiceFactory.getAWSUserService();

        recyclerView = (RecyclerView) findViewById(R.id.sharedUserLookup_rv);
        List<User> initialUsersFromCache = null;
        try {
            initialUsersFromCache = userHandler.getAllCachedUsers();
        }catch(StorageFetchException sfe){
            initialUsersFromCache = new ArrayList<>();
            sfe.printStackTrace();
        }
        refreshRecyclerView(initialUsersFromCache);
        if(SharedUserUpdaterHelper.updatedUsers.size() > 0){
            pickedUsers = SharedUserUpdaterHelper.updatedUsers;
        }


        searchedUsers = new ArrayList<>();
        userSearchInput = (AutoCompleteTextView) findViewById(R.id.sharedUserLookup_actv);
        searchInputResult_tv = (TextView) findViewById(R.id.sharedUserLookUpResult_tv);


        snackbarCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.sharedUserLookupSnackbarCoord);
        NoInternetSnackbar.initialize(getApplicationContext(),snackbarCoordinatorLayout);
        if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.show();
        }




        setupUserSearchInput();
        setupConfirmUsersBtn();
        setupSearchBtn();
    }

    /**
     * Sets up the search button with simple prevalidation on whether
     * anything is typed by the user
     */
    private void setupSearchBtn() {
        Button searchBtn = (Button) findViewById(R.id.sharedUsersAddBtn);
        searchBtn.setOnClickListener(view -> {
            if(validate(userSearchInput.getText().toString())) {
                search();
            }
        });
    }

    private boolean validate(String searchString){
        return !searchString.equalsIgnoreCase("");
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(USER_INPUT, userSearchInput.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        userSearchInput.setText(savedInstanceState.getString(USER_INPUT));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Searches for users on the server (DynamoDB). Hides keyboard upon
     * result
     */
    private void search(){
        final String username = userSearchInput.getText().toString();
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.dismiss();
            awsUserService.getUser(username,getApplicationContext());
        }else{
            NoInternetSnackbar.show();
        }
        KeyboardUtils.hideKeyboard(this);
    }

    /**
     * Setup for the autocompletetextview. Checks for existing users in the local
     * cache to avoid making requests to the server.
     */
    private void setupUserSearchInput() {

        userSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
                // needed for interface
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                String query = charSequence.toString();
                if(query.length()>2) {
                    Log.d(TAG, "onTextChanged: Searching for : "+query);
                    //updateSelectionByQuery(selectionAdapter, query);
                    updateSelectionByQuery(query);
                }else{
                    updateSelectionWithAllCachedUsers();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // needed for interface
            }
        });

    }

    @SuppressWarnings("unchecked")
    private void updateSelectionByQuery(final String query){
        try{
            recyclerAdapter.clear();
            List<User> updatedUserBySearch = userHandler.getAllCachedUsersByName(query);
            if(updatedUserBySearch.size() > 0) {
                updatedUserBySearch.forEach(user -> recyclerAdapter.addUser(user));
                searchInputResult_tv.setText("");
            }else{
                searchInputResult_tv.setText(getString(R.string.shared_lookup_user_not_found_in_cache, query));
            }
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void updateSelectionWithAllCachedUsers(){
        try{
            recyclerAdapter.clear();
            List<User> updatedUserBySearch = userHandler.getAllCachedUsers();
            updatedUserBySearch.forEach(user -> recyclerAdapter.addUser(user));
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
    }





    /**
     * Refreshes RecyclerView with new User objects. Uses the SharedUserUpdaterHelper
     * to communicate with AddListActivity on what User objects are already added.
     * @param users
     */
    @SuppressWarnings("unchecked")
    private void refreshRecyclerView(List<User> users){
            recyclerAdapter = new SharedUserLookUpRecyclerAdapter(this,
                    R.layout.activity_shared_user_lookup_recycler_item,
                    users);
            recyclerAdapter.setShareAddHandler(this);
            recyclerAdapter.setShareRemoveHandler(this);
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1,
                    StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setAdapter(recyclerAdapter);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(false);
    }


    private void setupConfirmUsersBtn() {
        Button confirmBtn = (Button) findViewById(R.id.sharedUserLookupConfirmBtn);
        confirmBtn.setOnClickListener(view -> {
            SharedUserUpdaterHelper.updatedUsers = pickedUsers;
            Log.d(TAG, "onClick: Starting new AddListActivity");
            startActivity(new Intent(getApplicationContext(), AddListActivity.class));
        });
    }


    /**
     * EventBus method for receiving events. This class subscribes on events that
     * tells the activity whether a User object has successfully been fetched from
     * the server (DynamoDB)
     * @param event
     */
    @SuppressWarnings("unchecked")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AWSEvent event){
        Log.d(TAG, "onEvent: Event received");
        if(event.getClass().equals(DynamoDBLoadUserFinishedEvent.class)){
            DynamoDBLoadUserFinishedEvent dynamoDBLoadUserFinishedEvent = (DynamoDBLoadUserFinishedEvent) event;
            if(dynamoDBLoadUserFinishedEvent.isSuccess()){
                Toast successToast = Toast.makeText(getApplicationContext(),
                        dynamoDBLoadUserFinishedEvent.getMessage(),
                        Toast.LENGTH_SHORT);
                successToast.show();
                User user = dynamoDBLoadUserFinishedEvent.getUser();
                storeUser(user);
                if(!userExistsInList(user)) {
                    recyclerAdapter.addUser(user);
                }
            }else{
                Toast failiureToast = Toast.makeText(getApplicationContext(),
                        dynamoDBLoadUserFinishedEvent.getMessage(),
                        Toast.LENGTH_SHORT);
                failiureToast.show();
            }
        }

    }

    /**
     * Helper method for storing a User object to the local cache. Called when a new User has been fetched
     * from the server not previously existing in the local cache.
     * @param user
     */
    private void storeUser(User user) {
        try{
            UserHandler handler = UserHandlerFactory.getUserHandler(getApplicationContext());
            handler.storeUser(user);
        }catch(StorageEditException see){
            see.printStackTrace();
        }
    }


    @Override
    public void addUser(User user) {
        if(userExistsInList(user)){
            Toast.makeText(getApplicationContext(), R.string.shared_lookup_user_already_added_toast, Toast.LENGTH_LONG).show();
        }else{
            pickedUsers.add(user);
        }
    }

    @Override
    public boolean userExistsInList(User user) {
        Predicate<User> existingUser = u -> u.getUsername().equals(user.getUsername());
        int existing = pickedUsers.stream()
                .filter(existingUser)
                .collect(Collectors.toList())
                .size();
        return existing > 0;
    }

    @Override
    public void removeUser(User user) {
        Predicate<User> existingUser = u -> u.getUsername().equals(user.getUsername());
        pickedUsers.removeIf(existingUser);
    }
}

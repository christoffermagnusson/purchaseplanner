package com.magnusson_dev.purchaseplanner;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSListService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoListFoundException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.adapters.DisplayListItemRecyclerAdapter;
import com.magnusson_dev.purchaseplanner.adapters.DisplayListItemRecyclerItemTouchHelper;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddListFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBReadListByIdEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.utils.ColorUtils;
import com.magnusson_dev.purchaseplanner.utils.DateFormatter;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;
import com.magnusson_dev.purchaseplanner.utils.ThemeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.List;


public class DisplayListActivity extends AppCompatActivity implements ICallback,StorageAction,TouchHelperListener{

    private static final String TAG = "DisplayListActivity";
    private BaseList list;
    private DisplayListItemRecyclerAdapter<ListItem> recyclerAdapter;
    private ConstraintLayout containerView;
    private ListHandler storageHandler;

    private AWSListService awsListService;

    private ProgressBar progressBar;
    private FloatingActionButton addNewItemFloatingBtn;
    private boolean inputEnabled = true;
    private ButtonMode mode;



    enum ButtonMode{
        STANDARD,
        FLOATING
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Intent intent = getIntent();
        String category = intent.getStringExtra(IntentConstants.INTENT_CATEGORY);
        setStyle(category);
        String tag = intent.getStringExtra(IntentConstants.INTENT_MESSAGE); // fetches whatever tag of list is to be displayed
        setupActionBar(tag);
        mode = ButtonMode.FLOATING;
        setViewMode();
        if(getWindow().getAttributes().windowAnimations!=R.style.fade_transition) {
            Log.d(TAG, "onCreate: Correct transition not set. Setting it now.");
            overridePendingTransition(R.anim.fade_in_transition, R.anim.fade_out_transition);
        }

        storageHandler = ListHandlerFactory.getListHandler(this);
        awsListService = AWSServiceFactory.getAWSServiceDynamoDB(getApplicationContext(),this);
        try {
            list = storageHandler.getListByTag(tag);
        }catch(StorageFetchException sfe){
            Log.d(TAG, "onCreate: "+sfe.getMessage());
        }
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            awsListService.readListById(list);
            // Show progress here? Calls are so short their barely noticeable
        }else{
            NoInternetSnackbar.show();
        }
        setupAddItemToListDialog();
        setupUI();
        setupRecyclerView();




        CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.displayListSnackbarCoord);
        Log.d(TAG, "onCreate: snackbarCoordinatorLayout = "+snackbarCoordinatorLayout);
        NoInternetSnackbar.initialize(getApplicationContext(), snackbarCoordinatorLayout);
        // Instantiate this as callback for CognitoConstants if lost between
        // activity switching
        if(CognitoConstants.callback==null){
            CognitoConstants.callback = this;
        }

        // EventBus register to receive event
        EventBus.getDefault().register(this);

        super.onCreate(savedInstanceState);
    }

    private void setupUI(){
        containerView = (ConstraintLayout) findViewById(R.id.displayListBackground);
        progressBar = (ProgressBar) findViewById(R.id.displayListProgressBar);
    }

    /**
     * Sets the theme of the activity depending on chosen UI mode
     */
    private void setViewMode(){
        int viewMode = mode==ButtonMode.FLOATING
                ? R.layout.activity_display_list_recycler_floating
                : R.layout.activity_display_list_recycler_standard;
        setContentView(viewMode);
    }


    @Override
    protected void onResume() {
        super.onResume();
        try{
            list = storageHandler.getListByTag(list.getTag());
            setupRecyclerView();
        }catch(StorageFetchException sfe){
            Log.e(TAG, "onResume: "+sfe.getMessage(),sfe);
        }

        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: IN");
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
            awsListService.editList(list);
        }else{
            Toast noInternetConnectionToast = Toast.makeText(getApplicationContext(),
                    getString(R.string.display_list_no_internet),
                    Toast.LENGTH_LONG);
            noInternetConnectionToast.show();
        }

        try {
            storageHandler.editListToStorage(list);
        }catch(StorageEditException see){
            Log.d(TAG, "onStop: "+see.getMessage());
        }

        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }

        Log.d(TAG, "onStop: OUT");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onStartNewActivity(ActivityType.MAIN_ACTIVITY,
                "BACK_PRESSED_FROM_DISPLAYLISTACTIVITY",
                null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_display_list_menu,menu);
        MenuItem shareInteractionItem = menu.getItem(0);
        MenuItem refreshInteractionItem = menu.getItem(1);
        setColorOnIcon(refreshInteractionItem.getIcon());
        setColorOnIcon(shareInteractionItem.getIcon());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.activityDisplayListRefresh:
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    NoInternetSnackbar.dismiss();
                    awsListService.readListById(list);
                    showProgressBar();
                }else{
                    NoInternetSnackbar.show();
                }
                return true;
            case R.id.displayListShare:
                onStartNewActivity(ActivityType.SHARE_LIST_ACTIVITY, list.getTag(), null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Disables touch events while refreshing content from AWS
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !inputEnabled || super.dispatchTouchEvent(ev);
    }





    private void setupRecyclerView(){
        RecyclerView displayRecyclerView = (RecyclerView) findViewById(R.id.displayListRecyclerView);
        Log.d(TAG, "setupRecyclerView: RECYCLERVIEW ? "+displayRecyclerView);
        Log.d(TAG, "setupRecyclerView: Contents of listItems = "+list.getListItems());
        recyclerAdapter = new DisplayListItemRecyclerAdapter<>(this,
                                                                R.layout.activity_display_list_item_reverse,
                                                                list.getListItems());
        recyclerAdapter.setCallback(this);
        recyclerAdapter.setStorageAction(this);
        recyclerAdapter.setCurrentCategory(list.getCategoryType());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        Log.d(TAG, "setupRecyclerView: RECYCLERVIEW == "+displayRecyclerView);
        displayRecyclerView.setLayoutManager(layoutManager);
        displayRecyclerView.setAdapter(recyclerAdapter);
        DisplayListItemRecyclerItemTouchHelper touchHelperCallback = new DisplayListItemRecyclerItemTouchHelper(this);
        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
        touchHelper.attachToRecyclerView(displayRecyclerView);
        recyclerAdapter.sortList();

    }

    @Override
    public void onSwiped(int position) {
        // Temp store item until certain of deletion
        ListItem swipedItem = recyclerAdapter.getItem(position);
        // Remove item from recyclerview
        recyclerAdapter.removeItem(position);
        // Display snackbar with option to undo deletion
        Snackbar undoDeletionSnackBar = Snackbar.make(containerView, getString(R.string.display_list_item_deleted), Snackbar.LENGTH_LONG);
        undoDeletionSnackBar.setAction(getString(R.string.display_list_undo_deletion),(v) -> recyclerAdapter.addItem(swipedItem));
        undoDeletionSnackBar.show();

    }

    private void setupActionBar(String title) {
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        try {
            actionBar.setTitle(title);
        }catch(NullPointerException npe){
            Log.d(TAG, "setupActionBar: No title available for this list. Defaulting to \"List\"");
            actionBar.setTitle("List");
        }
    }

    /**
     * Sets the style theme of the UI depending on the list category
     * @param category
     */
    private void setStyle(String category) {
        category = category.toUpperCase();
        switch(category){
            case "SHOPPING":
                super.setTheme(ThemeUtils.shoppingTheme());
                break;
            case "TODO":
                super.setTheme(ThemeUtils.todoTheme());
                break;
            case "WISHLIST":
                super.setTheme(ThemeUtils.wishlistTheme());
                break;
        }
    }





    private void setColorOnIcon(Drawable icon) {
        icon.mutate();
        icon.setColorFilter(this.getColor(ColorUtils.cardviewLightBackground()), PorterDuff.Mode.SRC_IN);
    }


    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
        if(addNewItemFloatingBtn!=null) {
            hideFloatingActionButton(true);
        }
        enableInput(false);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
        if(addNewItemFloatingBtn!=null) {
            hideFloatingActionButton(false);
        }
        enableInput(true);
    }

    private void enableInput(boolean enabled){
        inputEnabled = enabled;
        float alpha = inputEnabled ? 1.0f : 0.5f;
        recyclerAdapter.setAlpha(alpha);

    }




    private void hideFloatingActionButton(boolean isHidden) {
        if(mode==ButtonMode.FLOATING) {
            if (isHidden) {
                addNewItemFloatingBtn.hide();
            } else {
                addNewItemFloatingBtn.show();
            }
        }
        }



    private void setupAddItemToListDialog() {

        class AddNewItemListener implements View.OnClickListener{

            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DisplayListActivity.this);
                builder.setTitle("Add new item to list");
                final AutoCompleteTextView input = new AutoCompleteTextView(DisplayListActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton(getString(R.string.display_list_add_item_dialog_pos_btn), (dialogInterface, i) -> {
                    try {
                        String name = input.getText().toString();
                        ListItem.Builder itemBuilder = new ListItem.Builder().name(name)
                                .lastUpdatedBy(CognitoSession.CURRENT_USER_INAPP.getUsername());
                        ListItem item = FactoryMaker.getListItemFactory()
                                .buildListItem(itemBuilder, list.getCategoryType().toString());
                        recyclerAdapter.addItem(item);
                        list.setListItems(recyclerAdapter.getListOfItems());
                        list.setFormatDateUpdated(new Date());
                        storageHandler.editListToStorage(list);
                        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                            NoInternetSnackbar.dismiss();
                            awsListService.editList(list);
                        }else{
                            NoInternetSnackbar.show();
                        }
                    }catch(StorageEditException see){
                        Log.e(TAG, "onClick: "+see.getMessage(),see );
                    }

                });
                builder.setNegativeButton(getString(R.string.display_list_add_item_dialog_neg_btn), (dialogInterface, i) -> dialogInterface.cancel());
                builder.show();
            }
        }


        // TEMP VIEW. Check out ShowcaseView if there is time // START
        if(mode==ButtonMode.STANDARD) {
            ImageView icon = (ImageView) findViewById(R.id.addNewItemIcon);
            icon.setImageResource(R.drawable.ic_add_circle_outline_24dp);
            icon.setColorFilter(ContextCompat.getColor(this, R.color.shopping_color));
            Button addNewItemBtn = (Button) findViewById(R.id.addNewItemBtn);
            addNewItemBtn.setBackgroundColor(Color.TRANSPARENT);
            addNewItemBtn.setOnClickListener(new AddNewItemListener());
        }else if(mode==ButtonMode.FLOATING){
            addNewItemFloatingBtn = (FloatingActionButton) findViewById(R.id.displayListFloatingActionBtn);
            addNewItemFloatingBtn.setOnClickListener(new AddNewItemListener());
        }

    }




    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        Intent intent;
        switch(activityType){
            case MAIN_ACTIVITY:
                intent = new Intent(this, MainActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                Log.d(TAG, "onStartNewActivity: Starting new MainActivity");
                startActivity(intent);
                break;
            case SHARE_LIST_ACTIVITY:
                intent = new Intent(this, ShareListActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                Log.d(TAG, "onStartNewActivity: Starting new ShareListActivity");
                startActivity(intent);
                break;

        }
    }

    @Override
    public Context onRefreshContext() {
        return this.getApplicationContext();
    }

    @Override
    public void updateListToStorage(List<ListItem> listOfItems) {
        list.setListItems(listOfItems);
        list.setFormatDateUpdated(new Date());
        try {
            storageHandler.editListToStorage(list);
        }catch(StorageEditException see){
            Log.d(TAG, "updateListToStorage: "+see.getMessage());
        }

        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.dismiss();
            awsListService.editList(list);
        }else{
            NoInternetSnackbar.show();
        }
    }
    
    @Subscribe
    public void onEvent(AWSEvent event){
        Log.d(TAG, "onEvent: Incoming event = "+event.getClass());
        EventHandler.Handler successHandler = defaultSuccessHandler -> Log.d(TAG, "onEvent: No eventhandler attached");
        EventHandler.Handler failureHandler = defaultFailureHandler -> Log.d(TAG, "onEvent: No eventhandler attached");

        switch(event.getClass().getSimpleName()){

            case EventName.GET_REQUEST_RESPONSE_EVENT:
                successHandler = e -> hideProgressBar();
                failureHandler = e -> hideProgressBar();
                break;

            case EventName.DYNAMODB_READ_LIST_BY_ID_EVENT:
                DynamoDBReadListByIdEvent dynamoDBReadListByIdEvent = (DynamoDBReadListByIdEvent) event;
                successHandler = e -> {
                    hideProgressBar();
                    BaseList.Builder builder = new BaseList.Builder().tag(list.getTag())
                            .category(list.getCategory())
                            .invitesSent(list.isInvitesSent())
                            .dateCreated(DateFormatter.parseToDate(list.getDateCreated()))
                            .dateUpdated(DateFormatter.parseToDate(list.getDateUpdated()))
                            .id(list.getId())
                            .listItems(dynamoDBReadListByIdEvent.getBaseList().getListItems())
                            .users(list.getSharedUsers());
                    list = FactoryMaker.getListFactory().buildList(builder);
                    recyclerAdapter.addAllItems(list.getListItems());
                };
                failureHandler = e -> {
                    hideProgressBar();
                    Exception exception = dynamoDBReadListByIdEvent.getException();
                    if(exception.getClass().equals(NoConnectionException.class)){
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                        NoInternetSnackbar.show();
                    }else if(exception.getClass().equals(NoListFoundException.class)){
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                };
                break;

            case EventName.COGNITO_CREDENTIALS_PROVIDER_CREATED_EVENT:
                successHandler = e -> Log.d(TAG, "onEvent: CredentialsProvider successfully created");
                failureHandler = e -> NoInternetSnackbar.show();
                break;

            case EventName.DYNAMODB_ADD_LIST_FINISHED_EVENT:
                DynamoDBAddListFinishedEvent addListFinishedEvent = (DynamoDBAddListFinishedEvent) event;
                successHandler = e -> {
                    try{
                        storageHandler.addListToStorage(addListFinishedEvent.getAddedList());
                    }catch(StorageEditException see){
                        Log.d(TAG, "onEvent: "+see.getMessage());
                    }
                };
                failureHandler = e -> Log.d(TAG, "onEvent: Couldn't add list to backend service");
        }


        event.setHandler(event.isSuccess() ? successHandler : failureHandler);

        if(event.getHandler()!=null) {
            EventHandler.handle(event);
        }
    }
}

package com.magnusson_dev.purchaseplanner;

import com.magnusson_dev.purchaseplanner.model.ListItem;

import java.util.List;

/**
 * Used by adapters to make changes to storage by their respective activities
 */
public interface StorageAction {

    /**
     * Makes an update of the list items into storage
     * @param listOfItems
     */
    void updateListToStorage(List<ListItem> listOfItems);
}

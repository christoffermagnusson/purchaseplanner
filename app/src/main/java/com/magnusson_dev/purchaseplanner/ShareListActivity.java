package com.magnusson_dev.purchaseplanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSListService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSUserService;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.adapters.ShareListItemAdapter;
import com.magnusson_dev.purchaseplanner.adapters.ShareListSearchUserRecyclerAdapter;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddListFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationSentEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBLoadUserFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.ListCategory;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandler;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandlerFactory;
import com.magnusson_dev.purchaseplanner.utils.KeyboardUtils;
import com.magnusson_dev.purchaseplanner.utils.DrawableComparator;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class ShareListActivity extends AppCompatActivity implements ShareListAddHandler, ICallback{


    private ConstraintLayout background;
    private Snackbar resultsSnackbar;
    private ConstraintLayout shareListHideContainer;
    private CardView mainCardContainer;
    private CoordinatorLayout snackbarCoord;

    private TextView sharedWith_tv;
    private ListView sharedWith_lv;
    private ImageButton sharedWithCard_hideBtn;

    private TextView searchField_tv;
    private FloatingActionButton searchBtn;
    private RecyclerView foundUsers_rv;
    private ShareListItemAdapter sharedWithAdapter;

    private UserHandler userStorage;
    private ListHandler listStorage;
    private AWSUserService awsUserService;
    private AWSListService awsListService;
    private AWSInvitationService awsInvitationService;

    private BaseList editingList;
    private List<User> usersInCache;

    private Set<String> invitationReceivers;

    private ShareListSearchUserRecyclerAdapter<User> recyclerAdapter;

    private String SEARCHINPUT;

    private final static int CARD_HEIGHT = 150;

    private static final String TAG = "ShareListActivity";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String tag = getIntent().getStringExtra(IntentConstants.INTENT_MESSAGE);
        userStorage = UserHandlerFactory.getUserHandler(getApplicationContext());
        awsUserService = AWSServiceFactory.getAWSUserService();
        listStorage = ListHandlerFactory.getListHandler(getApplicationContext());
        awsListService = AWSServiceFactory.getAWSServiceDynamoDB(getApplicationContext(),this);
        awsInvitationService = AWSServiceFactory.getAWSInvitationsService(getApplicationContext(),this);
        invitationReceivers = new HashSet<>();

        try {
            editingList = listStorage.getListByTag(tag);
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        setStyle(editingList.getCategoryType());
        setContentView(R.layout.activity_share_list);

        shareListHideContainer = (ConstraintLayout) findViewById(R.id.shareListHideContainer);
        mainCardContainer = (CardView) findViewById(R.id.shareListSharingCard);
        background = (ConstraintLayout) findViewById(R.id.shareListBackground);
        snackbarCoord = (CoordinatorLayout) findViewById(R.id.shareListSnackbarCoord);

        sharedWith_tv = (TextView) findViewById(R.id.shareListIsSharedWith_tv);
        sharedWith_tv.setText(getString(R.string.share_list_list_is_shared_with,tag));
        sharedWith_lv = (ListView) findViewById(R.id.shareListSharedWithList);
        sharedWithCard_hideBtn = (ImageButton) findViewById(R.id.shareListHideCardBtn);

        searchField_tv = (TextView) findViewById(R.id.shareListSearch_tv);
        searchBtn = (FloatingActionButton) findViewById(R.id.shareListSearchBtn);
        foundUsers_rv = (RecyclerView) findViewById(R.id.shareListUserRecyclerView);

        NoInternetSnackbar.initialize(getApplicationContext(),snackbarCoord);
        if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
               NoInternetSnackbar.show();
        }



        setupActionBar(tag);
        setupSharedWithListView();
        setupHideSharedWithBtn();
        initateFoundUsersRecyclerView();
        setupSearchField();
        setupSearchButton();

        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(SEARCHINPUT, searchField_tv.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        searchField_tv.setText(savedInstanceState.getString(SEARCHINPUT));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_share_list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), DisplayListActivity.class);
        intent.putExtra(IntentConstants.INTENT_MESSAGE, editingList.getTag());
        intent.putExtra(IntentConstants.INTENT_CATEGORY, editingList.getCategory());

        switch(item.getItemId()){
            case android.R.id.home:
                Toast.makeText(getApplicationContext(),getString(R.string.share_list_no_changes_saved),Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;
            case R.id.shareListChangesDone_menuItem:
                confirmChanges();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar(String title) {
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        try {
            actionBar.setTitle(title);
        }catch(NullPointerException npe){
            Log.d(TAG, "setupActionBar: No title available for this list. Defaulting to \"List\"");
            actionBar.setTitle("List");
        }
    }

    private void setStyle(ListCategory categoryType) {
        switch(categoryType){
            case SHOPPING:
                super.setTheme(R.style.ShoppingTheme);
                break;
            case TODO:
                super.setTheme(R.style.TodoTheme);
                break;
            case WISHLIST:
                super.setTheme(R.style.WishlistTheme);
                break;
            default:
                break;
        }
    }

    private void setupHideSharedWithBtn() {
        sharedWithCard_hideBtn.setOnClickListener(view -> {
            if(DrawableComparator.compareDrawable(sharedWithCard_hideBtn.getDrawable(),
                    getDrawable(R.drawable.ic_arrow_drop_down_light_24dp))){
                showCardView();
            }else{
                hideCardView();
            }
            switchHideBtnIcon();
        });
    }

    private void hideCardView() {
        sharedWith_lv.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = mainCardContainer.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        mainCardContainer.setLayoutParams(params);
    }

    private void showCardView() {
        sharedWith_lv.setVisibility(View.VISIBLE);
        // Below needed because of the constant value 150 on the height of each card
        ViewGroup.LayoutParams params = mainCardContainer.getLayoutParams();
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                CARD_HEIGHT,
                getResources().getDisplayMetrics());
        mainCardContainer.setLayoutParams(params);
    }

    private void switchHideBtnIcon() {
        Drawable icon = DrawableComparator.compareDrawable(sharedWithCard_hideBtn.getDrawable(),getDrawable(R.drawable.ic_arrow_drop_down_light_24dp))
                ? getDrawable(R.drawable.ic_arrow_drop_up_light_24dp)
                : getDrawable(R.drawable.ic_arrow_drop_down_light_24dp);
        sharedWithCard_hideBtn.setImageDrawable(icon);
    }



    private void setupSharedWithListView() {
        List<User> users = editingList.getSharedUsers();
        sharedWithAdapter = new ShareListItemAdapter<>(getApplicationContext(), R.layout.activity_share_list_share_with_item, users);
        sharedWith_lv.setAdapter(sharedWithAdapter);
    }

    @SuppressWarnings("unchecked")
    private void updateSharedWithListView(final User user){
        sharedWithAdapter.addUser(user);
    }

    private void initateFoundUsersRecyclerView() {
        usersInCache = new ArrayList<>();
        try{
            usersInCache = userStorage.getAllCachedUsers();
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }

        recyclerAdapter = new ShareListSearchUserRecyclerAdapter<>(getApplicationContext(),
                R.layout.activity_share_list_search_recycler_item, usersInCache);
        recyclerAdapter.setTheme(editingList.getCategoryType());
        recyclerAdapter.setShareListHandler(this);
        foundUsers_rv.setAdapter(recyclerAdapter);
        foundUsers_rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }



    private void setupSearchField(){
        searchField_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @SuppressLint("StringFormatMatches")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()>2){
                    String filter = charSequence.toString();
                    List<User> filteredUsers = usersFromLocalStorageFiltered(filter);
                    if(filteredUsers != null && filteredUsers.size()>0){
                        recyclerAdapter.clear();
                        recyclerAdapter.addAll(filteredUsers);
                        String multipleResults = filteredUsers.size() > 1 ? "s" : "";
                        showResultsSnackBar(getString(R.string.share_list_found_users_snackbar_msg, String.valueOf(filteredUsers.size()), multipleResults), Snackbar.LENGTH_SHORT);
                    }else{
                        recyclerAdapter.clear();
                        showResultsSnackBar(getString(R.string.share_list_no_users_found_snackbar_msg), Snackbar.LENGTH_INDEFINITE);
                    }
                }else if(charSequence.length()==0){
                    List<User> allUsers = allUsersFromLocalStorage();
                    if(allUsers != null){
                        recyclerAdapter.clear();
                        recyclerAdapter.addAll(allUsers);
                        hideResultsSnackBar();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //
            }
        });
    }

    private void setupSearchButton(){
        searchBtn.setOnClickListener(v -> {
            String searchValue = searchField_tv.getText().toString();
            if(searchValue.length()>0){
                if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
                    NoInternetSnackbar.show();
                }else{
                    NoInternetSnackbar.dismiss();
                    search(searchValue);
                }
            }
        });
    }

    private void search(String searchValue){
        KeyboardUtils.hideKeyboard(this);
        hideResultsSnackBar();
        awsUserService.getUser(searchValue,getApplicationContext());
    }


    private List<User> usersFromLocalStorageFiltered(String filter){
        List<User> usersFromLocalStorage = null;
        try{
            usersFromLocalStorage = userStorage.getAllCachedUsersByName(filter);
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        return usersFromLocalStorage;
    }

    private List<User> allUsersFromLocalStorage(){
        List<User> usersFromLocalStorage = null;
        try{
            usersFromLocalStorage = userStorage.getAllCachedUsers();
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        return usersFromLocalStorage;
    }

    private void showResultsSnackBar(String msg, int duration){
        resultsSnackbar = Snackbar.make(background, msg, duration);
        resultsSnackbar.show();
    }

    private void hideResultsSnackBar(){
        if(resultsSnackbar!=null) {
            resultsSnackbar.dismiss();
        }
    }


    @Subscribe
    public void onEvent(AWSEvent event){
        EventHandler.Handler successHandler = defaultSuccessHandler -> Log.d(TAG, "onEvent: No eventhandler attached");
        EventHandler.Handler failureHandler = defaultFailureHandler -> Log.d(TAG, "onEvent: No eventhandler attached");

        switch(event.getClass().getSimpleName()){

            case EventName.DYNAMODB_LOAD_USER_FINISHED_EVENT:
                DynamoDBLoadUserFinishedEvent loadUserFinishedEvent = (DynamoDBLoadUserFinishedEvent) event;
                successHandler = e -> {
                    Toast userFoundToast = Toast.makeText(getApplicationContext(),
                            loadUserFinishedEvent.getMessage(),
                            Toast.LENGTH_SHORT);
                    userFoundToast.show();
                    storeUserLocally(loadUserFinishedEvent.getUser());
                    recyclerAdapter.clear();
                    recyclerAdapter.addAll(Collections.singletonList(loadUserFinishedEvent.getUser()));
                };
                failureHandler = e -> Toast.makeText(getApplicationContext(), loadUserFinishedEvent.getMessage(), Toast.LENGTH_LONG).show();
                break;

            case EventName.DYNAMODB_ADD_LIST_FINISHED_EVENT:
                DynamoDBAddListFinishedEvent addListFinishedEvent = (DynamoDBAddListFinishedEvent) event;
                successHandler = e -> Log.d(TAG, "onEvent: List was successfully added");
                failureHandler = e -> Toast.makeText(getApplicationContext(),
                        addListFinishedEvent.getMessage(),
                        Toast.LENGTH_SHORT).show();
                break;

            case EventName.DYNAMODB_INVITATION_SENT_EVENT:
                DynamoDBInvitationSentEvent sentEvent = (DynamoDBInvitationSentEvent) event;
                successHandler = e -> {
                    // Clearing the previously holded invitations
                    // and starting DisplayListActivity
                    InvitationsHelper.invitationsToUpload.clear();
                    Toast.makeText(getApplicationContext(),getString(R.string.share_list_changes_saved),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), DisplayListActivity.class);
                    intent.putExtra(IntentConstants.INTENT_MESSAGE, editingList.getTag());
                    intent.putExtra(IntentConstants.INTENT_CATEGORY, editingList.getCategory());
                    startActivity(intent);
                };
                // TODO display error message to user, prompting for action
                failureHandler = e -> {
                    InvitationsHelper.invitationsToUpload.clear();
                    Log.d(TAG, "onEvent: Failed to send events");
                };
                break;
        }

        event.setHandler(event.isSuccess() ? successHandler : failureHandler);

        if(event.getHandler()!=null) {
            EventHandler.handle(event);
        }
    }

    private void storeUserLocally(final User user){
        try{
            userStorage.storeUser(user);
        }catch(StorageEditException see){
            see.printStackTrace();
        }
    }

    private void updateListToStorage(){
        try{
            listStorage.editListToStorage(editingList);
        }catch(StorageEditException see){
            see.printStackTrace();
        }
    }

    private void updateListToOnlineStorage(){
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
            NoInternetSnackbar.dismiss();
            awsListService.editList(editingList);
        }else{
            NoInternetSnackbar.show();
        }
    }

    private void sendInvitations(){
        Set<ListInvitation> invitationsToSend = new HashSet<>();
        invitationReceivers.forEach(receiver -> {
            ListInvitation invitationItem = new ListInvitation.Builder().id(editingList.getId())
                    .listName(editingList.getTag())
                    .inviteHandled(false)
                    .receiver(receiver)
                    .sender(CognitoSession.CURRENT_USER_INAPP.getUsername())
                    .timeOfCreation(editingList.getDateCreated())
                    .build();
            invitationsToSend.add(invitationItem);
            Log.d(TAG, "sendInvitations: Sending invite : "+invitationItem);
        });

        Toast.makeText(getApplicationContext(),
                R.string.share_list_sending_invitations,
                Toast.LENGTH_SHORT).
                show();
        awsInvitationService.sendInvitations(invitationsToSend);

    }




    @SuppressWarnings("unchecked")
    @Override
    public void addUser(final User user) {
        Predicate<User> existingUser = u -> u.getUsername().equals(user.getUsername());
        int existing = ((List<User>) sharedWithAdapter
                .getUsers()
                .stream()
                .filter(existingUser)
                .collect(Collectors.toList()))
                .size();
        if(existing==0) {
            updateSharedWithListView(user);
            invitationReceivers.add(user.getUsername());
        }else{
            Toast alreadyAddedToast = Toast.makeText(getApplicationContext(),
                    getString(R.string.share_list_user_already_added_msg, user.getUsername()),
                    Toast.LENGTH_SHORT);
            alreadyAddedToast.show();
        }
    }

    @Override
    public boolean userExistsInList(User user) {
        // Not implemented
        return false;
    }

    @SuppressWarnings("unchecked")
    private void confirmChanges(){
        editingList.setSharedUsers((List<User>) sharedWithAdapter.getUsers());
        updateListToStorage();
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
            updateListToOnlineStorage();
            sendInvitations();
        }
    }

    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        // TODO
    }

    @Override
    public Context onRefreshContext() {
        return getApplicationContext();
    }
}

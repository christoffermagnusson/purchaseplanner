package com.magnusson_dev.purchaseplanner;


import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import java.util.HashSet;
import java.util.Set;

public class InvitationsHelper {

    private InvitationsHelper(){}

    static Set<ListInvitation> downloadedInvitations = new HashSet<>();

    public static Set<ListInvitation> invitationsToUpload = new HashSet<>();

    public static ListInvitation singleInvitationToUpdate = new ListInvitation();

    public static ListInvitation handledInvitation = new ListInvitation();


}

package com.magnusson_dev.purchaseplanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.services.cognitoidentityprovider.model.AliasExistsException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoActivation;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoAccountActivationEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.validators.Validator;
import com.magnusson_dev.purchaseplanner.utils.validators.ValidatorFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class ConfirmRegistrationActivity extends AppCompatActivity implements ICallback{

    private TextView confirmRegistrationMessage_tv;
    private String username;

    private static String ACTIVATION_CODE= "ACTIVATION_CODE";

    private static final String TAG = "ConfirmRegistrationActi";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_registration);
        if(getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
        EventBus.getDefault().register(this);

        username = getIntent().getStringExtra(IntentConstants.USERNAME);
        confirmRegistrationMessage_tv = (TextView) findViewById(R.id.confirmRegistrationMessage);
        String email = getIntent().getStringExtra(IntentConstants.EMAIL);
        String confirmHint = email==null
                ? String.format("%s %s %s", getString(R.string.confirm_registration_message_pt1),"you",getString(R.string.confirm_registration_message_pt2))
                : String.format("%s %s %s",getString(R.string.confirm_registration_message_pt1),email,getString(R.string.confirm_registration_message_pt2));

        confirmRegistrationMessage_tv.setText(confirmHint);

        final TextView confirmRegistrationInput_tv = (TextView) findViewById(R.id.confirmRegistrationInput);
        Button confirmRegistrationSubmitBtn = (Button) findViewById(R.id.confirmRegistrationSubmitBtn);
        confirmRegistrationSubmitBtn.setOnClickListener(view -> {
            Log.d(TAG, "onCreate: CLICKED CONFIRM BUTTON");
            String activationCode = confirmRegistrationInput_tv.getText().toString();
            if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                if (validate(activationCode)) {
                    submit(activationCode);
                }
            }else{
                Toast noInternetToast = Toast.makeText(getApplicationContext(),
                                        getString(R.string.confirm_registration_no_internet),
                                        Toast.LENGTH_LONG);
                noInternetToast.show();
            }
        });

    }




    @Override
    protected void onStop() {
        super.onStop();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(ACTIVATION_CODE, confirmRegistrationMessage_tv.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        confirmRegistrationMessage_tv.setText(savedInstanceState.getString(ACTIVATION_CODE));
    }

    private boolean validate(final String activationCode) {
        Validator activationCodeValidator = ValidatorFactory.createActivationCodeValidator(getApplicationContext());
        return activationCodeValidator.validate(activationCode);
    }

    private void submit(final String activationCode) {
        CognitoUserPool pool = CognitoUserPoolCreator.createUserPool(getApplicationContext());
        CognitoUser user = pool.getUser(username);
        CognitoActivation.activateAccount(user, activationCode);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AWSEvent event){
        Object eventClass = event.getClass();
        EventHandler.Handler successHandler;
        EventHandler.Handler failureHandler;
        if(eventClass.equals(CognitoAccountActivationEvent.class)){
            CognitoAccountActivationEvent activationEvent = (CognitoAccountActivationEvent) event;
            successHandler = e -> {
                Toast successToast = Toast.makeText(getApplicationContext(),getString(R.string.confirm_activation_success_toast),Toast.LENGTH_SHORT);
                successToast.show();
                onStartNewActivity(ActivityType.MAIN_ACTIVITY,null,null);
            };
            failureHandler = e -> {
                Toast failureToast = Toast.makeText(getApplicationContext(),
                                                    activationEvent.getMessage(),
                                                    Toast.LENGTH_SHORT);
                failureToast.show();
                Log.d(TAG, "onEvent: Failure "+activationEvent.getMessage());
                if(activationEvent.getErrorCause().getClass().equals(AliasExistsException.class)) {
                    onStartNewActivity(ActivityType.REGISTER_ACTIVITY, null, null);
                }
            };
            event.setHandler(event.isSuccess() ? successHandler : failureHandler);
        }
        if(event.getHandler()!=null) {
            EventHandler.handle(event);
        }
    }


    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        Intent intent;
        switch(activityType){
            case MAIN_ACTIVITY:
                Log.d(TAG, "onStartNewActivity: Starting MainActivity");
                intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                break;
            case REGISTER_ACTIVITY:
                Log.d(TAG, "onStartNewActivity: Starting RegisterActivity");
                intent = new Intent(getApplicationContext(),RegistrationActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public Context onRefreshContext() {
        return getApplicationContext();
    }
}

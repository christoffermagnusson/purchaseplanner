package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import java.util.Set;

public class DynamoDBPendingInvitationsFinishedEvent extends AWSEvent {

    private Set<ListInvitation> pendingInvitations;

    public DynamoDBPendingInvitationsFinishedEvent(boolean success) {
        super(success);
    }

    public Set<ListInvitation> getPendingInvitations() {
        return pendingInvitations;
    }

    public void setPendingInvitations(Set<ListInvitation> pendingInvitations) {
        this.pendingInvitations = pendingInvitations;
    }
}

package com.magnusson_dev.purchaseplanner.event;



public class EventName {

    public static final String GET_REQUEST_RESPONSE_EVENT                 = "GETRequestResponseEvent";
    public static final String DYNAMODB_REMOVED_LIST_EVENT                = "DynamoDBRemovedListEvent";
    public static final String DYNAMODB_READ_LIST_BY_ID_EVENT             = "DynamoDBReadListByIdEvent";
    public static final String DYNAMODB_READ_ALL_LISTS_EVENT              = "DynamoDBReadAllListsEvent";
    public static final String DYNAMODB_NEXT_ID_GENERATED_EVENT           = "DynamoDBNextIdGeneratedEvent";
    public static final String DYNAMODB_LOAD_USER_FINISHED_EVENT          = "DynamoDBLoadUserFinishedEvent";
    public static final String DYNAMODB_ADD_USER_EVENT                    = "DynamoDBAddUserEvent";
    public static final String DYNAMODB_ADD_LIST_FINISHED_EVENT           = "DynamoDBAddListFinishedEvent";
    public static final String DYNAMODB_ACCESS_TO_LIST_REMOVED_EVENT      = "DynamoDBAccessToListRemovedEvent";
    public static final String DYNAMODB_PENDING_INVITATIONS_FINISHED_EVENT= "DynamoDBPendingInvitationsFinishedEvent";
    public static final String DYNAMODB_INVITATION_SENT_EVENT             = "DynamoDBInvitationSentEvent";
    public static final String DYNAMODB_INVITATION_ACCEPTED_EVENT         = "DynamoDBInvitationAcceptedEvent";
    public static final String DYNAMODB_INVITATION_DECLINED_EVENT         = "DynamoDBInvitationDeclinedEvent";
    public static final String COGNITO_USER_SESSION_CREATED_EVENT         = "CognitoUserSessionCreatedEvent";
    public static final String COGNITO_USER_DETAILS_CREATED_EVENT         = "CognitoUserDetailsCreatedEvent";
    public static final String COGNITO_REGISTRATION_EVENT                 = "CognitoRegistrationEvent";
    public static final String COGNITO_CREDENTIALS_PROVIDER_CREATED_EVENT = "CognitoCredentialsProviderCreatedEvent";
    public static final String COGNITO_ACCOUNT_ACTIVATION_EVENT           = "CognitoAccountActivationEvent";


}

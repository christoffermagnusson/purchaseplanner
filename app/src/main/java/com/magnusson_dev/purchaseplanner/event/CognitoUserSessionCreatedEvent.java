package com.magnusson_dev.purchaseplanner.event;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;

/**
 * An event representing when a session against a Cognito user pool
 * is created. If the session is created successfully this event
 * will have the attached CognitoUserSession. If not , an Exception
 * will be sent through with a message on the cause of error
 */
public class CognitoUserSessionCreatedEvent extends CognitoEvent{

    private CognitoUserSession userSession;
    private Exception exception;


    public CognitoUserSessionCreatedEvent(final boolean success){
        super(success);
    }

    public void setUserSession(CognitoUserSession userSession) {
        this.userSession = userSession;
    }


    public CognitoUserSession getUserSession() {
        return userSession;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}

package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.BaseList;

/**
 * An event containing a list that has been added and initiated with
 * an unique ID in DynamoDB. If this list has been configured correctly then
 * it is added to local storage by the subscriber of this event.
 */
public class DynamoDBAddListFinishedEvent extends DynamoDBEvent {


    private BaseList addedList;

    public DynamoDBAddListFinishedEvent(boolean success) {
        super(success);
    }

    public BaseList getAddedList() {
        return addedList;
    }

    public void setAddedList(BaseList addedList) {
        this.addedList = addedList;
    }
}

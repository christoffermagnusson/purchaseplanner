package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.BaseList;

import java.util.List;

/**
 * An event that is sent when all lists related to
 * the current user are fetched from DynamoDB.
 * If the fetching was successful a List of
 * BaseLists are attached to the event.
 */
public class DynamoDBReadAllListsEvent extends DynamoDBEvent {

    private List<BaseList> fetchedLists;

    public DynamoDBReadAllListsEvent(boolean success) {
        super(success);
    }

    public List<BaseList> getFetchedLists() {
        return fetchedLists;
    }

    public void setFetchedLists(List<BaseList> fetchedLists) {
        this.fetchedLists = fetchedLists;
    }
}

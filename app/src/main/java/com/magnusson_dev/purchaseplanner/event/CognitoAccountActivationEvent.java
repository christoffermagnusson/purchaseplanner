package com.magnusson_dev.purchaseplanner.event;


/**
 * Event related to the activation of a new user account
 * Contains an Exception attribute that can be attached
 * when an error occurred during activation
 */
public class CognitoAccountActivationEvent extends CognitoEvent {

    private Exception errorCause;

    public CognitoAccountActivationEvent(boolean success) {
        super(success);
    }

    public Exception getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(Exception errorCause) {
        this.errorCause = errorCause;
    }
}

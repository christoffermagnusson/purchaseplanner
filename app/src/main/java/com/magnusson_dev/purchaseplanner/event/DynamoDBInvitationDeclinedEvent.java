package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.ListInvitation;

public class DynamoDBInvitationDeclinedEvent extends AWSEvent {

    private ListInvitation declinedInvitation;

    public DynamoDBInvitationDeclinedEvent(boolean success) {
        super(success);
    }

    public ListInvitation getDeclinedInvitation() {
        return declinedInvitation;
    }

    public void setDeclinedInvitation(ListInvitation declinedInvitation) {
        this.declinedInvitation = declinedInvitation;
    }
}

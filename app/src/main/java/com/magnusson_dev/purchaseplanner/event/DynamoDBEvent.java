package com.magnusson_dev.purchaseplanner.event;


/**
 * A subclass of AWSEvent and an abstraction from it.
 * Used by DynamoDB related events
 */
public abstract class DynamoDBEvent extends AWSEvent {



    public DynamoDBEvent(final boolean success){
        super(success);
    }


}

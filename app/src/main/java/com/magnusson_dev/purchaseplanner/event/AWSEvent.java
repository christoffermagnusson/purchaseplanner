package com.magnusson_dev.purchaseplanner.event;


/**
 * The superclass of all events in PurchasePlanner. This event contains basic success attribute and message attribute
 */
public abstract class AWSEvent {

    /**
     * Attribute used to determine if an event was considered
     * a success or a failure.
     */
    private boolean success;

    /**
     * Attribute used for communicating for example what went wrong
     * in a failing event.
     */
    private String message;


    /**
     * Attribute used when a failure occurs. To be handled by caller.
     */
    private Exception exception;

    /**
     * Attribute used to handle a specific event
     */
    private EventHandler.Handler handler;

    public AWSEvent(final boolean success){
        this.success=success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setHandler(EventHandler.Handler handler){ this.handler=handler; }

    public EventHandler.Handler getHandler(){ return this.handler; }

    public Exception getException() { return exception;}

    public void setException(Exception exception) {this.exception = exception;}
}

package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.BaseList;

/**
 * An event to let the app know it is okay to delete a list from local storage since
 * it has been removed from DynamoDB. The attribute listToDelete is the list to be
 * deleted from local storage by the subscriber of this event.
 */
public class DynamoDBAccessToListRemovedEvent extends DynamoDBEvent {

    private BaseList listToDelete;

    public DynamoDBAccessToListRemovedEvent(boolean success) {
        super(success);
    }

    public BaseList getListToDelete() {
        return listToDelete;
    }

    public void setListToDelete(BaseList listToDelete) {
        this.listToDelete = listToDelete;
    }
}

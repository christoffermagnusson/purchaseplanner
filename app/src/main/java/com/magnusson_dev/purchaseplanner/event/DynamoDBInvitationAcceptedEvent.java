package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.ListInvitation;

public class DynamoDBInvitationAcceptedEvent extends AWSEvent {

    private ListInvitation acceptedInvitation;

    public DynamoDBInvitationAcceptedEvent(boolean success) {
        super(success);
    }

    public ListInvitation getAcceptedInvitation() {
        return acceptedInvitation;
    }

    public void setAcceptedInvitation(ListInvitation acceptedInvitation) {
        this.acceptedInvitation = acceptedInvitation;
    }
}

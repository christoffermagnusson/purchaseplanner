package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.BaseList;

public class DynamoDBRemovedListEvent extends DynamoDBEvent {

    private BaseList deletedList;

    public DynamoDBRemovedListEvent(boolean success) {
        super(success);
    }

    public BaseList getDeletedList() {
        return deletedList;
    }

    public void setDeletedList(BaseList deletedList) {
        this.deletedList = deletedList;
    }
}

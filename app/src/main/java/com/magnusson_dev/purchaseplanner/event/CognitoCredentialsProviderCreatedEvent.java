package com.magnusson_dev.purchaseplanner.event;


import com.amazonaws.auth.CognitoCachingCredentialsProvider;

/**
 * An event bearing a created CognitoCredentialsProvider if
 * successful. This provider can then be used to provide access to
 * different AWS services such as DynamoDB or S3.
 */
public class CognitoCredentialsProviderCreatedEvent extends CognitoEvent {

    private CognitoCachingCredentialsProvider provider;


    public CognitoCredentialsProviderCreatedEvent(final boolean success){
        super(success);
    }

    public CognitoCachingCredentialsProvider getProvider() {
        return provider;
    }

    public void setProvider(CognitoCachingCredentialsProvider provider) {
        this.provider = provider;
    }

}

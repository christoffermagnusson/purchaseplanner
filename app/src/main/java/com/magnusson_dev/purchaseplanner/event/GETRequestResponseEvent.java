package com.magnusson_dev.purchaseplanner.event;

/**
 * An event used to indicate if a GET request is
 * considered finished against the AWS S3 service
 */
public class GETRequestResponseEvent extends AWSEvent{

    private boolean isRequestFinished;


    public GETRequestResponseEvent(boolean isRequestFinished) {
        super(isRequestFinished);
        this.isRequestFinished = isRequestFinished;
    }

    public boolean isRequestFinished() {
        return isRequestFinished;
    }
}

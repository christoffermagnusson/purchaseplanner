package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.User;

/**
 * An event that is sent after a search for a user
 * is completed in DynamoDB. If the user exists, then a
 * User object is attached to the event.
 */
public class DynamoDBLoadUserFinishedEvent extends DynamoDBEvent {

    private User user;

    public DynamoDBLoadUserFinishedEvent(boolean success) {
        super(success);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

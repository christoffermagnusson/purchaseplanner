package com.magnusson_dev.purchaseplanner.event;


/**
 * A subclass of AWSEvent and an abstraction from it.
 * Used by Cognito related events
 */
public abstract class CognitoEvent extends AWSEvent{



    public CognitoEvent(final boolean success){
        super(success);
    }



}

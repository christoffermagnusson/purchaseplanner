package com.magnusson_dev.purchaseplanner.event;



public class DynamoDBInvitationSentEvent extends AWSEvent {

    public DynamoDBInvitationSentEvent(boolean success) {
        super(success);
    }
}

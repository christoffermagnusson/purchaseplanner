package com.magnusson_dev.purchaseplanner.event;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;

/**
 * An event containing the state of a newly registered user.
 * If the event is successful then the created CognitoUser object
 * will be attached to the event.
 */
public class CognitoRegistrationEvent extends CognitoEvent {


    private CognitoUser user;

    public CognitoRegistrationEvent(final boolean success) {
        super(success);
    }

    public CognitoUser getUser() {
        return user;
    }

    public void setUser(CognitoUser user) {
        this.user = user;
    }
}

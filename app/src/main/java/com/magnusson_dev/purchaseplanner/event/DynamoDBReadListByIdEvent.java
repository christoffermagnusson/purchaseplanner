package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.BaseList;

/**
 * An event sent after a search has been performed for a list
 * in DynamoDB. If the list was found, it will be attached to
 * the event.
 */
public class DynamoDBReadListByIdEvent extends DynamoDBEvent {

    private BaseList baseList;
    private Exception exception;

    public DynamoDBReadListByIdEvent(boolean success) {
        super(success);
    }

    public BaseList getBaseList() {
        return baseList;
    }

    public void setBaseList(BaseList baseList) {
        this.baseList = baseList;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}

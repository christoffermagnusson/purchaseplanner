package com.magnusson_dev.purchaseplanner.event;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;

/**
 * An event containing details about a specific user. If the event is
 * considered successful then a CognitoUserDetails object will be attached.
 */
public class CognitoUserDetailsCreatedEvent extends CognitoEvent {


    private CognitoUserDetails userDetails;



    public CognitoUserDetailsCreatedEvent(final boolean success){
        super(success);
    }


    public CognitoUserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(CognitoUserDetails userDetails) {
        this.userDetails = userDetails;
    }
}

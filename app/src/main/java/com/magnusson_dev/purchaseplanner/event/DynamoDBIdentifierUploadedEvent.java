package com.magnusson_dev.purchaseplanner.event;



public class DynamoDBIdentifierUploadedEvent extends AWSEvent {
    public DynamoDBIdentifierUploadedEvent(boolean success) {
        super(success);
    }
}

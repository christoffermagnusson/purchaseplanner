package com.magnusson_dev.purchaseplanner.event;


/**
 * An event containing the next unique identifier
 * generated in DynamoDB to be used for a new list.
 * The attribute nextId is then attached to new list
 * that is to be uploaded.
 */
public class DynamoDBNextIdGeneratedEvent extends DynamoDBEvent {

    private int nextId;

    public DynamoDBNextIdGeneratedEvent(boolean success) {
        super(success);
    }

    public int getNextId() {
        return nextId;
    }

    public void setNextId(int nextId) {
        this.nextId = nextId;
    }
}

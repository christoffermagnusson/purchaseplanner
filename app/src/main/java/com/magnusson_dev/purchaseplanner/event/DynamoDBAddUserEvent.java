package com.magnusson_dev.purchaseplanner.event;



public class DynamoDBAddUserEvent extends AWSEvent {

    private Exception exception;

    public DynamoDBAddUserEvent(boolean success) {
        super(success);
    }

    public Exception exception(){
        return this.exception;
    }

    public void setException(Exception exception){
        this.exception = exception;
    }
}

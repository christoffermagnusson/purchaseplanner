package com.magnusson_dev.purchaseplanner.event;


import com.magnusson_dev.purchaseplanner.model.User;

import java.util.List;

public class SharedUsersUpdateEvent {

    private List<User> listOfUsersToUpdate;

    public SharedUsersUpdateEvent(List<User> listOfUsersToUpdate){
        this.listOfUsersToUpdate=listOfUsersToUpdate;
    }

    public List<User> getListOfUsersToUpdate() {
        return listOfUsersToUpdate;
    }
}

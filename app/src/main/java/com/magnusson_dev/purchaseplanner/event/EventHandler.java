package com.magnusson_dev.purchaseplanner.event;


public class EventHandler {



    public interface Handler{
        void handle(AWSEvent event);
    }

    public static void handle(AWSEvent event){
        Handler handler = event.getHandler();
        handler.handle(event);
    }



}

package com.magnusson_dev.purchaseplanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserSessionCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.exception.UserSessionTokensEmptyException;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserSessionCreatedEvent;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.SharedPreferencesWrapper;
import com.magnusson_dev.purchaseplanner.utils.ThemeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class WelcomePortalActivity extends AppCompatActivity implements ICallback{

    private static final String TAG = "WelcomePortalActivity";
    private CognitoUserPool userPool;
    private CognitoUser currentUser;
    private TextView loginMessage_tv;
    private ProgressBar loginProgress;
    private View mainView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtils.CURRENT_THEME = R.style.AppTheme;
        super.setTheme(ThemeUtils.CURRENT_THEME);
        setContentView(R.layout.activity_welcome_portal);
        EventBus.getDefault().register(this);

        CognitoConstants.callback = this;

        loginProgress = (ProgressBar) findViewById(R.id.portalLoginProgress);
        mainView = findViewById(R.id.portalMainview);

        loginMessage_tv = (TextView) findViewById(R.id.portalLoginMessage);
        loginMessage_tv.setText(getString(R.string.portal_login_attempt_msg));
        userPool = CognitoUserPoolCreator.createUserPool(getApplicationContext());
        currentUser = userPool.getCurrentUser();
        setupGUI();
        tryLogin();




    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Method that checks if there currently is a logged in user in SharedPreferences.
     * If not, prompt user to register or log in to existing account.
     * If exists, then try to fetch an online session with Cognito to validate against server.
     * Response is received through EventBus.
     */
    private void tryLogin() {
          if(currentUser.getUserId()==null){
            loginMessage_tv.setText(getString(R.string.portal_user_not_existing_in_local));
            // No stored user in SharedPreferences. Could happen if the user cleared the app cache
        }
        else{
              if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                  if(CognitoSession.USER_SESSION==null){
                      startSession();
                  }
              }else{
                  // Start offline session
                  startActivity(new Intent(getApplicationContext(),MainActivity.class));
              }
          }
    }
    
    private void startSession(){
        if(!startSessionFromCache()){
            startOnlineSession();   
        }
    }
    
    private boolean startSessionFromCache(){
        boolean started = false;
        try{
            CognitoUserSessionCreator.createUserSessionFromCache(getApplicationContext());
            started = true;
            Log.d(TAG, "startSessionFromCache: Started session from cache");
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }catch(UserSessionTokensEmptyException e){
            Log.d(TAG, "startSessionFromCache: "+e.getMessage());
        }
        return started;
    }
    
    private void startOnlineSession(){
        currentUser.getSessionInBackground(CognitoUserSessionCreator.createCognitoUserSessionAsync(currentUser));
        showProgressBar();
    }


    /**
     * Sets up the GUI components login button and register button
     */
    private void setupGUI() {
        getSupportActionBar().hide();
        Button loginBtn = (Button) findViewById(R.id.portalLoginBtn);
        loginBtn.setOnClickListener(view -> {
            Log.d(TAG, "onClick: Starting LoginActivity");
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        });

        Button registerBtn = (Button) findViewById(R.id.portalRegisterBtn);
        registerBtn.setOnClickListener(view -> {
            Log.d(TAG, "onClick: Starting new RegistrationActivity");
            startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
        });
    }

    private void showProgressBar(){
        loginProgress.setVisibility(View.VISIBLE);
        mainView.setAlpha(0.5f);
    }

    private void hideProgressBar(){
        loginProgress.setVisibility(View.INVISIBLE);
        mainView.setAlpha(1.0f);
    }


    /**
     * EventBus method for handling events. This activity subscribes on events regarding the
     * creation of an online user session and validation of a User.
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AWSEvent event){
        if(event.getClass().equals(CognitoUserSessionCreatedEvent.class)){
            CognitoUserSessionCreatedEvent userSessionCreatedEvent = (CognitoUserSessionCreatedEvent) event;
            if(userSessionCreatedEvent.isSuccess()){
                hideProgressBar();
                // Store tokens in sharedpreferences
                if(CognitoSession.USER_SESSION!=null){
                    SharedPreferencesWrapper.putString(CognitoConstants.COGNITO_ID_TOKEN,
                                                        CognitoSession.USER_SESSION.getIdToken().getJWTToken(),
                                                        getApplicationContext());

                    SharedPreferencesWrapper.putString(CognitoConstants.COGNITO_ACCESS_TOKEN,
                                                        CognitoSession.USER_SESSION.getAccessToken().getJWTToken(),
                                                        getApplicationContext());

                    SharedPreferencesWrapper.putString(CognitoConstants.COGNTIO_REFRESH_TOKEN,
                                                        CognitoSession.USER_SESSION.getRefreshToken().getToken(),
                                                        getApplicationContext());

                }
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
            else{
                Log.d(TAG, "onEvent: EXCEPTION = "+userSessionCreatedEvent.getException().getClass());
                hideProgressBar();
                handleLoginFailure(userSessionCreatedEvent);
            }

        }
    }


    /**
     * Method to inform the if something went wrong validating their account against the server.
     * Right now the method handles if an account has not yet been confirmed using the activation
     * code received in email. If not confirmed, then send user to ConfirmRegistrationActivity
     * and prompt for account activation
     * @param userSessionCreatedEvent
     */
    private void handleLoginFailure(CognitoUserSessionCreatedEvent userSessionCreatedEvent) {
        if(userSessionCreatedEvent.getException().getClass().equals(AmazonServiceException.class)){
            AmazonServiceException awsException = (AmazonServiceException) userSessionCreatedEvent.getException();
            switch (awsException.getErrorCode()){
                case "UserNotConfirmedException":
                    Toast userNotConfirmedToast = Toast.makeText(getApplicationContext(),
                            getString(R.string.login_user_not_confirmed_toast),
                            Toast.LENGTH_LONG);
                    userNotConfirmedToast.show();
                    Log.d(TAG, "handleLoginFailure: Starting new ConfirmRegistrationActivity");
                    Intent intent = new Intent(getApplicationContext(),ConfirmRegistrationActivity.class);
                    intent.putExtra(IntentConstants.USERNAME, currentUser.getUserId());
                    startActivity(intent);
                    break;

            }
        }
    }

    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {

    }

    @Override
    public Context onRefreshContext() {
        return getApplicationContext();
    }
}

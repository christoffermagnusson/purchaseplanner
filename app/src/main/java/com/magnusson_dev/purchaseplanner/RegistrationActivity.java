package com.magnusson_dev.purchaseplanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;
import com.amazonaws.services.cognitoidentityprovider.model.InvalidParameterException;
import com.amazonaws.services.cognitoidentityprovider.model.InvalidPasswordException;
import com.amazonaws.services.cognitoidentityprovider.model.UsernameExistsException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoRegistrationEvent;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;
import com.magnusson_dev.purchaseplanner.utils.validators.Validator;
import com.magnusson_dev.purchaseplanner.utils.validators.ValidatorFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class RegistrationActivity extends AppCompatActivity implements ICallback {

    private TextView username_tv;
    private TextView password_tv;
    private TextView givenName_tv;
    private TextView email_tv;
    private Button submitBtn;
    private CoordinatorLayout snackbarCoordinatorLayout;

    // need this on instance level to send through to next activity and storage on DynamoDB
    private String email;
    private String username;
    private String givenName;


    // instance state variables when rotating device
    private static String EMAIL = "EMAIL";
    private static String USERNAME = "USERNAME";
    private static String GIVEN_NAME = "GIVEN_NAME";
    private static String PASSWORD = "PASSWORD";

    private static final String TAG = "RegistrationActivity";




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().hide();
        }

        username_tv = (TextView) findViewById(R.id.registrationUsername_tv);
        username_tv.setOnEditorActionListener((v, keyCode, event)-> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                Log.d(TAG, "onCreate: Pressed enter");
                Toast t = Toast.makeText(this, "TEST",Toast.LENGTH_SHORT);
                t.show();
            }
            return false;
        });
        password_tv = (TextView) findViewById(R.id.registrationPassword_tv);
        givenName_tv = (TextView) findViewById(R.id.registrationGivenName_tv);
        email_tv = (TextView) findViewById(R.id.registrationEmail_tv);

        submitBtn = (Button) findViewById(R.id.registrationSubmitBtn);
        snackbarCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.registrationSnackbarCoord);
        NoInternetSnackbar.initialize(getApplicationContext(),snackbarCoordinatorLayout);
        if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.show();
        }


        submitBtn.setOnClickListener((v) -> {
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    NoInternetSnackbar.dismiss();
                    if(validate()) {
                        Log.d(TAG, "onClick: Registering to Cognito");
                        register();
                    }
                }else{
                    NoInternetSnackbar.show();
                }
        });

        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: Saving state!");
        outState.putString(USERNAME, username_tv.getText().toString());
        outState.putString(PASSWORD, password_tv.getText().toString());
        outState.putString(EMAIL, email_tv.getText().toString());
        outState.putString(GIVEN_NAME, givenName_tv.getText().toString());
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState: Restoring state!");
        super.onRestoreInstanceState(savedInstanceState);
        username_tv.setText(savedInstanceState.getString(USERNAME));
        password_tv.setText(savedInstanceState.getString(PASSWORD));
        email_tv.setText(savedInstanceState.getString(EMAIL));
        givenName_tv.setText(savedInstanceState.getString(GIVEN_NAME));
    }

    /**
     * Simple validation of registration form attributes. For now only
     * checks whether there is input in each required field.
     * @return
     */
    private boolean validate() {
        String username = username_tv.getText().toString();
        String givenName = givenName_tv.getText().toString();
        String email = email_tv.getText().toString();

        Validator usernameValidator = ValidatorFactory.createUsernameValidator(getApplicationContext());
        Validator givennameValidator = ValidatorFactory.createGivennameValidator(getApplicationContext());
        Validator emailValidator = ValidatorFactory.createEmailValidator(getApplicationContext());
        return usernameValidator.validate(username) && givennameValidator.validate(givenName) && emailValidator.validate(email);
    }





    /**
     * Registers a new User against the server. Gathers attributes from input and
     * starts up a background process against the Cognito service at AWS.
     * Whether success or failure, then fire of an event with information regarding
     * the registration
     */
    private void register() {
        CognitoUserAttributes attributes = new CognitoUserAttributes();

        username = username_tv.getText().toString();
        String password = password_tv.getText().toString();

        givenName = givenName_tv.getText().toString();
        email = email_tv.getText().toString();

        attributes.addAttribute("given_name",givenName);
        attributes.addAttribute("email",email);

        SignUpHandler signUpHandler = new SignUpHandler() {
            @Override
            public void onSuccess(CognitoUser user,
                                  boolean signUpConfirmationState,
                                  CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
                String confirmed = signUpConfirmationState ? "User is confirmed" : "User is not confirmed";
                Log.d(TAG, "onSuccess: "+confirmed);
                CognitoRegistrationEvent event = new CognitoRegistrationEvent(true);
                event.setUser(user);
                EventBus.getDefault().post(event);
            }

            @Override
            public void onFailure(Exception exception) {
                CognitoRegistrationEvent event = new CognitoRegistrationEvent(false);
                String errorMessage = generateErrorMessage(exception);
                exception.printStackTrace();
                event.setMessage(errorMessage);
                EventBus.getDefault().post(event);
            }
        };

        CognitoUserPool userPool = CognitoUserPoolCreator.createUserPool(getApplicationContext());
        userPool.signUpInBackground(username,password,attributes,null,signUpHandler);

    }

    /**
     * Generates error message regarding registration. This
     * generated message is then attached to the CognitoRegistration event
     * in case of failure.
     * @param exception
     * @return
     */
    private String generateErrorMessage(Exception exception) {
        String errorMessage = null;
        Object exClass = exception.getClass();
        if(exClass.equals(UsernameExistsException.class)){
            errorMessage = getString(R.string.registration_username_exists_err);
        }else if(exClass.equals(InvalidPasswordException.class)){
            errorMessage = getString(R.string.registration_password_conform_err);
        }else if(exClass.equals(InvalidParameterException.class)){
            errorMessage = getString(R.string.registration_invalid_params);
        }

        return errorMessage;
    }


    /**
     * EventBus method for handling incoming events. This activity handles events concerning the
     * registration of a new account.
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AWSEvent event){
        Log.d(TAG, "onEvent: Event received = "+event.getClass());
        if(event.getClass().equals(CognitoRegistrationEvent.class)){
            CognitoRegistrationEvent registrationEvent = (CognitoRegistrationEvent) event;
            if(registrationEvent.isSuccess()){
                // Store user on DynamoDB..
                clearInput();
                Toast successToast = Toast.makeText(getApplicationContext(),
                        getString(R.string.registration_success),
                        Toast.LENGTH_SHORT);
                successToast.show();
                onStartNewActivity(ActivityType.CONFIRM_REGISTRATION_ACTIVITY, email,null);
            }
            else{
                // Show whats needed to complete the form or what went wrong with the registration
                Toast errorToast = Toast.makeText(getApplicationContext(),registrationEvent.getMessage(),Toast.LENGTH_LONG);
                errorToast.show();
                Log.d(TAG, "onEvent: Failed to register || stack trace = "+registrationEvent.getMessage());
            }
        }
    }

    private void clearInput(){
        username_tv.setText("");
        password_tv.setText("");
        givenName_tv.setText("");
        email_tv.setText("");
    }

    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        switch(activityType){
            case CONFIRM_REGISTRATION_ACTIVITY:
                Intent intent = new Intent(getApplicationContext(),ConfirmRegistrationActivity.class);
                intent.putExtra(IntentConstants.EMAIL, intentMessage);
                intent.putExtra(IntentConstants.USERNAME, username);
                startActivity(intent);
                break;
        }
    }

    @Override
    public Context onRefreshContext() {
        return getApplicationContext();
    }
}

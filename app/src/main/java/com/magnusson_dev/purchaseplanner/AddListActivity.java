package com.magnusson_dev.purchaseplanner;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSListService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddListFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AddListActivity extends AppCompatActivity implements ICallback {

    private static final String TAG = "AddListActivity";

    private static final String NAME_INPUT = "Chosen name";
    private static final String CATEGORY_INPUT = "Chosen category";

    private EditText createNewListNameText;
    private Spinner categorySpinner;
    private TextView sharedWithList_tv;


    private ListHandler storageHandler;
    private AWSListService awsListService;

    private List<User> sharedUsers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: IN");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list);
        createNewListNameText = (EditText) findViewById(R.id.createNewListNameText);
        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        sharedWithList_tv = (TextView) findViewById(R.id.sharedWithList_tv);
        CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.addListSnackBarCoord);
        NoInternetSnackbar.initialize(getApplicationContext(), snackbarCoordinatorLayout);
        if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.show();
        }

        storageHandler = ListHandlerFactory.getListHandler(this);
        awsListService = AWSServiceFactory.getAWSServiceDynamoDB(getApplicationContext(),this);

        EventBus.getDefault().register(this);
        setupCategorySpinner();
        setupShareButton();
        setupActionBar(getString(R.string.add_list_activity_title));
        setSharedUsers(SharedUserUpdaterHelper.updatedUsers);

        setInput(SharedUserUpdaterHelper.listName, SharedUserUpdaterHelper.categorySelection);
        Log.d(TAG, "onCreate: OUT");

    }

    private void setInput(String listName, int selection) {
        if(listName!=null){
            createNewListNameText.setText(listName);
        }
        categorySpinner.setSelection(selection);
    }


    private void setupActionBar(String title) {
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_add_list_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.addListCreateMenuBtn:
                if(checkInput()) {
                    BaseList createdList = buildNewList();
                    Log.d(TAG, "onOptionsItemSelected: "+createdList);
                    addListToStorage(createdList);
                    if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                        NoInternetSnackbar.dismiss();
                        awsListService.addList(createdList);
                    }else{
                        Toast listNotAddedToServerToast = Toast.makeText(getApplicationContext(),
                                getString(R.string.add_list_not_added_to_server),
                                Toast.LENGTH_LONG);
                        listNotAddedToServerToast.show();
                    }

                    if(SharedUserUpdaterHelper.updatedUsers!=null) {
                        SharedUserUpdaterHelper.updatedUsers.clear();
                    }
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: IN");
        super.onStart();
        Log.d(TAG, "onStart: OUT");

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: Saving instance state");
        outState.putString(NAME_INPUT,createNewListNameText.getText().toString());
        outState.putInt(CATEGORY_INPUT, categorySpinner.getSelectedItemPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState: Restoring instance state");
        createNewListNameText.setText(savedInstanceState.getString(NAME_INPUT));
        categorySpinner.setSelection(savedInstanceState.getInt(CATEGORY_INPUT));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed(){
        if(SharedUserUpdaterHelper.updatedUsers!=null) {
            SharedUserUpdaterHelper.updatedUsers.clear();
        }
        onStartNewActivity(ActivityType.MAIN_ACTIVITY,"BACK_PRESSED_FROM_ADDLISTACTIVITY",null);
    }

    private void setupShareButton(){
        final Button shareBtn = (Button) findViewById(R.id.addActivityShareBtn);
        class ShareListener implements View.OnClickListener{
            private ICallback callback;

            private ShareListener(ICallback callback){
                this.callback=callback;
            }

            @Override
            public void onClick(View view) {
                callback.onStartNewActivity(ActivityType.SHARED_USER_LOOK_UP_ACTIVITY, null, null);
            }
        }
        shareBtn.setOnClickListener(new ShareListener(this));


    }


    private boolean checkInput(){
        if(createNewListNameText.getText().toString().equalsIgnoreCase("")){
            Toast noNameToast = Toast.makeText(this,getString(R.string.add_list_no_name_toast),Toast.LENGTH_LONG);
            noNameToast.show();
            return false;
        }
        return true;

    }

    private BaseList buildNewList() {
        String tag = createNewListNameText.getText().toString();
        String category = categorySpinner.getSelectedItem().toString();
        try {
            List<User> newListSharedUsers = new ArrayList<>();
            // Add currentUser
            newListSharedUsers.add(CognitoSession.CURRENT_USER_INAPP);
            if(sharedUsers!=null){
                newListSharedUsers.addAll(sharedUsers);
            }

            BaseList.Builder listBuilder = new BaseList.Builder().tag(tag)
                    .category(category)
                    .dateCreated(new Date())
                    .dateUpdated(new Date())
                    .listItems(new ArrayList<>())
                    .users(newListSharedUsers);

            BaseList createdList = FactoryMaker.getListFactory().buildList(listBuilder);

            Toast createdNewListToast = Toast.makeText(this,
                    getString(R.string.add_list_created_new_list_toast),
                    Toast.LENGTH_LONG);
            createdNewListToast.show();
            return createdList;
        } catch (NullPointerException npe) {
            npe.printStackTrace();
            return null;
        }
    }

    private void addListToStorage(BaseList list){
        try {
            storageHandler.addListToStorage(list);
        }catch(StorageEditException see){
            see.printStackTrace();
        }
    }

    private void setupCategorySpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.add_list_activity_spinner_items,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String category = categorySpinner.getSelectedItem().toString().toUpperCase();
                switch (category) {
                    case "SHOPPING":
                        AddListActivity.super.setTheme(R.style.ShoppingTheme);
                        break;
                    case "TODO":
                        AddListActivity.super.setTheme(R.style.TodoTheme);
                        break;
                    case "WISHLIST":
                        AddListActivity.super.setTheme(R.style.WishlistTheme);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }




    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        Intent intent;
        switch (activityType) {
            case DISPLAY_LIST_ACTIVITY:
                intent = new Intent(this, DisplayListActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                intent.putExtra(IntentConstants.INTENT_CATEGORY, category);
                SharedUserUpdaterHelper.listName = "";
                SharedUserUpdaterHelper.categorySelection = 0;
                Log.d(TAG, "onStartNewActivity: Starting new DisplayListActivity");
                startActivity(intent);
                break;
            case MAIN_ACTIVITY:
                intent = new Intent(this, MainActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                SharedUserUpdaterHelper.listName = "";
                SharedUserUpdaterHelper.categorySelection = 0;
                Log.d(TAG, "onStartNewActivity: Starting new MainActivity");
                startActivity(intent);
                break;
            case SHARED_USER_LOOK_UP_ACTIVITY:
                intent = new Intent(getApplicationContext(), SharedUserLookUpActivity.class);
                SharedUserUpdaterHelper.listName = createNewListNameText.getText().toString();
                SharedUserUpdaterHelper.categorySelection = categorySpinner.getSelectedItemPosition();
                Log.d(TAG, "onStartNewActivity: Starting new SharedUserLookUpActivity");
                startActivity(intent);
                break;
        }
    }

    @Override
    public Context onRefreshContext() {
        return this.getApplicationContext();
    }

    
    public void setSharedUsers(List<User> sharedUsers) {
        this.sharedUsers = sharedUsers;
        if(sharedUsers!=null) {
            sharedUsers.forEach(user -> {
                String oldSharedWithStr = sharedWithList_tv.getText().toString();
                sharedWithList_tv.setText(oldSharedWithStr.equalsIgnoreCase("")
                        ? user.getUsername()
                        : String.format("%s\n%s", oldSharedWithStr, user.getUsername()));
            });
        }
    }


    @Subscribe
    public void onEvent(AWSEvent event){
        Log.d(TAG, "onEvent: EventName "+event.getClass().getSimpleName());
        EventHandler.Handler handler = defaultHandler -> Log.d(TAG, "onEvent: No handler attached to event");

        switch(event.getClass().getSimpleName()){
            case EventName.DYNAMODB_ADD_LIST_FINISHED_EVENT:
                DynamoDBAddListFinishedEvent addListFinishedEvent = (DynamoDBAddListFinishedEvent) event;
                handler = getAddListFinishedEventHandler(addListFinishedEvent);
                break;
        }
        event.setHandler(handler);
        EventHandler.handle(event);

    }

    private EventHandler.Handler getAddListFinishedEventHandler(DynamoDBAddListFinishedEvent addListFinishedEvent) {
        return addListFinishedEvent.isSuccess()
                ? handler -> {
                    Log.d(TAG, "getAddListFinishedEventHandler: Added list, starting DisplayListActivity");
                    BaseList addedList = addListFinishedEvent.getAddedList();
                    addListToStorage(addedList);
                    onStartNewActivity(ActivityType.DISPLAY_LIST_ACTIVITY,
                            addedList.getTag(),
                            addedList.getCategoryType().toString());
                }
                : handler -> Log.d(TAG, "getAddListFinishedEventHandler: List couldn't be added. Try again later");

    }


}

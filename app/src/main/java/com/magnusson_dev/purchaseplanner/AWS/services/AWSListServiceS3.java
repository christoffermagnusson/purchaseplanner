package com.magnusson_dev.purchaseplanner.AWS.services;

import android.content.Context;

import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONService;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONServiceFactory;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.BaseList;

/**
 * An implementation of the AWSListService interface using AWS S3 buckets.
 * Also makes use of JSON reading, parsing and writing
 */
public class AWSListServiceS3 implements AWSListService {
    Context context;
    ICallback callback;
    JSONService jsonService;

    private static AWSListServiceS3 instance = null;

    AWSListServiceS3(Context context, ICallback callback){
        this.context=context;
        this.callback=callback;
        setup();
    }



    private void setup(){
        jsonService = JSONServiceFactory.getJSONServiceInstance(callback);
        jsonService.setContext(context);
    }

    @Override
    public void readAllLists() {
        jsonService.readJSON();
    }

    @Override
    public void readListById(BaseList list) {}

    @Override
    public void addList(BaseList list) {
        jsonService.writeList(list);
    }

    @Override
    public void editList(BaseList list) {
        jsonService.editList(list);
    }

    @Override
    public void deleteList(BaseList list) {
        jsonService.deleteList(list);
    }
}

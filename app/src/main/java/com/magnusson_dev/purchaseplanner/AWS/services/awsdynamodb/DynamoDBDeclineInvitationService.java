package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.utils.DynamoDBErrorConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.InvitationsHelper;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationDeclinedEvent;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;


public class DynamoDBDeclineInvitationService extends Service {


    private DynamoDBMapper objectMapper;

    private static final String TAG = "DynamoDBDeclineInvitati";

    private static boolean retryDone;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

        declineInvitation(InvitationsHelper.singleInvitationToUpdate);

        return Service.START_NOT_STICKY;
    }

    private void declineInvitation(ListInvitation singleInvitationToUpdate) {
        class InvitationDecliner extends AsyncTask<ListInvitation,Void,DynamoDBInvitationDeclinedEvent>{

            @Override
            protected DynamoDBInvitationDeclinedEvent doInBackground(ListInvitation... listInvitations) {
                DynamoDBInvitationDeclinedEvent event = new DynamoDBInvitationDeclinedEvent(false);
                try {
                    if (NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                        objectMapper.save(listInvitations[0]);
                        event.setSuccess(true);
                        event.setDeclinedInvitation(listInvitations[0]);
                    }else{
                        event.setException(new NoConnectionException(getString(R.string.dynamodb_no_internet_connection_msg)));
                        event.setMessage(getString(R.string.dynamodb_no_internet_connection_msg));
                    }
                }catch(AmazonServiceException ase){
                    event.setException(ase);
                    handleError(event, ase);
                }

                return event;
            }



            @Override
            protected void onPostExecute(DynamoDBInvitationDeclinedEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }

            private void handleError(DynamoDBInvitationDeclinedEvent event, AmazonServiceException ase) {
                switch(ase.getStatusCode()){
                    case DynamoDBErrorConstants.SERVICE_UNAVAILABLE:
                        // Could be temporary failure on AWS. Try again else throw failed event back to caller
                        if(!retryDone) {
                            new InvitationDecliner().execute(event.getDeclinedInvitation());
                            retryDone = true;
                        }else{
                            event.setSuccess(false);
                            event.setException(ase);
                            event.setMessage(getString(R.string.dynamodb_service_unavailable));
                            EventBus.getDefault().post(event);
                            retryDone = false;
                        }
                        break;
                }
            }
        }

        new InvitationDecliner().execute(singleInvitationToUpdate);

    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

/**
 * Responsible for creation of DynamoDBMapper objects.
 */
public class DynamoDBMapperCreator {


    public static DynamoDBMapper createMapper(AmazonDynamoDBClient client){
        return new DynamoDBMapper(client);
    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception;



public class NoConnectionException extends Exception {

    public NoConnectionException(final String msg){
        super(msg);
    }
}

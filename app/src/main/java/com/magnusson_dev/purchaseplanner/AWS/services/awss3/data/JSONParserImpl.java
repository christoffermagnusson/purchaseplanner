package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;



import android.util.Log;

import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.factories.AbstractListFactory;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.utils.DateFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class JSONParserImpl implements JSONParser{

    private static final String TAG = "JSONParserImpl";




    @Override
    public List<BaseList> parseTotalLists(String jsonStr){
        List<BaseList> parsedEntries = new ArrayList<>();
        try{
            JSONObject rootObject = new JSONObject(jsonStr);
            JSONArray jsonArray = rootObject.getJSONArray("lists");
            for(int i=0; i<jsonArray.length(); i++){
                JSONObject jsonObject = JSONHelper.cleanJSONElement(jsonArray.get(i).toString());
                String tag = jsonObject.getString("tag");
                String category = jsonObject.getString("category");
                String dateCreated = jsonObject.getString("dateCreated");
                String dateUpdated = jsonObject.getString("dateUpdated");
                List<ListItem> listOfItems = parseListItems(jsonObject);
                AbstractListFactory listFactory = FactoryMaker.getListFactory();

                BaseList list = listFactory.createList(tag,category);
                list.setFormatDateCreated(DateFormatter.parseToDate(dateCreated));
                list.setFormatDateUpdated(DateFormatter.parseToDate(dateUpdated));
                list.setListItems(listOfItems);
                parsedEntries.add(list);
                Log.d(TAG, "parseTotalLists: "+jsonObject.getString("tag"));
            }

        }catch(JSONException je){
            Log.e(TAG, "parseTotalLists: "+je.getMessage(), je);
        }
        return parsedEntries;
    }




    // UTILS
    private List<ListItem> parseListItems(JSONObject jsonObject) {
        List<ListItem> parsedListOfItems = new ArrayList<>();
        try{
            JSONArray jsonListOfItems = jsonObject.getJSONArray("listItems");
            for(int i=0; i<jsonListOfItems.length(); i++){
                String itemName = jsonListOfItems.getJSONObject(i).getString("name");
                boolean itemIsChecked = jsonListOfItems.getJSONObject(i).getBoolean("isChecked");
                ListItem item = new ListItem(itemName,itemIsChecked);
                parsedListOfItems.add(item);
            }

        }catch(JSONException je){
            Log.e(TAG, "parseListItems: "+je.getMessage(), je);
        }
        return parsedListOfItems;

    }






}

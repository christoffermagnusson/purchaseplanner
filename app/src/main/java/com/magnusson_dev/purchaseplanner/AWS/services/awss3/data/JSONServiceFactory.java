package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import com.magnusson_dev.purchaseplanner.ICallback;

public class JSONServiceFactory {


    public static JSONService getJSONServiceInstance(ICallback callback){
        return new JSONServiceImpl(callback);
    }

}

package com.magnusson_dev.purchaseplanner.AWS.services.cognito.exception;



public class UserSessionTokensEmptyException extends Exception {


    public UserSessionTokensEmptyException(final String msg){
        super(msg);
    }

}

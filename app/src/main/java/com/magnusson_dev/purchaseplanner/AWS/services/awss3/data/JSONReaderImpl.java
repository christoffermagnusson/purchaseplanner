package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.AWSConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.RESTService;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerImpl;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

class JSONReaderImpl implements JSONReader{
    private static final String TAG = "JSONReaderImpl";

    private File fileToManage;
    private final Context context;

    public static JSONLatestObject readObject;

    JSONReaderImpl (Context context){
        this.context=context;
    }



    @Override
    public void readJSON() throws IOException{
        Log.d(TAG, "readJSON: Context is = "+context);
        Log.d(TAG, "readJSON: Calling async readJSON");
        Intent intent = new Intent(context, RESTService.class);
        intent.putExtra(IntentConstants.ENDPOINT, String.format("https://s3.amazonaws.com/%s/%s",AWSConstants.BUCKET_NAME,AWSConstants.CURRENT_OBJECT));
        intent.putExtra(IntentConstants.KEY, AWSConstants.CURRENT_OBJECT);
        intent.putExtra(IntentConstants.INITIAL_DOWNLOAD, "NO");
        context.startService(intent);



    }



    // need to check everytime for reassignment of the downloaded object
    @Override
    public JSONLatestObject getLatestJSON() {
        if(readObject!=null) {
            Log.d(TAG, "getDownloadedJSON: Returning the downloaded object");
            Log.d(TAG, "getDownloadedJSON: Contents of downloaded object : "+readObject);
            return readObject;
        }else{
            JSONLatestObject locallyCachedObject = createCachedJSON();
            Log.d(TAG, "getDownloadedJSON: Returning locally cached object");
            Log.d(TAG, "getDownloadedJSON: Contents of locally cached object : "+locallyCachedObject);
            return locallyCachedObject;

        }
    }

    @Override
    public void setDownloadedJSON(String jsonStr){
        if(jsonStr!=null) {
            Log.d(TAG, "setDownloadedJSON: Setting new download as active object");
            readObject = new JSONLatestObject(JSONHelper.translateStringToJSON(jsonStr), JSONLatestObject.Type.DOWNLOADED);
            ListHandlerImpl storageHandler = new ListHandlerImpl(context);
            JSONParser parser = new JSONParserImpl();
            List<BaseList> downloadedLists = parser.parseTotalLists(readObject.getObject().toString());
            for (BaseList list : downloadedLists) {
                try {
                    storageHandler.addListToStorage(list);
                    Log.d(TAG, "setDownloadedJSON: Adding " + list + " to storage");
                } catch (StorageEditException see) {
                    see.printStackTrace();
                }
            }
        } else{
            Log.d(TAG, "setDownloadedJSON: No data in jsonStr.. defaulting back to previous data");
        }
    }

    @Override
    public void consumeDownloadedJSON() {
        Log.d(TAG, "consumeDownloadedJSON: Consuming the downloaded object");
        readObject = null;
    }

    private JSONLatestObject createCachedJSON(){

        return new JSONLatestObject(new JSONObject(), JSONLatestObject.Type.CACHED);

    }




    @Override
    public Context getContext() {
        return this.context;
    }

    private JSONObject convertStringToJson(String toConvert){
        try{
            Log.d(TAG, "convertStringToJson: Passed string = "+toConvert);
            JSONObject jsonToReturn = new JSONObject(toConvert);
            JSONArray jsonArray = jsonToReturn.getJSONArray("lists");
            for(int i=0; i<jsonArray.length(); i++){
                Log.d(TAG, "convertStringToJson: Element "+i+" in json = "+jsonArray.get(i));
            }
            return jsonToReturn;
        }catch(JSONException je){
            Log.d(TAG, "convertStringToJson: "+je.getMessage());
            je.printStackTrace();
        }
        return null;
    }




}

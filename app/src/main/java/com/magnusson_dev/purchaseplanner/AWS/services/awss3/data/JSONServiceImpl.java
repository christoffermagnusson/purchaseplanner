package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;

import android.content.Context;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.BaseList;

import java.io.IOException;
import java.util.List;


public class JSONServiceImpl implements JSONService {

    // TODO
    // Make not public when implementing web

    private Context context;
    private final ICallback callback;
    private static final String TAG = "JSONServiceImpl";


    public JSONServiceImpl(ICallback callback){
        this.callback= callback;
    }

    private JSONReader reader;
    private JSONWriter writer;
    private JSONParser parser = new JSONParserImpl();

    @Override
    public void setContext(Context newContext){
        Log.d(TAG, "setContext: Setting context "+newContext+" to JSONPackage");
        context=newContext;
        reader = new JSONReaderImpl(context);
        writer = new JSONWriterImpl(context);
    }




    private void checkContextReader() {
        Log.d(TAG, "checkContextReader: Current context of JSONPackage = "+context);
        if(reader==null || reader.getContext() == null){
            context = callback.onRefreshContext();
            Log.d(TAG, "checkContextReader: new JSONReader initiated with new context");
            reader = new JSONReaderImpl(context);
        }
    }

    private void checkContextWriter(){
        Log.d(TAG, "checkContextWriter: Current context of JSONPackage = "+context);
        if(writer==null || writer.getContext() == null){
            context = callback.onRefreshContext();
            Log.d(TAG, "checkContextWriter: new JSONWriter initiated with new content");
            writer = new JSONWriterImpl(context);
        }
    }


    @Override
    public void readJSON() {
        checkContextReader();
        try {
            reader.readJSON();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }

    }



    @Override
    public void setDownloadedJSON(String jsonStr) {
        checkContextReader();
        setContext(context);
        Log.d(TAG, "setDownloadedJSON: reader=="+reader);
        Log.d(TAG, "setDownloadedJSON: reader=="+reader+" , jsonStr=="+jsonStr);
        reader.setDownloadedJSON(jsonStr);
    }

    @Override
    public JSONLatestObject getLatestJSON() {
        checkContextReader();
        return reader.getLatestJSON();
    }

    @Override
    public void consumeDownloadedJSON() {
        checkContextReader();
        reader.consumeDownloadedJSON();
    }


    @Override
    public void writeList(BaseList listToWrite) {
        checkContextWriter();
        writer.writeList(listToWrite);
    }


    @Override
    public void editList(BaseList listToEdit) {
        checkContextWriter();
        Log.d(TAG, "editList: Current writer = "+writer);
        writer.editList(listToEdit);
    }

    @Override
    public void deleteList(BaseList listToDelete) {
        checkContextWriter();
        writer.deleteList(listToDelete);
    }

    @Override
    public List<BaseList> parseTotalLists(String jsonStr) {return parser.parseTotalLists(jsonStr);}


}

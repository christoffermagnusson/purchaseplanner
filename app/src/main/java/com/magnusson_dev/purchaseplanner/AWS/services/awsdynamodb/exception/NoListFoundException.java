package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception;



public class NoListFoundException extends Exception {

    public NoListFoundException(final String msg){
        super(msg);
    }

}

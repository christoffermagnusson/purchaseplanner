package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoCredentialsProviderCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.event.CognitoCredentialsProviderCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddUserEvent;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Service used to add new users to DynamoDB
 */
public class DynamoDBAddUserService extends Service {

    private AmazonDynamoDBClient dynamoDBClient;
    private DynamoDBMapper objectMapper;
    private CognitoCachingCredentialsProvider credentialsProvider;
    private DynamoDBUserModel userHolder;

    private static final String TAG = "DynamoDBAddUserService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Startup method for services. Checks if the service is
     * registered to EventBus's default channel.
     * Checks if the CredentialsProvider is existing. If not, initialize it, else
     * upload the user using the credentials.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        User userToUpload = CognitoSession.CURRENT_USER_INAPP;
        userHolder = new DynamoDBUserModel(userToUpload.getUsername(), userToUpload.getGivenName(), userToUpload.getEmail());
        if(CognitoSession.CREDENTIALS_PROVIDER==null){
            CognitoCredentialsProviderCreator.createCognitoCredentialsProvider(getApplicationContext(),CognitoSession.USER_SESSION);
        }else{
            uploadUser(userHolder);
        }
        return Service.START_NOT_STICKY;
    }



    @Override
    public boolean onUnbind(Intent intent) {
        EventBus.getDefault().unregister(this);
        return super.onUnbind(intent);
    }

    /**
     * Uploads a user to DynamoDB if not already existing.
     * @param user
     */
    private void uploadUser(final DynamoDBUserModel user) {
        class UserUploader extends AsyncTask<DynamoDBUserModel,Void,DynamoDBAddUserEvent>{

            @Override
            protected DynamoDBAddUserEvent doInBackground(DynamoDBUserModel... dynamoDBUserModels) {
                dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
                objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
                Log.d(TAG, "doInBackground: Saving user "+dynamoDBUserModels[0].getUsername()+ " to DynamoDB");
                DynamoDBUserModel validatingModel = objectMapper.load(DynamoDBUserModel.class, dynamoDBUserModels[0].getUsername());

                String existing = null;
                DynamoDBAddUserEvent event;
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    if (validatingModel == null) {
                        objectMapper.save(dynamoDBUserModels[0]);
                        existing = "Added new user to DynamoDB";
                    } else {
                        existing = "User already exists";
                    }
                    event = new DynamoDBAddUserEvent(true);
                    event.setMessage(existing);
                }else {
                    NoConnectionException exception = new NoConnectionException(getString(R.string.dynamodb_no_internet_connection_msg));
                    event = new DynamoDBAddUserEvent(false);
                    event.setException(exception);
                }
                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBAddUserEvent event) {
                Log.d(TAG, "onPostExecute: "+event);
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }
        }

        new UserUploader().execute(user);
    }


    /**
     * EventBus method for handling events. This class receives events on when a credentials
     * provider has been created. Whenever this event is recieved and is successful, the uploadUser
     * method in invoked.
     * @param event
     */
    @Subscribe
    public void onEvent(CognitoEvent event){
        if(event.getClass().equals(CognitoCredentialsProviderCreatedEvent.class)){
            CognitoCredentialsProviderCreatedEvent credentialsProviderCreatedEvent = (CognitoCredentialsProviderCreatedEvent) event;
            if(credentialsProviderCreatedEvent.isSuccess()){
                uploadUser(userHolder);
            }
        }
    }






}

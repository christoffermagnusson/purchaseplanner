package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.content.Context;

import com.amazonaws.regions.Regions;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.R;

/**
 * Constants used by Cognito
 */
public class CognitoConstants {

    public static ICallback callback;



    public static final String USER_POOL_ID = "cognito_user_pool_id";
    public static final String USER_POOL_ARN = "cognito_user_pool_arn";
    public static final String APP_CLIENT_ID = "app_client_id";
    public static final String APP_CLIENT_SECRET = "app_client_secret";
    public static final String IDENTITY_POOL_ID = "cognito_identity_pool_id";
    public static final Regions REGION = Regions.US_EAST_1;
    public static final String CREDENTIALS_VALIDATION = "credentials_validation";

    public static final String COGNITO_ID_TOKEN = "cognito_id_token";
    public static final String COGNITO_ACCESS_TOKEN = "cognito_access_token";
    public static final String COGNTIO_REFRESH_TOKEN = "cognito_refresh_token";


    public static String getResourceValue(final String reference){
        Context context = callback.onRefreshContext();
        String resourceValue = null;
        switch(reference){
            case USER_POOL_ID:
                resourceValue = context.getString(R.string.cognito_user_pool_id);
                break;
            case USER_POOL_ARN:
                resourceValue = context.getString(R.string.cognito_user_pool_arn);
                break;
            case APP_CLIENT_ID:
                resourceValue = context.getString(R.string.app_client_id);
                break;
            case APP_CLIENT_SECRET:
                resourceValue = context.getString(R.string.app_client_secret);
                break;
            case IDENTITY_POOL_ID:
                resourceValue = context.getString(R.string.cognito_identity_pool_id);
                break;
            case CREDENTIALS_VALIDATION:
                resourceValue = "cognito-idp."+REGION+".amazonaws.com/"+context.getString(R.string.cognito_user_pool_id);
                break;

        }
        return resourceValue;
    }
}

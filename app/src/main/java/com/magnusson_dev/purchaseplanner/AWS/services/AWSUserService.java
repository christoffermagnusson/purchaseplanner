package com.magnusson_dev.purchaseplanner.AWS.services;


import android.content.Context;

/**
 * Interface specifying which actions can be taken against an AWSUserService
 */
public interface AWSUserService {

    /**
     * Storing the current user in app. Done during confirmation of account
     * @param context
     */
    void storeUser(Context context);

    /**
     * Fetches the user specified by a username. Used as a search method.
     * @param username
     * @param context
     */
    void getUser(String username, Context context);

}

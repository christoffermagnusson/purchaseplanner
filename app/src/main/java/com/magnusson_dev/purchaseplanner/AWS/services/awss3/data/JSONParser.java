package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import com.magnusson_dev.purchaseplanner.model.BaseList;

import java.util.List;

interface JSONParser {

    List<BaseList> parseTotalLists(String jsonStr);



}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.magnusson_dev.purchaseplanner.ActivityType;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.utils.HTTPConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.utils.HTTPHelper;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONService;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONServiceFactory;
import com.magnusson_dev.purchaseplanner.event.GETRequestResponseEvent;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.greenrobot.eventbus.EventBus;


public class RESTService extends Service implements ICallback {

    private static final String TAG = "RESTService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String endpoint = intent.getStringExtra(IntentConstants.ENDPOINT);
        String key = intent.getStringExtra(IntentConstants.KEY);
        Log.d(TAG, "onStartCommand: Executing RESTService Async");
        GETRequest getRequest = new GETRequest();
        getRequest.execute(endpoint, key);
        return Service.START_NOT_STICKY;
    }




    class GETRequest extends AsyncTask<String,String,String>{


        @Override
        protected String doInBackground(String... url) {
            String endpoint = url[0];
            String objectKey = url[1];

            int index = 0;
            String response = HTTPHelper.GET(endpoint);
            Log.d(TAG, "doInBackground: Response = "+response);
                while (response.equalsIgnoreCase(HTTPConstants.ERROR_403)) {
                    Log.d(TAG, "doInBackground: Setting public read permission");
                    setPublicReadPermission(objectKey);
                    index++;
                    Log.d(TAG, "doInBackground: Attempt " + index + " to set public read permissions");
                    response = HTTPHelper.GET(endpoint);
                    setDownloadedJSON(response);
                }

                if(response.equalsIgnoreCase(HTTPConstants.NO_INTERNET)){
                    Log.d(TAG, "doInBackground: No internet connection available...");
                    return HTTPConstants.NO_INTERNET;
                }else{
                    setDownloadedJSON(response);
                    return HTTPConstants.CONNECTION_OK;
                }


        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d(TAG, "onProgressUpdate: "+values);
        }

        @Override
        protected void onPostExecute(String connectionStatus) {
            Log.d(TAG, "onPostExecute: RESULT OF GET = "+connectionStatus);
            if(connectionStatus.equalsIgnoreCase(HTTPConstants.CONNECTION_OK)) {
                EventBus.getDefault().post(new GETRequestResponseEvent(true));
            }else{
                EventBus.getDefault().post(new GETRequestResponseEvent(false));
            }
        }


    }

    private void setDownloadedJSON(String actualResponse) {
        JSONService jsonService = JSONServiceFactory.getJSONServiceInstance(this);
        jsonService.setDownloadedJSON(actualResponse);
    }

    private void setPublicReadPermission(String objectKey) {
        Log.d(TAG, "setPublicReadPermission: Setting public READ permission to public");
        AWSUtils.setContext(this);
        AWSUtils.getAmazonS3Client(AWSUtils.getCredentialsProvider()).setObjectAcl(
                AWSConstants.BUCKET_NAME,
                objectKey,
                CannedAccessControlList.PublicRead);
    }


    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {

    }

    @Override
    public Context onRefreshContext() {
        return this.getApplicationContext();
    }

}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.utils;


import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileCreator {

    private static final String TAG = "FileCreator";

    public static File getFileToUpload() {
        File toUpload = null;
        try {
            File root = new File(FileConstants.FILES_DIRECTORY, FileConstants.ROOT_DIR);
            FileWriter writer;
            if (!root.exists()) {
                Log.d(TAG, "getFileToUpload: Creating root directory");
                if (root.mkdirs()) {
                    Log.d(TAG, "getFileToUpload: Creating new fileToUpload");
                    toUpload = new File(root, FileConstants.FILE_TO_UPLOAD);
                    writer = new FileWriter(toUpload);
                    writer.flush();
                    writer.close();
                    if(toUpload.exists()) {
                        Log.d(TAG, "getFileToUpload: fileToUpload exists, returning it");
                        return toUpload;
                    }
                }
            } else {
                toUpload = new File(root, FileConstants.FILE_TO_UPLOAD);
                if (toUpload.exists()) {
                    Log.d(TAG, "getFileToUpload: Root directory and toUpload exists. Returning existing toUpload");
                    return toUpload;

                } else { // root exists, but file does not
                    Log.d(TAG, "getFileToUpload: Root directory exists but toUpload does not. Creating and returning new toUpload");
                    writer = new FileWriter(toUpload);
                    writer.flush();
                    writer.close();
                    if(toUpload.exists()) {
                        return toUpload;
                    }
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        Log.d(TAG, "getFileToUpload: For some reason fell outside of creation of file..");
        return toUpload;
    }


}

package com.magnusson_dev.purchaseplanner.AWS.services;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.ICallback;

/**
 * Creates the different AWSServices that are available to use.
 */
public class AWSServiceFactory {

    /**
     * Not in use.
     * @param context
     * @param callback
     * @return
     */
    public static AWSListService getAWSServiceS3(Context context, ICallback callback){
        return new AWSListServiceS3(context,callback);
    }

    public static AWSListService getAWSServiceDynamoDB(Context context, ICallback callback){
        return AWSListServiceDynamoDB.getInstance(context,callback);
    }

    public static AWSUserService getAWSUserService(){
        return AWSUserServiceDynamoDB.getInstance();
    }

    public static AWSInvitationService getAWSInvitationsService(Context context, ICallback callback){
        return AWSInvitationServiceDynamoDB.getInstance(context,callback);
    }
}

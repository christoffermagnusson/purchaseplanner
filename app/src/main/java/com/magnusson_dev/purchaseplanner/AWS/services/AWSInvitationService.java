package com.magnusson_dev.purchaseplanner.AWS.services;


import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.SharedListIdentifier;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import java.util.Set;

public interface AWSInvitationService {

    /**
     * Fetches all pending invitations from AWS storage. Pending
     * means that they have the status of not handled.
     * @return
     */
    void getPendingInvitations();

    /**
     * Uploads invitations. Typically done when a list has been created.
     * @param invitations
     */
    void sendInvitations(Set<ListInvitation> invitations);


    void acceptInvitation(ListInvitation acceptedInvitation);

    void declineInvitation(ListInvitation declinedInvitation);

    void uploadIdentifier(SharedListIdentifier identifier);



}

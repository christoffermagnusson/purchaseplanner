package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAccessToListRemovedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBRemovedListEvent;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.greenrobot.eventbus.EventBus;

/**
 * Service responsible for deleting access to lists
 * and the lists themselves if needed.
 */
public class DynamoDBDeleteListService extends Service {


    private DynamoDBMapper objectMapper;

    private static final String TAG = "DynamoDBDeleteListServi";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Service startup method. Checks whether the current user is
     * the last user currently having access to the list. If true, then
     * delete the whole list, else remove access to the list for the
     * current user.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
        BaseList listToDelete = fetchListFromStorage(intent.getStringExtra(IntentConstants.LIST_TAG));
        Log.d(TAG, "onStartCommand: listToDelete "+listToDelete);
        if(listToDelete!=null){
            removeAccess(listToDelete);
        }
        return Service.START_NOT_STICKY;
    }





    private void removeAccess(final BaseList listToDelete){
        // Remove list and shared identifier in order to removeAccess access to list.
        // Do check if last user, then removeAccess list entirely from DynamoDB
        class ListAccessRemover extends AsyncTask<BaseList,Void,BaseList>{

            @Override
            protected BaseList doInBackground(BaseList... baseLists) {
                SharedListIdentifier identifier = new SharedListIdentifier(CognitoSession.CURRENT_USER_INAPP.getUsername());
                identifier.setListId(baseLists[0].getId());
                Log.d(TAG, String.format("doInBackground: Deleting access for %s, with id %d",identifier.getUsername(),identifier.getListId()));
                objectMapper.delete(identifier);
                return listToDelete;
            }

            @Override
            protected void onPostExecute(BaseList listToDelete) {
                super.onPostExecute(listToDelete);
                DynamoDBAccessToListRemovedEvent event = new DynamoDBAccessToListRemovedEvent(true);
                event.setListToDelete(listToDelete);
                EventBus.getDefault().post(event);
            }
        }

        new ListAccessRemover().execute(listToDelete);
    }




    private BaseList fetchListFromStorage(String tag){
        ListHandler listStorage = ListHandlerFactory.getListHandler(getApplicationContext());
        BaseList toUpload = null;
        try{
            toUpload = listStorage.getListByTag(tag);
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        return toUpload;
    }
}

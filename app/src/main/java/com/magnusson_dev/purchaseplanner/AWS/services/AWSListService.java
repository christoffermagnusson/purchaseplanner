package com.magnusson_dev.purchaseplanner.AWS.services;


import com.magnusson_dev.purchaseplanner.model.BaseList;

/**
 * Interface representing the actions that can be performed against an AWSListService.
 */
public interface AWSListService {

    /**
     * Reads all lists that are related to the current user. The current user is fetched from
     * the CognitoSession class.
     */
    void readAllLists();

    /**
     * Reads the list specified in the parameter using chosen service.
     * Uses the Id attribute of the list.
     * @param list
     */
    void readListById(BaseList list);

    /**
     * Adds the list specified in the parameter using chosen service
     * @param list
     */
    void addList(BaseList list);

    /**
     * Edits the list specified in the parameter using chosen service
     * @param list
     */
    void editList(BaseList list);

    /**
     * Deletes the list specified in the parameter using chosen service
     * @param list
     */
    void deleteList(BaseList list);


}

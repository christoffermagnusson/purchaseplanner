package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws;


import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.DefaultSyncCallback;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.util.List;

class AWSUtils {

    private static final String TAG = "AWSUtils";

    private static Context context;
    private static CognitoCachingCredentialsProvider provider;


    public static void setContext(Context newContext){
        context=newContext;
    }


    static AmazonS3 getAmazonS3Client(CognitoCachingCredentialsProvider credentialsProvider){
        return new AmazonS3Client(credentialsProvider);

    }

    static CognitoCachingCredentialsProvider getCredentialsProvider(){
        // Initialize the Amazon Cognito credentials provider
        final CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                context,
                "us-east-1:b03e8f78-e1c0-4b86-a9f8-0dc2e4d6c453", // Identity pool ID // TODO make to @/strings
                Regions.US_EAST_1 // Region
        );

        Log.d(TAG, "onCreate: "+credentialsProvider);
        createSyncClient(credentialsProvider);
        provider = credentialsProvider;
        return provider;
    }

    private static void createSyncClient(CognitoCachingCredentialsProvider credentialsProvider){
        // Initialize the Cognito Sync client
        final CognitoSyncManager syncClient = new CognitoSyncManager(
                context,
                Regions.US_EAST_1, // Region
                credentialsProvider);

        Log.d(TAG, "onCreate: "+syncClient);
        // Create a record in a dataset and synchronize with the server
        Dataset dataset = syncClient.openOrCreateDataset("myDataset");
        dataset.put("myKey", "myValue");
        dataset.synchronize(new DefaultSyncCallback() {
            @Override
            public void onFailure(DataStorageException dse) {
                super.onFailure(dse);
                dse.printStackTrace();
                Log.d(TAG, "onFailure: FAILURE");
            }

            @Override
            public void onSuccess(Dataset dataset, List newRecords) {
                Log.d(TAG, "onSuccess: SUCCESSFUL");
                for(String s : dataset.getAll().values()){
                    Log.d(TAG, "onSuccess: Value : "+s);
                }
                //Your handler code here
            }
        });
    }

    static TransferUtility getTransferUtility(){
        return new TransferUtility(getAmazonS3Client(provider),context);
    }

}

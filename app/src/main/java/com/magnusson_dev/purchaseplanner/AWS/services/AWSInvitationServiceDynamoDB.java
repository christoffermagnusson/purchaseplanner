package com.magnusson_dev.purchaseplanner.AWS.services;


import android.content.Context;
import android.content.Intent;

import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBDeclineInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBPendingInvitationsService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBSendInvitationsService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBAcceptInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBUploadIdentifierService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.SharedListIdentifier;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.InvitationsHelper;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import java.util.Set;

public class AWSInvitationServiceDynamoDB implements AWSInvitationService {

    private Context context;
    private ICallback callback;

    private static AWSInvitationServiceDynamoDB instance;

    private AWSInvitationServiceDynamoDB(final Context context, final ICallback callback){
        this.context=context;
        this.callback=callback;
    }

    static AWSInvitationServiceDynamoDB getInstance(final Context context, final ICallback callback){
        instance = instance == null
                ? new AWSInvitationServiceDynamoDB(context,callback)
                : instance;

        return instance;
    }

    @Override
    public void getPendingInvitations() {
        checkContext();
        Intent intent = new Intent(context, DynamoDBPendingInvitationsService.class);
        context.startService(intent);
    }

    @Override
    public void sendInvitations(Set<ListInvitation> invitations) {
        checkContext();
        InvitationsHelper.invitationsToUpload = invitations;
        Intent intent = new Intent(context, DynamoDBSendInvitationsService.class);
        context.startService(intent);
    }

    @Override
    public void acceptInvitation(ListInvitation acceptedInvitation) {
        checkContext();
        InvitationsHelper.singleInvitationToUpdate = acceptedInvitation;
        Intent intent = new Intent(context, DynamoDBAcceptInvitationService.class);
        context.startService(intent);
    }

    @Override
    public void declineInvitation(ListInvitation declinedInvitation) {
        checkContext();
        InvitationsHelper.singleInvitationToUpdate = declinedInvitation;
        Intent intent = new Intent(context, DynamoDBDeclineInvitationService.class);
        context.startService(intent);
        // TODO continue working on declineService + events
    }

    @Override
    public void uploadIdentifier(SharedListIdentifier identifier) {
        checkContext();
        Intent intent = new Intent(context, DynamoDBUploadIdentifierService.class);
        intent.putExtra(IntentConstants.SHARED_LIST_ID, identifier.getListId());
        intent.putExtra(IntentConstants.SHARED_LIST_USERNAME, identifier.getUsername());
        context.startService(intent);
    }

    private void checkContext(){
        context = context == null
                ? callback.onRefreshContext()
                : context;
    }


}

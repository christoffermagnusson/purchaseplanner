package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.utils;


public class HTTPConstants {


    public static final String ERROR_403 = "ERROR_403";

    public static final String NO_INTERNET = "NO_INTERNET";

    public static final String CONNECTION_OK = "CONNECTION_OK";
}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.magnusson_dev.purchaseplanner.ActivityType;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONService;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.JSONServiceFactory;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import java.io.File;


public class UploadService extends Service implements ICallback {



    private ServiceBinder serviceBinder = new ServiceBinder();



    public class ServiceBinder extends Binder{
        public UploadService getService(){
            return UploadService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: Binding service");
        return serviceBinder;
    }



    private static final String TAG = "UploadService";
    TransferUtility transferUtility;
    AmazonS3 s3;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AWSUtils.setContext(this);
        s3 = AWSUtils.getAmazonS3Client(AWSUtils.getCredentialsProvider());
        Log.d(TAG, "onCreate: S3client = "+s3);
        transferUtility = AWSUtils.getTransferUtility();
        uploadToS3(intent.getStringExtra(IntentConstants.FILEPATH));
        return Service.START_NOT_STICKY;
    }


    public void uploadToS3(String file) {
        Log.d(TAG, "uploadToS3: Uploading file to S3 with filepath = "+file);
        final File fileToUpload = new File(file);
        if(fileToUpload.exists()){
            Log.d(TAG, "uploadToS3: File to upload exists : "+file);
        }
        final TransferObserver observer = transferUtility.upload(getString(R.string.bucket_name),fileToUpload.getName(),fileToUpload);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d(TAG, "onStateChanged: Upload State : "+state);
                if(state==TransferState.COMPLETED){
                    Log.d(TAG, "onStateChanged: Upload "+state);
                    //readLatestContent();
                }else if(state==TransferState.FAILED){
                    Toast uploadFailedToast = Toast.makeText(getApplicationContext(), getString(R.string.upload_failed), Toast.LENGTH_LONG);
                    uploadFailedToast.show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.d(TAG, "onProgressChanged: Uploaded "+bytesCurrent+" bytes of total "+bytesTotal+" bytes");
            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    private void readLatestContent() {
        JSONService jsonService = JSONServiceFactory.getJSONServiceInstance(this);
        jsonService.readJSON();
    }


    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {

    }

    @Override
    public Context onRefreshContext() {
        return this.getApplicationContext();
    }
}

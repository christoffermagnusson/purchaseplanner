package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.utils.DynamoDBErrorConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.InvitationsHelper;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationAcceptedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationSentEvent;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;

/**
 * Service used to accept a pending invitation in order to gain access
 * to a shared list
 */
public class DynamoDBAcceptInvitationService extends Service {

    private static final String TAG = "DynamoDBUpdateInvitatio";

    DynamoDBMapper objectMapper;

    private static boolean retryDone;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

        acceptInvitation(InvitationsHelper.singleInvitationToUpdate);

        return Service.START_NOT_STICKY;
    }

    /**
     * Saves an accepted invitation into DynamoDB and sends an
     * Event to be handled in InvitationActivity
     * @param singleInvitationToUpdate
     */
    private void acceptInvitation(ListInvitation singleInvitationToUpdate) {

        class InvitationUpdater extends AsyncTask<ListInvitation,Void,DynamoDBInvitationAcceptedEvent>{


            @Override
            protected DynamoDBInvitationAcceptedEvent doInBackground(ListInvitation... listInvitations) {

                DynamoDBInvitationAcceptedEvent event = new DynamoDBInvitationAcceptedEvent(false);
                try {
                    if (NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                        objectMapper.save(listInvitations[0]);
                        event.setSuccess(true);
                        event.setAcceptedInvitation(listInvitations[0]);
                    }else{
                        event.setMessage(getString(R.string.dynamodb_no_internet_connection_msg));
                        event.setException(new NoConnectionException(getString(R.string.dynamodb_no_internet_connection_msg)));
                    }
                }catch(AmazonServiceException ase){
                    event.setAcceptedInvitation(listInvitations[0]);
                    handleError(event, ase);
                }
                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBInvitationAcceptedEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }


            /**
             * Handles errors and does eventual retries or sends events back to the caller.
             * // TODO look into breaking this part out into an own component
             * // would possibly need attributes Event, Exception and AsyncTask
             * // to generalize the process. Some errors might not be generalizable though
             * @param event
             * @param ase
             */
            private void handleError(DynamoDBInvitationAcceptedEvent event, AmazonServiceException ase) {
                switch (ase.getStatusCode()){
                    case DynamoDBErrorConstants.SERVICE_UNAVAILABLE:
                        // Could be temporary failure on AWS. Try again else throw failed event back to caller
                        if(!retryDone) {
                            new InvitationUpdater().execute(event.getAcceptedInvitation());
                            retryDone = true;
                        }else{
                            event.setSuccess(false);
                            event.setException(ase);
                            event.setMessage(getString(R.string.dynamodb_service_unavailable));
                            EventBus.getDefault().post(event);
                            retryDone = false;
                        }
                        break;

                }

            }
        }

        new InvitationUpdater().execute(singleInvitationToUpdate);
    }


}

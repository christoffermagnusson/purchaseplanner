package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.BaseList;

import java.util.List;

public interface JSONService {


    void setContext(Context context);


    // Reader
    void readJSON();
    void setDownloadedJSON(String jsonStr);
    JSONLatestObject getLatestJSON();
    void consumeDownloadedJSON();

    // Writer
    void writeList(BaseList listToWrite);
    void editList(BaseList listToEdit);
    void deleteList(BaseList listToDelete);

    // Parser
    List<BaseList> parseTotalLists(String jsonStr);


}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.content.Context;

import java.io.IOException;

interface JSONReader {


    void readJSON() throws IOException;

    void setDownloadedJSON(String jsonStr);

    void consumeDownloadedJSON();

    JSONLatestObject getLatestJSON();

    Context getContext();
}

package com.magnusson_dev.purchaseplanner.AWS.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBDeleteListService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBReadAllListsService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBReadListByIdService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBWriteListService;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * An implementation of the AWSListService interface using the AWS Service DynamoDB
 */
public class AWSListServiceDynamoDB implements AWSListService {

    private Context context;
    private ICallback callback;

    private static final String TAG = "AWSListServiceDynamoDB";

    private static AWSListServiceDynamoDB instance = null;

    private AWSListServiceDynamoDB(final Context context, ICallback callback){
        this.context=context;
        this.callback=callback;
    }

    public static AWSListServiceDynamoDB getInstance(final Context context, ICallback callback){
        instance = instance == null
                ? new AWSListServiceDynamoDB(context,callback)
                : instance;

        return instance;
    }

    @Override
    public void readAllLists() {
        checkContext();
        Intent intent = new Intent(context, DynamoDBReadAllListsService.class);
        context.startService(intent);
    }

    @Override
    public void readListById(BaseList list) {
        checkContext();
        Intent intent = new Intent(context, DynamoDBReadListByIdService.class);
        intent.putExtra(IntentConstants.LIST_ID, list.getId());
        context.startService(intent);
    }

    @Override
    public void addList(BaseList list) {
        checkContext();
        Intent intent = new Intent(context, DynamoDBWriteListService.class);
        intent.putExtra(IntentConstants.LIST_TAG, list.getTag());
        context.startService(intent);
    }

    @Override
    public void editList(BaseList list) {
        checkContext();
        Intent intent = new Intent(context, DynamoDBWriteListService.class);
        intent.putExtra(IntentConstants.LIST_TAG, list.getTag());
        context.startService(intent);
    }

    @Override
    public void deleteList(BaseList list) {
        checkContext();
        User currentUser = CognitoSession.CURRENT_USER_INAPP;
        String currentUsername = currentUser.getUsername();
        User userToDelete = null;
        List<String> usernames = new ArrayList<>();
        list.getSharedUsers().forEach(user -> usernames.add(user.getUsername()));
        Set<String> locatorSet = new HashSet<>(usernames);
        userToDelete = locatorSet.contains(currentUsername) ? currentUser : null;
        if(userToDelete!=null){
            list.getSharedUsers().remove(userToDelete);
        }
        Intent intent = new Intent(context, DynamoDBDeleteListService.class);
        intent.putExtra(IntentConstants.LIST_TAG, list.getTag());
        context.startService(intent);

    }


    private void checkContext(){
        context = context == null
                ? callback.onRefreshContext()
                : context;
    }


}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;


/**
 * Model class only to represent the domain User object when
 * dealing with users in DynamoDB decoupled from the lists.
 *
 * (Needed since the original User objects are part of the BaseList objects)
 */
@DynamoDBTable(tableName = "Plnner_user")
public class DynamoDBUserModel {

    private String username;
    private String givenName;
    private String email;

    public DynamoDBUserModel(){}

    public DynamoDBUserModel(final String username, final String givenName, final String email){
        this.username=username;
        this.givenName=givenName;
        this.email=email;
    }

    @DynamoDBHashKey
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @DynamoDBAttribute
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @DynamoDBAttribute
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

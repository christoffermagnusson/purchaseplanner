package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.content.Context;

import com.magnusson_dev.purchaseplanner.model.BaseList;

interface JSONWriter {



    void writeList(BaseList listToWrite);

    void editList(BaseList listToEdit);

    void deleteList(BaseList listToDelete);

    Context getContext();

}

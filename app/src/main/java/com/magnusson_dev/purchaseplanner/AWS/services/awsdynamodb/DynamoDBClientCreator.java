package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;


import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

/**
 * Creator class for AmazonDynamoDBClient objects
 */
public class DynamoDBClientCreator {

    public static AmazonDynamoDBClient createDynamoDBClient(CognitoCachingCredentialsProvider credentialsProvider){
        return new AmazonDynamoDBClient(credentialsProvider);
    }
}

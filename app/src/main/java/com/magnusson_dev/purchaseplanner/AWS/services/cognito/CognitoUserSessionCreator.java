package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.tokens.CognitoAccessToken;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.tokens.CognitoIdToken;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.tokens.CognitoRefreshToken;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.exception.UserSessionTokensEmptyException;
import com.magnusson_dev.purchaseplanner.event.CognitoUserSessionCreatedEvent;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.utils.SharedPreferencesNullValueException;
import com.magnusson_dev.purchaseplanner.utils.SharedPreferencesWrapper;

import org.greenrobot.eventbus.EventBus;

/**
 * Class representing the creator of CognitoUserSessions.
 * This class initiates asyncronous requests to Cognito
 * with a users credentials. Posts an CognitoUserSessionCreatedEvent
 * upon completion, containing a CognitoUserSession if successful or an
 * Exception if failure
 */
public class CognitoUserSessionCreator {

    private static final String TAG = "CognitoUserSessionCreat";

    /**
     * Used when user details are found in app cache. In cases where
     * a user needs to be validated after the initial registration (each startup)
     * @param user
     * @return
     */
    public static AuthenticationHandler createCognitoUserSessionAsync(final CognitoUser user){
        AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
            @Override
            public void onSuccess(CognitoUserSession userSession) {
                Log.d(TAG, "onSuccess: Usersession successfully created: posting event");
                CognitoUserSessionCreatedEvent event = new CognitoUserSessionCreatedEvent(true);
                event.setUserSession(userSession);
                if(CognitoSession.USER_SESSION==null) {
                    CognitoSession.USER_SESSION = userSession;
                }
                EventBus.getDefault().post(event);

            }

            @Override
            public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String UserId) {
                AuthenticationDetails details = new AuthenticationDetails(user.getUserId(), null,null);
                Log.d(TAG, "getAuthenticationDetails: "+details.getUserId());
                authenticationContinuation.setAuthenticationDetails(details);
                authenticationContinuation.continueTask();
            }

            @Override
            public void getMFACode(MultiFactorAuthenticationContinuation continuation) {
                // No MFA functionality implemented in Plnner. Look for other solution?
            }

            @Override
            public void onFailure(Exception exception) {
                Log.d(TAG, "onFailure: Failed to create usersession");
                CognitoUserSessionCreatedEvent event = new CognitoUserSessionCreatedEvent(false);
                exception.printStackTrace();
                event.setMessage(String.format("Couldn't not create CognitoUserSession | \n Exception message %s",exception.getMessage()));
                event.setException(exception);
                EventBus.getDefault().post(event);
            }
        };
        return authenticationHandler;
    }

    /**
     * Used when doing the initial login into the app.
     * Validates the user against Cognito.
     * @param username
     * @param password
     * @return
     */
    public static AuthenticationHandler createLoginCognitoUserSessionAsync(final String username, final String password){
        AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
            @Override
            public void onSuccess(CognitoUserSession userSession) {
                Log.d(TAG, "onSuccess: Successfully logged in!");
                if(CognitoSession.USER_SESSION==null){
                    CognitoSession.USER_SESSION = userSession;
                }
                CognitoUserSessionCreatedEvent event = new CognitoUserSessionCreatedEvent(true);
                event.setUserSession(userSession);
                EventBus.getDefault().post(event);
            }

            @Override
            public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String UserId) {
                AuthenticationDetails details = new AuthenticationDetails(username, password, null);
                authenticationContinuation.setAuthenticationDetails(details);
                authenticationContinuation.continueTask();
            }

            @Override
            public void getMFACode(MultiFactorAuthenticationContinuation continuation) {

            }

            @Override
            public void onFailure(Exception exception) {
                Log.d(TAG, "onFailure: "+exception.getMessage());
                exception.printStackTrace();
                CognitoUserSessionCreatedEvent event = new CognitoUserSessionCreatedEvent(false);
                event.setMessage(exception.getMessage());
                event.setException(exception);
                EventBus.getDefault().post(event);
             }
        };
        return authenticationHandler;
    }


    public static void createUserSessionFromCache(Context context)throws UserSessionTokensEmptyException{
        CognitoUserSession userSessionFromCache = null;

        try {
            String idTokenStr = SharedPreferencesWrapper.getString(CognitoConstants.COGNITO_ID_TOKEN, context);
            String accessTokenStr = SharedPreferencesWrapper.getString(CognitoConstants.COGNITO_ACCESS_TOKEN, context);
            String refreshTokenStr = SharedPreferencesWrapper.getString(CognitoConstants.COGNTIO_REFRESH_TOKEN, context);

            CognitoIdToken idToken = new CognitoIdToken(idTokenStr);
            CognitoAccessToken accessToken = new CognitoAccessToken(accessTokenStr);
            CognitoRefreshToken refreshToken = new CognitoRefreshToken(refreshTokenStr);
            // TODO handle if refreshtoken has expired.. create new Exception and initiate new session with Cognito
            // clear this from CognitoSession.USER_SESSION

            userSessionFromCache = new CognitoUserSession(idToken,accessToken,refreshToken);
            CognitoSession.USER_SESSION = userSessionFromCache;
        }catch(SharedPreferencesNullValueException e){
            throw new UserSessionTokensEmptyException(context.getString(R.string.cognito_user_session_tokens_empty_exception_msg));
        }

    }
}

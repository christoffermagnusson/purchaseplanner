package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.magnusson_dev.purchaseplanner.event.CognitoCredentialsProviderCreatedEvent;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Initializes requests to create CognitoCredentialsProvider object. The credentialsprovider
 * is needed to get access to DynamoDB and other AWS services.
 */
public class CognitoCredentialsProviderCreator {

    private static final String TAG = "CognitoCredentialsProvi";

    /**
     * Creates a CognitoCachingCredentialsProvider. Sets the current sessions
     * credentialsprovider to the one created and sends a CognitoCredentialsProviderCreatedEvent
     * to subscribers to tell if it was successful or not
     * @param context
     * @param session
     */
    public static void createCognitoCredentialsProvider(final Context context, final CognitoUserSession session){
        class CreateCredentialsProvider extends AsyncTask<Void,Void,CognitoCachingCredentialsProvider>{

            @Override
            protected CognitoCachingCredentialsProvider doInBackground(Void... voids) {
                CognitoCachingCredentialsProvider provider = null;
                if(NetworkStateChecker.isNetworkingEnabled(context)) {
                    provider = new CognitoCachingCredentialsProvider(context,
                                CognitoConstants.getResourceValue(CognitoConstants.IDENTITY_POOL_ID),
                                CognitoConstants.REGION);
                    // If needed the access tokens are refreshed.
                    Map<String, String> logins = provider.getLogins();
                    if (logins == null) {
                        Log.d(TAG, "doInBackground: Refreshing tokens");
                        logins.put(CognitoConstants.getResourceValue(CognitoConstants.CREDENTIALS_VALIDATION),
                                session.getIdToken().getJWTToken());
                    }
                    provider.setLogins(logins);
                    provider.refresh();
                }
                return provider;
            }

            @Override
            protected void onPostExecute(CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider) {
                super.onPostExecute(cognitoCachingCredentialsProvider);
                // sets the credentialProvider for this session
                // TODO
                // Don't yet know what the possible errors could be here.. need to test this more
                // Here I assume that everything will go smooth. As said above, will need to read
                // up on the subject and do some testing
                CognitoCredentialsProviderCreatedEvent event = cognitoCachingCredentialsProvider!=null
                        ? new CognitoCredentialsProviderCreatedEvent(true)
                        : new CognitoCredentialsProviderCreatedEvent(false);
                event.setProvider(cognitoCachingCredentialsProvider);
                if(!event.isSuccess()){
                    event.setMessage("No internet");
                }
                EventBus.getDefault().post(event);
                CognitoSession.CREDENTIALS_PROVIDER = cognitoCachingCredentialsProvider;
            }
        }
        new CreateCredentialsProvider().execute();
    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.DynamoDBPendingInvitationsFinishedEvent;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class DynamoDBPendingInvitationsService extends Service {

    private DynamoDBMapper objectMapper;

    private static final String TAG = "DynamoDBPendingInvitati";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

        fetchPendingInvitations();

        return Service.START_NOT_STICKY;
    }



    private void fetchPendingInvitations() {

        class PendingInvitationsFetcher extends AsyncTask<Void,Void,DynamoDBPendingInvitationsFinishedEvent>{

            @Override
            protected DynamoDBPendingInvitationsFinishedEvent doInBackground(Void... voids) {
                ListInvitation queryObj = new ListInvitation.Builder()
                        .receiver(CognitoSession.CURRENT_COGNITO_USER.getUserId())
                        .build();

                DynamoDBQueryExpression<ListInvitation> queryExpression = new DynamoDBQueryExpression<ListInvitation>()
                        .withHashKeyValues(queryObj);

                Set<ListInvitation> pendingInvitations = new HashSet<>();
                DynamoDBPendingInvitationsFinishedEvent event = new DynamoDBPendingInvitationsFinishedEvent(false);
                try{
                   List<ListInvitation> temp = objectMapper.query(ListInvitation.class, queryExpression);
                   pendingInvitations.addAll(temp);
                   event.setPendingInvitations(pendingInvitations);
                   event.setSuccess(true);
                }catch(IllegalArgumentException iae){
                    iae.printStackTrace();
                }
                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBPendingInvitationsFinishedEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }
        }

        new PendingInvitationsFetcher().execute();
    }


}

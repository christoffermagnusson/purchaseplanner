package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

/**
 * Represents the current unique identifier on DynamoDB
 */
@DynamoDBTable(tableName = "NextID")
public class ListUUID {

    private String index;
    private int UUID;

    public ListUUID(){}

    @DynamoDBHashKey
    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @DynamoDBAttribute
    public int getUUID() {
        return UUID;
    }

    public void setUUID(int UUID) {
        this.UUID = UUID;
    }
}

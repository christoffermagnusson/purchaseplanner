package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.magnusson_dev.purchaseplanner.model.User;

/**
 * Represents the current app session with utility access to user session,
 * credentials provider and current user objects.
 */
public class CognitoSession {

    public static CognitoUserSession USER_SESSION;
    public static CognitoCachingCredentialsProvider CREDENTIALS_PROVIDER;

    public static User CURRENT_USER_INAPP;

    public static CognitoUser CURRENT_COGNITO_USER;

    public static boolean CURRENT_USER_UPLOADED;




}

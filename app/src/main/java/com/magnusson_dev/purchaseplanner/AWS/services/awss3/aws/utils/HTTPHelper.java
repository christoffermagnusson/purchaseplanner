package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.utils;


import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class HTTPHelper {

    private static final String TAG = "HTTPHelper";


    public static String GET(String endpoint) {
        int responseCode = 0;
        StringBuilder response = new StringBuilder();
        try{
            URL urlObj = new URL(endpoint);
            Log.d(TAG, "GET: Sending GET request to "+urlObj);
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User Agent", "Mozilla/5.0");
            connection.setRequestProperty("Host","s3.amazonaws.com");
            responseCode = connection.getResponseCode();

            Log.d(TAG, "GET: response code = "+responseCode);

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while((line = reader.readLine()) != null){
                response.append(line);
            }
            reader.close();
            Log.d(TAG, "GET: Result of GET = "+response.toString());
        }catch(MalformedURLException mue){
            mue.printStackTrace();
        }catch(FileNotFoundException fe){
            if(responseCode==403){
                return HTTPConstants.ERROR_403;
            }

        }catch(UnknownHostException uhe){
            uhe.printStackTrace();
            Log.d(TAG, "GET: Logging UnknownHostException");
            return HTTPConstants.NO_INTERNET;
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        Log.d(TAG, "GET: Returning null");
        return response.toString();
    }
}

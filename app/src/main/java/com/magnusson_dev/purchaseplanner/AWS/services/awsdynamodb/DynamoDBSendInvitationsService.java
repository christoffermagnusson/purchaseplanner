package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.InvitationsHelper;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationSentEvent;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class DynamoDBSendInvitationsService extends Service {

    AmazonDynamoDBClient dynamoDBClient;
    DynamoDBMapper objectMapper;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

        Set<ListInvitation> invitations = InvitationsHelper.invitationsToUpload;

        sendInvitations(invitations);

        return Service.START_NOT_STICKY;
    }

    @SuppressWarnings("unchecked")
    private void sendInvitations(Set<ListInvitation> invitations) {

        class InvitationsEmitter extends AsyncTask<Set<ListInvitation>,Void,DynamoDBInvitationSentEvent>{

            @Override
            protected DynamoDBInvitationSentEvent doInBackground(Set<ListInvitation>[] invitations) {
                DynamoDBInvitationSentEvent event = new DynamoDBInvitationSentEvent(false);
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    List<ListInvitation> upload = new ArrayList<>(invitations[0]); // casting set to ArrayList because DynamoDB demands it.
                    objectMapper.batchSave(upload);
                    event.setSuccess(true);
                }
                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBInvitationSentEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);

            }
        }

        new InvitationsEmitter().execute(invitations);

    }
}

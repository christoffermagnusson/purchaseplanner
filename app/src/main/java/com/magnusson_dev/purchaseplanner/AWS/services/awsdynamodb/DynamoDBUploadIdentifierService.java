package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.DynamoDBIdentifierUploadedEvent;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;


public class DynamoDBUploadIdentifierService extends Service {

    private DynamoDBMapper objectMapper;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

        String username = intent.getStringExtra(IntentConstants.SHARED_LIST_USERNAME);
        int id = intent.getIntExtra(IntentConstants.SHARED_LIST_ID, 0);

        SharedListIdentifier identifier = new SharedListIdentifier(username);
        identifier.setListId(id);

        upload(identifier);

        return Service.START_NOT_STICKY;
    }

    private void upload(SharedListIdentifier identifier) {
        class IdUploader extends AsyncTask<SharedListIdentifier,Void,DynamoDBIdentifierUploadedEvent>{

            @Override
            protected DynamoDBIdentifierUploadedEvent doInBackground(SharedListIdentifier... sharedListIdentifiers) {
                DynamoDBIdentifierUploadedEvent event = new DynamoDBIdentifierUploadedEvent(false);
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
                    objectMapper.save(sharedListIdentifiers[0]);
                    event.setSuccess(true);
                }

                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBIdentifierUploadedEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }
        }

        new IdUploader().execute(identifier);

    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.DynamoDBLoadUserFinishedEvent;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.greenrobot.eventbus.EventBus;

/**
 * Service for loading users from DynamoDB. These users
 * can then share lists between them.
 */
public class DynamoDBLoadUserService extends Service {

    private DynamoDBUserModel userModel;
    private DynamoDBMapper objectMapper;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        EventBus.getDefault().register(this);
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        EventBus.getDefault().unregister(this);
        return super.onUnbind(intent);
    }

    /**
     * Service startup method. Starts up the search
     * for a user from user input.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
        String username = intent.getStringExtra(IntentConstants.DYNAMO_USERNAME);
        if(username!=null) {
            loadUser(username);
        }
        return Service.START_NOT_STICKY;
    }

    /**
     * Tries to load a user from DynamoDB from user input.
     * If a user exists, the object is sent through with a
     * DynamoDBLoadUserFinishedEvent, else an error message
     * is sent with the event instead.
     * @param username
     */
    private void loadUser(final String username){
        class UserLoader extends AsyncTask<String, Void, DynamoDBUserModel>{

            @Override
            protected DynamoDBUserModel doInBackground(String... userDetails) {
                userModel = objectMapper.load(DynamoDBUserModel.class, userDetails[0]);
                return userModel;
            }

            @Override
            protected void onPostExecute(DynamoDBUserModel user) {
                String message = user != null
                        ? "Found "+username+" in database"
                        : "User "+username+" does not exist in the database";
                if(user!=null){
                    DynamoDBLoadUserFinishedEvent event = new DynamoDBLoadUserFinishedEvent(true);
                    event.setMessage(message);
                    User convertedUser = convert(user);
                    event.setUser(convertedUser);
                    EventBus.getDefault().post(event);
                }else{
                    DynamoDBLoadUserFinishedEvent event = new DynamoDBLoadUserFinishedEvent(false);
                    event.setMessage(message);
                    EventBus.getDefault().post(event);
                }
                super.onPostExecute(user);
            }

            /**
             * Convert the DynamoDBUserModel object to the domain
             * model object User
             * @param user
             * @return
             */
            private User convert(DynamoDBUserModel user) {
                String username = user.getUsername();
                String givenName = user.getGivenName();
                String email = user.getEmail();
                User convertedUser = FactoryMaker.getUserFactory().createUser(username);
                convertedUser.setGivenName(givenName);
                convertedUser.setEmail(email);

                return convertedUser;
            }
        }
        new UserLoader().execute(username);
    }
}

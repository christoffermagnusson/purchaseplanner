package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.util.Log;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.services.cognitoidentityprovider.model.AliasExistsException;
import com.amazonaws.services.cognitoidentityprovider.model.CodeMismatchException;
import com.magnusson_dev.purchaseplanner.event.CognitoAccountActivationEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Handles the activation process of a new account
 */
public class CognitoActivation {

    private static final String TAG = "CognitoActivation";

    /**
     * Activates the user account with the activation code parameter.
     * Depending on the outcome the CognitoAccountActivationEvent may be
     * successful or a failure.
     * @param user
     * @param activationCode
     */
    public static void activateAccount(final CognitoUser user, final String activationCode){
        class ActivationHandler implements GenericHandler{
            @Override
            public void onSuccess() {
                // Post complete event
                Log.d(TAG, "onSuccess: Successfully activated account");
                // Store user on DynamoDB
                CognitoAccountActivationEvent event = new CognitoAccountActivationEvent(true);
                EventBus.getDefault().post(event);
            }

            @Override
            public void onFailure(Exception exception) {
                Log.d(TAG, "onFailure: Failed to activate account");
                CognitoAccountActivationEvent event = new CognitoAccountActivationEvent(false);
                exception.printStackTrace();
                String errorMessage = generateErrorMessage(exception);
                event.setMessage(errorMessage);
                event.setErrorCause(exception);
                EventBus.getDefault().post(event);
            }

            /**
             * Generates an error message depending on the exception class
             * @param exception
             * @return
             */
            private String generateErrorMessage(Exception exception) {
                Object exClass = exception.getClass();
                String errorMessage = null;
                if(exClass.equals(AliasExistsException.class)){
                    errorMessage = "An account with the email already exist";
                }else if(exClass.equals(CodeMismatchException.class)){
                    errorMessage = "The code you provided is invalid. Please check your code again";
                }
                return errorMessage;
            }
        }

        user.confirmSignUpInBackground(activationCode, false, new ActivationHandler());
    }

}

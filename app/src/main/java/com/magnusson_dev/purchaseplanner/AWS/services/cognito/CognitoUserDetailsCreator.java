package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.util.Log;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;
import com.magnusson_dev.purchaseplanner.event.CognitoUserDetailsCreatedEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Initializes requests to Cognito to fetch a specific users details.
 */
public class CognitoUserDetailsCreator {

    private static final String TAG = "CognitoUserDetailsCreat";

    /**
     * Returns a details handler to be used in a request for user details.
     * The handler fires an event when it is determined if the user details
     * were fetched successfully or not.
     * @return
     */
    public static GetDetailsHandler createDetailsHandler(){

        return new GetDetailsHandler() {
            @Override
            public void onSuccess(CognitoUserDetails cognitoUserDetails) {
                Log.d(TAG, "onSuccess: CognitoUserDetails successfully fetched");
                CognitoUserDetailsCreatedEvent event = new CognitoUserDetailsCreatedEvent(true);
                event.setUserDetails(cognitoUserDetails);
                EventBus.getDefault().post(event);
            }

            @Override
            public void onFailure(Exception exception) {
                Log.d(TAG, "onFailure: CognitoUserDetails not fetched");
                CognitoUserDetailsCreatedEvent event = new CognitoUserDetailsCreatedEvent(false);
                event.setMessage(exception.getMessage());

            }
        };
    }
}

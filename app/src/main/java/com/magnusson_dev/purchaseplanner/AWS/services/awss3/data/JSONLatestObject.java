package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import org.json.JSONObject;

class JSONLatestObject {


    private final JSONObject object;
    private final Type type;

    JSONLatestObject(final JSONObject object, final Type type){
        this.object=object;
        this.type=type;

    }

    enum Type{
        DOWNLOADED,
        CACHED
    }

    JSONObject getObject() {
        return object;
    }

    public Type getType() {
        return type;
    }
}

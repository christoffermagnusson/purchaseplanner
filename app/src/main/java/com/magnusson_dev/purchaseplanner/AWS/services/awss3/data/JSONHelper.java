package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class JSONHelper {

    private static final String TAG = "JSONHelper";

    static String translateObjectToJSONString(Object obj){
        Gson gson = new Gson();
        Log.d(TAG, "translateObjectToJSONString: "+gson.toJson(obj));
        return new Gson().toJson(obj);
    }

    static JSONObject translateObjectToJSONObject(Object object){
        try{
            String translatedString = translateObjectToJSONString(object);
            JSONArray jsonArray = new JSONArray(translatedString);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("lists",jsonArray);
            return jsonObject;
        }catch(JSONException je){
            je.printStackTrace();
        }
        return null;
    }

    static JSONArray translateStringToJSONArray(String jsonStr){
        try{
            return  new JSONArray(jsonStr);
        }catch(JSONException je){
            je.printStackTrace();
        }
        return null;
    }

    static JSONObject putJSONArrayInRootObject(JSONArray jsonArray){
        try{
            JSONObject rootObj = new JSONObject();
            rootObj.put("lists",jsonArray);
            return rootObj;
        }catch(JSONException je){
            je.printStackTrace();
        }
        return  null;
    }

    static JSONObject translateStringToJSON(String toTranslate){
        try{
            return new JSONObject(toTranslate);
        }catch(JSONException je){
            je.printStackTrace();
        }
        return null;
    }

    static JSONObject cleanJSONElement(String toClean){
        JSONObject cleaned = null;
        try {
            cleaned = new JSONObject(toClean);
        }catch(JSONException je){
            je.printStackTrace();
        }
        return cleaned;
    }



    static JSONObject removeOldElementAndUpdateRootObject(JSONArray rootArray, String tag){
        List<String> holderList = new ArrayList<>();
        JSONArray updatedRootArray = new JSONArray();
        JSONObject updatedRootObj = new JSONObject();
        try{
            // Slide over JSONObject ro regular list
            for(int i=0; i<rootArray.length(); i++){
                String  element = rootArray.get(i).toString();
                Log.d(TAG, "removeOldElementAndUpdateRootObject: Contents of element = "+element);
                holderList.add(element);
            }
            // Check for occurences of the specified tag and remove
            for(int i=0; i< holderList.size(); i++){
                JSONObject elementToCheck = new JSONObject(holderList.get(i));
                if(elementToCheck.getString("tag").equalsIgnoreCase(tag)){
                    Log.d(TAG, "removeOldElementAndUpdateRootObject: Removing element "+i);
                    holderList.remove(i);
                }
            }
            // Put updated list back in updated JSONArray
            for(String element : holderList){
                JSONObject updatedElement = new JSONObject(element);
                updatedRootArray.put(updatedElement);
            }
            updatedRootObj.put("lists",updatedRootArray);

        }catch(JSONException je){
            Log.d(TAG, "removeOldElementAndUpdateRootObject: "+je.getMessage());
            je.printStackTrace();
        }
        return updatedRootObj;
    }


    static JSONArray getRootArray(JSONObject rootObj){
        JSONArray rootArray = new JSONArray();
        try{
            rootArray = rootObj.getJSONArray("lists");
        }catch(JSONException je){
            Log.d(TAG, "getRootArray: "+je.getMessage());
        }
        return rootArray;
    }

    static ArrayList<String> convertJsonElementsToStrings(JSONArray rootArray){
        ArrayList<String> convertedValues = new ArrayList<>();
        try {

            for (int i = 0; i < rootArray.length(); i++) {
                convertedValues.add(rootArray.get(i).toString());
            }
        }catch(JSONException je){
            Log.d(TAG, "convertJsonElementsToStrings: "+je.getMessage());
        }
        return convertedValues;
    }


}

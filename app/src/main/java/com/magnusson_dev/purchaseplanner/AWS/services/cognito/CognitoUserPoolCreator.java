package com.magnusson_dev.purchaseplanner.AWS.services.cognito;


import android.content.Context;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;

/**
 * Class responsible of creating CognitoUserPool objects.
 */
public class CognitoUserPoolCreator {

    private static CognitoUserPool userPool = null;

    /**
     * Creates a CognitoUserPool object using the parameterized Context object.
     * @param context
     * @return
     */
    public static CognitoUserPool createUserPool(Context context){
        if(userPool==null){
            ClientConfiguration config = new ClientConfiguration();
            userPool = new CognitoUserPool(context,
                                            CognitoConstants.getResourceValue(CognitoConstants.USER_POOL_ID),
                                            CognitoConstants.getResourceValue(CognitoConstants.APP_CLIENT_ID),
                                            CognitoConstants.getResourceValue(CognitoConstants.APP_CLIENT_SECRET),
                                            config);
        }
        return userPool;
    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws;


import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.utils.FileConstants;

public class AWSConstants {



    public static final String BUCKET_NAME = "s3-endpoint-test-plnner";

    public static final String CURRENT_OBJECT = FileConstants.FILE_TO_UPLOAD;


}

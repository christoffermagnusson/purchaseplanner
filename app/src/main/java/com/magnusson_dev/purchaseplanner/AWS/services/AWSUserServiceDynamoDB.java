package com.magnusson_dev.purchaseplanner.AWS.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBAddUserService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.DynamoDBLoadUserService;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

/**
 * An implementation of the AWSUserService interface using the AWS service DynamoDB.
 */
public class AWSUserServiceDynamoDB implements AWSUserService {
    private static final String TAG = "AWSUserServiceDynamoDB";

    private static AWSUserServiceDynamoDB instance = null;
    private AWSUserServiceDynamoDB(){}
    public static AWSUserServiceDynamoDB getInstance(){
        if(instance==null){
            instance = new AWSUserServiceDynamoDB();
        }
        return instance;
    }


    @Override
    public void storeUser(Context context) {
        Intent intent = new Intent(context, DynamoDBAddUserService.class);
        Log.d(TAG, "storeUser: Starting new DynamoDBAddUserService");
        context.startService(intent);
    }

    @Override
    public void getUser(String username, Context context) {
        Intent intent = new Intent(context, DynamoDBLoadUserService.class);
        intent.putExtra(IntentConstants.DYNAMO_USERNAME, username);
        Log.d(TAG, "getUser: Starting new DynamoDBLoadUserService");
        context.startService(intent);
    }
}

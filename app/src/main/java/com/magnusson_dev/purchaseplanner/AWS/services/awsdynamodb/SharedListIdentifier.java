package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

/**
 * Represents the relation between a list and the user.
 * Used as an access point to receive lists from DynamoDB
 */
@DynamoDBTable(tableName = "shared_list_key_Plnner")
public class SharedListIdentifier {

    private String username;
    private int listId;

    public SharedListIdentifier(){}

    public SharedListIdentifier(final String username){
        this.username=username;
    }

    @DynamoDBHashKey(attributeName = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @DynamoDBAttribute(attributeName = "listid")
    @DynamoDBRangeKey
    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }
}

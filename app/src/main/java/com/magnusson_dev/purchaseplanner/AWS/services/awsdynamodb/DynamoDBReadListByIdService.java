package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoListFoundException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoCredentialsProviderCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoCredentialsProviderCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBReadListByIdEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


/**
 * Service that fetches lists by id from DynamoDB
 */
public class DynamoDBReadListByIdService extends Service {

    private AmazonDynamoDBClient dynamoDBClient;
    private DynamoDBMapper objectMapper;
    private int idToRead;

    private static final String TAG = "DynamoDBReadListByIdSer";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        EventBus.getDefault().unregister(this);
        return super.onUnbind(intent);
    }

    /**
     * Service startup method. Executes the readList
     * method with the sent through list id.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }

        idToRead = intent.getIntExtra(IntentConstants.LIST_ID, 0);

        if(CognitoSession.CREDENTIALS_PROVIDER==null){
            Log.d(TAG, "onStartCommand: NULL");
            CognitoCredentialsProviderCreator.createCognitoCredentialsProvider(getApplicationContext(),CognitoSession.USER_SESSION);
        }else {
            dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
            objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
            readList(idToRead);
        }
        return Service.START_NOT_STICKY;
    }

    /**
     * Fetches a list specified by the parameter id
     * from DynamoDB.
     * @param id
     */
    private void readList(final Integer id){

        class ListByIdReader extends AsyncTask<Integer,Void,DynamoDBReadListByIdEvent>{

            @Override
            protected DynamoDBReadListByIdEvent doInBackground(Integer... id) {
                BaseList queryList = FactoryMaker.getListFactory().createQueryList(id[0]);
                BaseList fetchedList = objectMapper.load(queryList);
                DynamoDBReadListByIdEvent event = null;
                if(fetchedList==null){
                    event = new DynamoDBReadListByIdEvent(false);
                    NoListFoundException exception = new NoListFoundException(getString(R.string.dynamodb_no_list_found_msg));
                    event.setException(exception);
                }else if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
                    event = new DynamoDBReadListByIdEvent(false);
                    NoConnectionException exception = new NoConnectionException(getString(R.string.dynamodb_no_internet_connection_msg));
                    event.setException(exception);
                } else{
                    event = new DynamoDBReadListByIdEvent(true);
                    event.setBaseList(fetchedList);
                }
                return event;
            }

            @Override
            protected void onPostExecute(DynamoDBReadListByIdEvent event) {
                super.onPostExecute(event);
                EventBus.getDefault().post(event);
            }
        }

        new ListByIdReader().execute(id);
    }

    @Subscribe
    public void onEvent(AWSEvent event){
        if(event.getClass().equals(CognitoCredentialsProviderCreatedEvent.class)){
            CognitoCredentialsProviderCreatedEvent credentialsProviderCreatedEvent = (CognitoCredentialsProviderCreatedEvent) event;
            EventHandler.Handler successHandler = e -> {
                dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
                objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
                readList(idToRead);
            };
            EventHandler.Handler failureHandler = e -> {
                EventBus.getDefault().unregister(this);
                EventBus.getDefault().post(new CognitoCredentialsProviderCreatedEvent(false));
            };
            event.setHandler(event.isSuccess() ? successHandler : failureHandler);

        }

        if(event.getHandler()!=null) {
            EventHandler.handle(event);
        }
    }

}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoCredentialsProviderCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoCredentialsProviderCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBReadAllListsEvent;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that handles requests to read all lists
 * a specific user has access to.
 */
public class DynamoDBReadAllListsService extends Service {

    private CognitoUser currentUser;

    private AmazonDynamoDBClient dynamoDBClient;
    private DynamoDBMapper objectMapper;

    private static final String TAG = "DynamoDBReadAllListsSer";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Service startup method. Fetches the current user from app cache.
     * Initializes credentials provider if needed.
     * Start fetching identifiers of lists that is shared with
     * the user.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: Started readService...");
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        currentUser = CognitoSession.CURRENT_COGNITO_USER;
        // initial setup of the CredentialsProvider if it is not provided.
        if(CognitoSession.CREDENTIALS_PROVIDER==null){
            // Either fetch the credentialsprovider or startup process right away
            CognitoCredentialsProviderCreator.createCognitoCredentialsProvider(this, CognitoSession.USER_SESSION);
        } else{
            if(currentUser.getUserId()!=null) {
                fetchIdentifiers(currentUser.getUserId());
            }else{
                Log.d(TAG, "onStartCommand: Current CognitoUser in need of refresh.");
            }
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        EventBus.getDefault().unregister(this);
        return super.onUnbind(intent);
    }


    /**
     * Fetches all identifiers for lists the specific user is
     * related to.
     * @param username
     */
    private void fetchIdentifiers(final String username) {
        class FetchIdentifiersTask extends AsyncTask<String, Void, List<SharedListIdentifier>> {

            @Override
            protected List<SharedListIdentifier> doInBackground(String... userdetails) {
                dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
                objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);

                String username = userdetails[0];
                Log.d(TAG, "doInBackground: USERNAME : "+userdetails[0]);
                SharedListIdentifier queryObj = new SharedListIdentifier(username);
                DynamoDBQueryExpression<SharedListIdentifier> queryExpression = new DynamoDBQueryExpression<SharedListIdentifier>()
                        .withHashKeyValues(queryObj);
                List<SharedListIdentifier> identifiers = null;
                try {
                    identifiers = objectMapper.query(SharedListIdentifier.class, queryExpression);
                }catch(IllegalArgumentException e){
                    Log.d(TAG, "doInBackground: Argument : "+queryExpression);
                }
                return identifiers;
            }

            @Override
            protected void onPostExecute(List<SharedListIdentifier> identifiers) {
                if(identifiers!=null) {
                    Log.d(TAG, "onPostExecute: Successfully fetched identifiers");
                    fetchAllLists(identifiers);
                }else{
                    Log.d(TAG, "onPostExecute: No lists to fetch..");
                }
                super.onPostExecute(identifiers);
            }
        }
        new FetchIdentifiersTask().execute(username);
    }

    /**
     * With previous fetched identifiers, fetches those lists
     * related to the specific user.
     * When the lists has been fetched, a DynamoDBReadAllListsEvent
     * is sent with the lists attached.
     * @param identifiers
     */
    @SuppressWarnings("unchecked")
    private void fetchAllLists(List<SharedListIdentifier> identifiers) {
        class FetchAllListsTask extends AsyncTask<List<SharedListIdentifier>, Void, List<BaseList>> {

            @Override
            protected List<BaseList> doInBackground(List<SharedListIdentifier>[] lists) {
                List<SharedListIdentifier> identifiers = lists[0];
                List<BaseList> fetchedLists = new ArrayList<>();
                for (SharedListIdentifier identifier : identifiers) {
                    BaseList queryList = FactoryMaker.getListFactory().createQueryList(identifier.getListId());
                    BaseList fetchedList = objectMapper.load(queryList);
                    fetchedLists.add(fetchedList);
                }
                return fetchedLists;
            }

            @Override
            protected void onPostExecute(List<BaseList> baseLists) {
                DynamoDBReadAllListsEvent event = new DynamoDBReadAllListsEvent(true);
                event.setFetchedLists(baseLists);
                EventBus.getDefault().post(event);
                super.onPostExecute(baseLists);
            }
        }
        new FetchAllListsTask().execute(identifiers);
    }

    /**
     * EventBus method for handling events. This class receives events of the
     * type CognitoCredentialsProviderCreatedEvent
     * @param event
     */
    @Subscribe
    public void onEvent(AWSEvent event){
        if(event.getClass().equals(CognitoCredentialsProviderCreatedEvent.class)){
            CognitoCredentialsProviderCreatedEvent credentialsEvent = (CognitoCredentialsProviderCreatedEvent) event;
            if(credentialsEvent.isSuccess()){
                fetchIdentifiers(currentUser.getUserId());
            }
        }
    }
}

package com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserDetailsCreator;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserDetailsCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddListFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBNextIdGeneratedEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.storage.StorageFetchException;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service used when uploading and writing a list to
 * DynamoDB. Has methods for generating ID's for lists
 * and uploading the lists themselves.
 */
public class DynamoDBWriteListService extends Service {

    private DynamoDBMapper objectMapper;

    private BaseList toUpload;

    private static final String TAG = "DynamoDBWriteListServic";
    private static final int ID_NOT_SET = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    /**
     * Startup method for services.
     * This service starts by checking if it is registered to
     * EventBus's default channel.
     * Depending on if the list previously has been uploaded
     * the service operates differently. If the list never has been uploaded, a
     * unique ID has to generated for DynamoDB, else the list is simply uploaded.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        AmazonDynamoDBClient dynamoDBClient = DynamoDBClientCreator.createDynamoDBClient(CognitoSession.CREDENTIALS_PROVIDER);
        objectMapper = DynamoDBMapperCreator.createMapper(dynamoDBClient);
        String tag = intent.getStringExtra(IntentConstants.LIST_TAG);
        toUpload = fetchListFromStorage(tag);
        Log.d(TAG, "onStartCommand: Id of list to upload : "+toUpload.getId());
        if(toUpload.getId()==ID_NOT_SET) {
            Log.d(TAG, "onStartCommand: Id is not set. Generating new ID");
            generateId();
        }else{
            write(toUpload);
        }
        return Service.START_NOT_STICKY;
    }

    /**
     * Generates a new unique ID for the list to be uploaded.
     * Uses a separate DynamoDB table which only has 1 row.
     * This row holds the current id and therefore needs to be incremented
     * after fetched. A check is then done to see if the incremented ID
     * is unique.
     * Posts an DynamoDBNextIdGeneratedEvent upon completion to tell the
     * application it can proceed with execution.
     */
    private void generateId() {
        class IDGenerator extends AsyncTask<Void, Void, Integer>{

            @Override
            protected Integer doInBackground(Void... voids) {

                ListUUID idObj = objectMapper.load(ListUUID.class, "index");
                if(idObj==null){
                    Log.d(TAG, "doInBackground: Something went wrong when fetching the latest id");
                }

                int nextId = idObj.getUUID() + 1;
                while(objectMapper.load(ListUUID.class, "index").getUUID()!=idObj.getUUID()){
                    idObj = objectMapper.load(ListUUID.class, "index");
                    nextId = idObj.getUUID()+1;
                }

                idObj.setUUID(nextId);
                objectMapper.save(idObj);
                Log.d(TAG, "doInBackground: Saved new index at "+nextId);
                return nextId;
            }

            @Override
            protected void onPostExecute(Integer id) {
                if(id!=0) {
                    DynamoDBNextIdGeneratedEvent event = new DynamoDBNextIdGeneratedEvent(true);
                    event.setNextId(id);
                    EventBus.getDefault().post(event);
                }
                super.onPostExecute(id);

            }
        }

        new IDGenerator().execute();
    }

    /**
     * Uploads a list to DynamoDB. When completed, upload the
     * sharing information of the list to DynamoDB.
     * @param toUpload
     */
    private void write(final BaseList toUpload){
        class ListUploader extends AsyncTask<BaseList,Void,BaseList>{

            @Override
            protected BaseList doInBackground(BaseList... baseLists) {
                BaseList listToShare = baseLists[0];
                uploadIdentifierForCurrentUser(listToShare);
                try {
                    List<ListInvitation> invitations = new ArrayList<>();

                    if(!listToShare.isInvitesSent()) {
                        Log.d(TAG, "doInBackground: Sending invitations");
                        List<User> invitees = listToShare.getSharedUsers()
                                .stream()
                                .filter(u -> !u.getUsername().equals(CognitoSession.CURRENT_USER_INAPP.getUsername()))
                                .collect(Collectors.toList());

                        invitees.forEach(user -> {
                            ListInvitation invite = new ListInvitation.Builder().listName(listToShare.getTag())
                                    .id(listToShare.getId())
                                    .inviteHandled(false)
                                    .receiver(user.getUsername())
                                    .sender(CognitoSession.CURRENT_USER_INAPP.getUsername())
                                    .timeOfCreation(listToShare.getDateCreated())
                                    .build();
                            invitations.add(invite);
                        });
                        sendInvitations(invitations);
                    }else{
                        Log.d(TAG, "doInBackground: Invites already sent for this list");
                    }

                    listToShare.setInvitesSent(true);
                    objectMapper.save(listToShare);
                }catch(RuntimeException e){
                    e.printStackTrace();
                    Log.d(TAG, "doInBackground: "+e.getMessage());
                }
                return listToShare;
            }

            @Override
            protected void onPostExecute(BaseList listToShare) {
                super.onPostExecute(listToShare);
                DynamoDBAddListFinishedEvent event = new DynamoDBAddListFinishedEvent(true);
                Log.d(TAG, "onPostExecute: Invitations sent (before event) : "+listToShare.isInvitesSent());
                event.setAddedList(listToShare);
                EventBus.getDefault().post(event);
            }
        }

        new ListUploader().execute(toUpload);

    }

    private void uploadIdentifierForCurrentUser(BaseList listToShare) {

        class IdentifierUploader extends AsyncTask<BaseList,Void,Void>{

            @Override
            protected Void doInBackground(BaseList... baseLists) {
                try {
                    SharedListIdentifier identifier = new SharedListIdentifier(CognitoSession.CURRENT_USER_INAPP.getUsername());
                    identifier.setListId(baseLists[0].getId());
                    objectMapper.save(identifier);
                }catch(NullPointerException npe){
                    if(CognitoSession.CURRENT_USER_INAPP==null){
                        CognitoSession.CURRENT_COGNITO_USER.getDetailsInBackground(CognitoUserDetailsCreator.createDetailsHandler());
                    }
                }
                return null;
            }
        }

        new IdentifierUploader().execute(listToShare);

    }

    @SuppressWarnings("unchecked")
    private void sendInvitations(List<ListInvitation> invitations) {
        
        class InvitationEmitter extends AsyncTask<List<ListInvitation>, Void, Void>{

            @Override
            protected Void doInBackground(List<ListInvitation>[] invitations) {
                objectMapper.batchSave(invitations[0]);
                
                return null;
            }
            // TODO handle callback-
        }
        
        new InvitationEmitter().execute(invitations);
    }


    /**
     * Uploads information on who has access to the list uploaded.
     * @param identifiers
     */
    @SuppressWarnings("unchecked")
    private void shareList(final List<SharedListIdentifier> identifiers){

        class SharedListIdentifierUploader extends AsyncTask<List<SharedListIdentifier>,Void,Void>{

            @Override
            protected Void doInBackground(List<SharedListIdentifier>[] sharedListIdentifiers) {
                objectMapper.batchSave(sharedListIdentifiers[0]);
                return null;
            }


        }
        new SharedListIdentifierUploader().execute(identifiers);
    }


    /**
     * Utility method for fetching a list from local storage
     * @param tag
     * @return
     */
    private BaseList fetchListFromStorage(String tag){
        ListHandler listStorage = ListHandlerFactory.getListHandler(getApplicationContext());
        BaseList toUpload = null;
        try{
            toUpload = listStorage.getListByTag(tag);
        }catch(StorageFetchException sfe){
            sfe.printStackTrace();
        }
        return toUpload;
    }

    /**
     * Utility method for storing a list in local storage
     * @param toUpload
     */
    private void updateListInStorage(BaseList toUpload) {
        ListHandler listStorage = ListHandlerFactory.getListHandler(getApplicationContext());
        try {
            Log.d(TAG, "updateListInStorage: List id of list to be stored : "+toUpload.getId());
            listStorage.addListToStorage(toUpload);
        }catch(StorageEditException see){
            Log.d(TAG, "updateListInStorage: "+see.getMessage());
        }
    }

    /**
     * EventBus method for handling event. This class handles
     * DynamoDBNextIdGeneratedEvent which has information
     * on if a unique ID for the list to upload has been generated.
     * @param event
     */
    @Subscribe
    public void onEvent(AWSEvent event){
        EventHandler.Handler handler = defaultHandler -> Log.d(TAG, "onEvent: No handler attached");

        switch(event.getClass().getSimpleName()){
            case EventName.DYNAMODB_NEXT_ID_GENERATED_EVENT:
                DynamoDBNextIdGeneratedEvent nextIdGeneratedEvent = (DynamoDBNextIdGeneratedEvent) event;
                handler = getNextIdGeneratedEventHandler(nextIdGeneratedEvent);
                break;
            case EventName.COGNITO_USER_DETAILS_CREATED_EVENT:
                CognitoUserDetailsCreatedEvent detailsCreatedEvent = (CognitoUserDetailsCreatedEvent) event;
                handler = getDetailsCreatedEventHandler(detailsCreatedEvent);
        }

        event.setHandler(handler);
        EventHandler.handle(event);

    }

    private EventHandler.Handler getNextIdGeneratedEventHandler(DynamoDBNextIdGeneratedEvent event) {
        return event.isSuccess()
                ? handler -> {
                    toUpload.setId(event.getNextId());
                    updateListInStorage(toUpload);
                    write(toUpload);
                }
                : handler -> Log.d(TAG, "getNextIdGeneratedEventHandler: Couldn't generate a new ID");

    }

    private EventHandler.Handler getDetailsCreatedEventHandler(CognitoUserDetailsCreatedEvent event) {
        return event.isSuccess()
                ? handler -> {
                    Map<String,String> attributes = event.getUserDetails().getAttributes().getAttributes();
                    CognitoSession.CURRENT_USER_INAPP = new User.Builder().username(CognitoSession.CURRENT_COGNITO_USER.getUserId())
                            .givenName(attributes.get("given_name"))
                            .email("email")
                            .build();

                }
                : handler -> Log.d(TAG, "getDetailsCreatedEventHandler: Couldn't create DetailsHandler");

    }


}

package com.magnusson_dev.purchaseplanner.AWS.services.awss3.data;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.aws.UploadService;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.utils.FileCreator;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerImpl;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

class JSONWriterImpl implements JSONWriter{

    private static final String TAG = "JSONWriterImpl";

    private Context context;
    private ListHandlerImpl handler;

    JSONWriterImpl(Context context){
        this.context=context;
        handler = new ListHandlerImpl(context);
    }






    @Override
    public void writeList(BaseList listToWrite) {
        String jsonToWrite = buildUponJSON(listToWrite);


        if(jsonToWrite != null) {
            try {
                File fileToUpload = FileCreator.getFileToUpload();
                Log.d(TAG, "writeList: writing \n"+jsonToWrite+"\nto "+fileToUpload);
                FileWriter writer = new FileWriter(fileToUpload);
                writer.write(jsonToWrite);
                writer.flush();
                writer.close();


            } catch (IOException ioe) {
                Log.d(TAG, "writeList: " + ioe.getMessage());
            }
        }


        validateFileExistance("writeList");
        uploadToS3();
    }

    @Override
    public void editList(BaseList listToEdit){
        JSONObject rootObj = JSONHelper.translateObjectToJSONObject(handler.getTotalList());
        Log.d(TAG, "editList: ROOTOBJ : "+rootObj);
        JSONObject updatedRootObj = JSONHelper.removeOldElementAndUpdateRootObject(JSONHelper.getRootArray(rootObj),listToEdit.getTag());
        JSONArray rootArray = JSONHelper.getRootArray(updatedRootObj);
        String jsonToAdd = new Gson().toJson(listToEdit);
        try {
            rootArray.put(new JSONObject(jsonToAdd));
            updatedRootObj.put("lists",rootArray);
            writeUpdate(updatedRootObj.toString());
        }catch(JSONException je){
            Log.d(TAG, "editList: "+je.getMessage());
            je.printStackTrace();
        }

        validateFileExistance("editList");
    }


    @Override
    public void deleteList(BaseList listToDelete){
        JSONObject rootObj = JSONHelper.translateObjectToJSONObject(handler.getTotalList());
        Log.d(TAG, "deleteList: Current content of rootObj = "+rootObj);
        JSONObject updatedRootObj = JSONHelper.removeOldElementAndUpdateRootObject(JSONHelper.getRootArray(rootObj),listToDelete.getTag());
        Log.d(TAG, "deleteList: Content after deletion = "+updatedRootObj);
        writeUpdate(updatedRootObj.toString());
        validateFileExistance("deleteList");
    }

    @Override
    public Context getContext() {
        return this.context;
    }


    private void writeUpdate(String updatedRootObjToWrite) {
        try{
            File fileToUpload = FileCreator.getFileToUpload();
            Log.d(TAG, "writeUpdate: Checking for NULL : fileToUpload=="+fileToUpload);
            FileWriter writer = new FileWriter(fileToUpload);
            Log.d(TAG, "writeUpdate: Writing \n"+updatedRootObjToWrite+"\n to "+fileToUpload);
            writer.write(updatedRootObjToWrite);
            writer.flush();
            writer.close();

            validateFileExistance("writeUpdate");
        }catch(IOException ioe){
            Log.d(TAG, "writeUpdate: "+ioe.getMessage());
            ioe.printStackTrace();
        }
        uploadToS3();

    }


    private String buildInitialJSON(String newElement){
        JSONObject rootObject = new JSONObject();
        JSONArray rootArray = new JSONArray();
        JSONObject initialObject;
        try {
            initialObject = new JSONObject(newElement);
            rootArray.put(initialObject);
            rootObject.put("lists", rootArray);
            return rootObject.toString();
        }catch(JSONException je){
            Log.d(TAG, "buildInitialJSON: "+je.getMessage());
            je.printStackTrace();
        }
        return rootObject.toString();
    }


    private String buildUponJSON(BaseList listToWrite){
        String newElement = JSONHelper.translateObjectToJSONString(listToWrite);
        JSONObject rootObj = JSONHelper.translateObjectToJSONObject(handler.getTotalList());
        ArrayList<String> convertedValues =  JSONHelper.convertJsonElementsToStrings(JSONHelper.getRootArray(rootObj));
        convertedValues.add(newElement);
        Log.d(TAG, "buildUponJSON: Added new object = "+newElement);
        return fillUpdatedJSON(convertedValues);
    }


    private String fillUpdatedJSON(ArrayList<String> convertedValues){
        JSONObject updatedRootObj = new JSONObject();
        JSONArray updatedRootArray = new JSONArray();
        try {
            for (String s : convertedValues) {
                Log.d(TAG, "fillUpdatedJSON: Adding element to json = " + s);
                updatedRootArray.put(s);
            }
            updatedRootObj.put("lists", updatedRootArray);
        }catch(JSONException je){
            Log.d(TAG, "fillUpdatedJSON: "+je.getMessage());
        }
        return updatedRootObj.toString();
    }

    private void validateFileExistance(String tag) {
        File fileToUpload = FileCreator.getFileToUpload();
        if(fileToUpload.exists()){
            Log.d(TAG, tag+"validateFileExistance: "+fileToUpload+" exists");
        }else{
            Log.d(TAG, tag+"validateFileExistance: "+fileToUpload+" doesn't exist");
        }
    }


    /**
     * Method to upload lists to online storage
     */

    private void uploadToS3(){
        File fileToUpload = FileCreator.getFileToUpload();
        Intent intent = new Intent(context, UploadService.class);
        intent.putExtra(IntentConstants.ACTION, "UPLOAD");
        intent.putExtra(IntentConstants.FILEPATH, fileToUpload.getAbsolutePath());
        context.startService(intent);
    }
}

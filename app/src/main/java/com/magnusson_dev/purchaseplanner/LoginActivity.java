package com.magnusson_dev.purchaseplanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.services.cognitoidentityprovider.model.NotAuthorizedException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserSessionCreator;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserSessionCreatedEvent;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;
import com.magnusson_dev.purchaseplanner.utils.validators.Validator;
import com.magnusson_dev.purchaseplanner.utils.validators.ValidatorFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private TextView username_tv;
    private TextView password_tv;
    private ProgressBar loginProgress;
    private CoordinatorLayout snackbarCoordinatorLayout;
    private View mainView;
    private String username;

    private static String USERNAME = "USERNAME";
    private static String PASSWORD = "PASSWORD";

    private boolean inputEnabled = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        EventBus.getDefault().register(this);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().hide();
        }

        username_tv = (TextView) findViewById(R.id.loginUserName_tv);
        password_tv = (TextView) findViewById(R.id.loginPassword_tv);

        loginProgress = (ProgressBar) findViewById(R.id.loginLoginProgress);
        mainView = findViewById(R.id.loginMainView);
        snackbarCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.loginSnackbarCoord);
        NoInternetSnackbar.initialize(getApplicationContext(),snackbarCoordinatorLayout);
        if(!NetworkStateChecker.isNetworkingEnabled(getApplicationContext())){
            NoInternetSnackbar.show();
        }

        Button loginBtn = (Button) findViewById(R.id.loginActivityBtn);
        loginBtn.setOnClickListener( v -> {
                username = username_tv.getText().toString();
                String password = password_tv.getText().toString();
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    NoInternetSnackbar.dismiss();
                    if (validate(username, password)) {
                        // Login
                        AuthenticationHandler handler = CognitoUserSessionCreator.createLoginCognitoUserSessionAsync(username, password);
                        CognitoUserPool userPool = CognitoUserPoolCreator.createUserPool(getApplicationContext());
                        CognitoUser currentUser = userPool.getUser(username);
                        currentUser.getSessionInBackground(handler);
                        showProgressBar();
                    }
                }else{
                    NoInternetSnackbar.show();
                }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(USERNAME, username_tv.getText().toString());
        outState.putString(PASSWORD, password_tv.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        username_tv.setText(savedInstanceState.getString(USERNAME));
        password_tv.setText(savedInstanceState.getString(PASSWORD));
        
    }

    /**
     * Validates username and password with respective Validator interfaces
     * @param username
     * @param password
     * @return
     */
    private boolean validate(final String username, final String password){
        Validator usernameValidator = ValidatorFactory.createUsernameValidator(getApplicationContext());
        Validator passwordValidator = ValidatorFactory.createPasswordValidator(getApplicationContext());
        return usernameValidator.validate(username) && passwordValidator.validate(password);
    }


    /**
     * Disables touch events while logging in
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !inputEnabled || super.dispatchTouchEvent(ev);
    }

    private void showProgressBar() {
        loginProgress.setVisibility(View.VISIBLE);
        enableInput(false);
    }

    private void hideProgressBar(){
        loginProgress.setVisibility(View.INVISIBLE);
        enableInput(true);
    }

    private void enableInput(boolean enabled){
        inputEnabled = enabled;
        float alpha = inputEnabled ? 1.0f : 0.5f;
        mainView.setAlpha(alpha);
    }


    /**
     * EventBus method for handling incoming events. This activity subscribes on CognitoUserSessionCreatedEvents.
     * If a user session is created successfully means that the User was successfully signed in to the app.
     * @param event
     */
    @Subscribe
    public void onEvent(AWSEvent event){
        Object eventClass = event.getClass();
        if(eventClass.equals(CognitoUserSessionCreatedEvent.class)){
            hideProgressBar();
            CognitoUserSessionCreatedEvent userSessionCreatedEvent = (CognitoUserSessionCreatedEvent) event;
            if(userSessionCreatedEvent.isSuccess()){
                Toast successToast = Toast.makeText(getApplicationContext(),getString(R.string.login_successful_toast),Toast.LENGTH_SHORT);
                successToast.show();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }else{
                handleLoginFailure(userSessionCreatedEvent);
                userSessionCreatedEvent.getException().printStackTrace();
                Log.d(TAG, "onEvent: EXCEPTION CLASS : "+userSessionCreatedEvent.getException().getClass());
            }
        }
    }

    /**
     * Handles a failing login attempt. Checks if the user specified is confirmed
     * or if the user exists at all in the database
     * @param userSessionCreatedEvent
     */
    private void handleLoginFailure(CognitoUserSessionCreatedEvent userSessionCreatedEvent) {
        if(userSessionCreatedEvent.getException().getClass().equals(AmazonServiceException.class)){
            AmazonServiceException awsException = (AmazonServiceException) userSessionCreatedEvent.getException();
            String errorCode = awsException.getErrorCode();
            switch(errorCode){
                case "UserNotConfirmedException":
                    Toast userNotConfirmedToast = Toast.makeText(getApplicationContext(),
                            getString(R.string.login_user_not_confirmed_toast),
                            Toast.LENGTH_LONG);
                    userNotConfirmedToast.show();
                    Log.d(TAG, "handleLoginFailure: Starting new ConfirmRegistrationActivity");
                    Intent intent = new Intent(getApplicationContext(),ConfirmRegistrationActivity.class);
                    intent.putExtra(IntentConstants.USERNAME, username);
                    startActivity(intent);
                    break;
                case "UserNotFoundException":
                    Toast userNotFoundToast = Toast.makeText(getApplicationContext(),
                            getString(R.string.login_user_not_found_toast),
                            Toast.LENGTH_SHORT);
                    userNotFoundToast.show();
                    break;
            }
        }else if(userSessionCreatedEvent.getException().getClass().equals(NotAuthorizedException.class)){
            Toast incorrectCredentialsToast = Toast.makeText(getApplicationContext(),
                    getString(R.string.login_incorrect_credentials),
                    Toast.LENGTH_LONG);
            incorrectCredentialsToast.show();
        }

    }


}

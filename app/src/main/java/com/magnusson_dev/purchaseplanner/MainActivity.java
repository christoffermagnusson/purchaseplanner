package com.magnusson_dev.purchaseplanner;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSListService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSUserService;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.exception.NoConnectionException;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoConstants;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserDetailsCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserSessionCreator;
import com.magnusson_dev.purchaseplanner.adapters.OverviewRecyclerAdapter;
import com.magnusson_dev.purchaseplanner.adapters.OverviewRecyclerItemTouchHelperCallback;
import com.magnusson_dev.purchaseplanner.AWS.services.awss3.data.utils.FileConstants;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserDetailsCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserSessionCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAccessToListRemovedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBAddUserEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBPendingInvitationsFinishedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBReadAllListsEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBRemovedListEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.event.GETRequestResponseEvent;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;
import com.magnusson_dev.purchaseplanner.model.factories.FactoryMaker;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.storage.StorageEditException;
import com.magnusson_dev.purchaseplanner.utils.ColorUtils;
import com.magnusson_dev.purchaseplanner.utils.IntentConstants;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.NoInternetSnackbar;
import com.magnusson_dev.purchaseplanner.utils.ThemeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements ICallback,TouchHelperListener{

    private static final String TAG = "MainActivity";
    private RecyclerView.LayoutManager overviewLayoutManager;
    private RecyclerView overviewRecyclerView;
    private SwipeRefreshLayout overviewSwipeView;
    private OverviewRecyclerAdapter overviewAdapter;
    private View mainView;
    private CoordinatorLayout snackbarCoordLayout;
    private CoordinatorLayout invSnackbarCoordLayout;
    private ConstraintLayout noListsMsgContainer;

    private static MenuItem deleteInteractionItem;
    private boolean deleteInteractionIsPossible = false;
    private static final String DELETE_INTERACTION = "DeleteInteractionPossible";

    private static MenuItem refreshInteractionItem;

    private ProgressBar progressBar;
    private boolean inputEnabled;

    private ListHandler storageHandler;
    private AWSListService awsListService;
    private AWSInvitationService awsInvitationService;

    private CognitoUserPool userPool;
    private CognitoUser currentUser;

    private long startTime;
    private long endTime;

    private enum ButtonMode{
        STANDARD,
        FLOATING
    }

    ButtonMode currentMode;

    Button standardCreateNewListBtn;
    FloatingActionButton floatingCreateNewListBtn;


    /**
     * Sets up an initiates GUI and Cognito components if needed to be used in app
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.nanoTime();
        super.onCreate(savedInstanceState);
        super.setTheme(ThemeUtils.CURRENT_THEME);
        if(getWindow().getAttributes().windowAnimations!=R.style.fade_transition){
            Log.d(TAG, "onCreate: Correct transition not set. Setting it now.");
            overridePendingTransition(R.anim.fade_in_transition,R.anim.fade_out_transition);
        }
        setContentView(R.layout.activity_main_recycler_floating);
        standardCreateNewListBtn = (Button) findViewById(R.id.standardCreateNewListBtn);
        floatingCreateNewListBtn = (FloatingActionButton) findViewById(R.id.floatingCreateNewListBtn);
        currentMode = ButtonMode.FLOATING;
        setupCreateNewListView();
        Log.d(TAG, "onCreate: IN");
        overviewRecyclerView = (RecyclerView) findViewById(R.id.mainOverviewRecyclerView);
        overviewSwipeView = (SwipeRefreshLayout) findViewById(R.id.mainOverviewSwipeView);
        mainView = findViewById(R.id.mainActivityMainview);
        invSnackbarCoordLayout = (CoordinatorLayout) findViewById(R.id.mainActivityInvCoord);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        snackbarCoordLayout = (CoordinatorLayout) findViewById(R.id.snackbarCoordinator);
        NoInternetSnackbar.initialize(getApplicationContext(),snackbarCoordLayout);

        noListsMsgContainer = (ConstraintLayout) findViewById(R.id.mainActivity_msgHolder);
        noListsMsgContainer.setVisibility(View.GONE);




        // Register MainActivity to listen for events
        EventBus.getDefault().register(this);

        // Initiates storage and AWS components
        storageHandler = ListHandlerFactory.getListHandler(this);
        awsListService = AWSServiceFactory.getAWSServiceDynamoDB(getApplicationContext(),this);
        awsInvitationService = AWSServiceFactory.getAWSInvitationsService(getApplicationContext(), this);



        // Set the current root dir for future uploads
        FileConstants.FILES_DIRECTORY = getFilesDir();

        if(CognitoConstants.callback==null){
            CognitoConstants.callback = this;
        }

        if(CognitoSession.CURRENT_COGNITO_USER==null) {
            userPool = CognitoUserPoolCreator.createUserPool(this);
            currentUser = userPool.getCurrentUser();
            CognitoSession.CURRENT_COGNITO_USER = currentUser;
        }
        currentUser = CognitoSession.CURRENT_COGNITO_USER;
        Log.d(TAG, "onCreate: Current User : "+currentUser);

        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
            if (CognitoSession.USER_SESSION == null) {
                CognitoUserSessionCreator.createCognitoUserSessionAsync(currentUser);
                showProgressBar();
            }
            // Only check this is if the user is not already stored
            currentUser.getDetailsInBackground(CognitoUserDetailsCreator.createDetailsHandler());
        }else{
            NoInternetSnackbar.show();
            hideProgressBar();
        }


        Log.d(TAG, "onCreate: OUT");

    }


    /**
     * Check if there is internet, in that case fetch all the
     * latest changes from AWS, else initiate recyclerview
     * with cached lists from local storage
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: IN");
        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
            NoInternetSnackbar.dismiss();
            if(overviewAdapter!=null) {
                showProgressBar();
            }
            awsListService.readAllLists();
            // check for available downloadedInvitations
            if(CognitoSession.CREDENTIALS_PROVIDER!=null) {
                awsInvitationService.getPendingInvitations();
            }
        }else{
            refreshRecyclerView();
        }
        Log.d(TAG, "onStart: OUT");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: IN");
        Log.d(TAG, "onStop: OUT");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: IN");
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        Log.d(TAG, "onPause: OUT");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: IN");
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        Log.d(TAG, "onDestroy: OUT");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(DELETE_INTERACTION, deleteInteractionIsPossible);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState: IN");
        deleteInteractionIsPossible = savedInstanceState.getBoolean(DELETE_INTERACTION);
        Log.d(TAG, "onRestoreInstanceState: OUT");
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.overview_menu,menu);
        deleteInteractionItem = menu.getItem(0);
        refreshInteractionItem = menu.getItem(1);
        if(overviewAdapter!=null) {
            deleteInteractionItem.setChecked(deleteInteractionIsPossible);
            overviewAdapter.setInteractionDeletePossible(deleteInteractionIsPossible);
        }
        setColorOnIcon(deleteInteractionItem.getIcon());
        setColorOnIcon(refreshInteractionItem.getIcon());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.deleteMenuBtn:
                Log.d(TAG, "onOptionsItemSelected: Delete action selected");
                if(!deleteInteractionIsPossible){
                    overviewAdapter.setInteractionDeletePossible(true);
                    deleteInteractionIsPossible = true;
                    hideFloatingActionButton(true);
                }else{
                    overviewAdapter.setInteractionDeletePossible(false);
                    deleteInteractionIsPossible = false;
                    hideFloatingActionButton(false);
                }
                break;

            case R.id.refreshMenuBtn:
                Log.d(TAG, "onOptionsItemSelected: Refresh action selected");
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    startTime = System.nanoTime();
                    NoInternetSnackbar.dismiss();
                    awsListService.readAllLists();
                    awsInvitationService.getPendingInvitations();
                    showProgressBar();
                }else{
                    NoInternetSnackbar.show();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onSwiped(int position) {
        // Temp store item
        BaseList temp = overviewAdapter.getItem(position);
        // Temp remove item
        overviewAdapter.removeItem(position);
        awsListService.deleteList(temp);
        // Show snackbar enabling user to regret action taken
        Snackbar undoDeletionSnackbar = Snackbar.make(invSnackbarCoordLayout,
                getString(R.string.main_activity_undo_deletion_msg),
                Toast.LENGTH_SHORT);
        undoDeletionSnackbar.setAction(getString(R.string.main_activity_undo_deletion),
                onClick -> {
                    overviewAdapter.addItem(temp);
                    awsListService.addList(temp);
                    addToStorage(temp);
                });
        undoDeletionSnackbar.show();

    }


    /**
     * Disables touch events while refreshing from AWS
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !inputEnabled || super.dispatchTouchEvent(ev);
    }

    private void showProgressBar() {
        Log.d(TAG, "showProgressBar: SHOWING");
        progressBar.setVisibility(View.VISIBLE);
        hideFloatingActionButton(true);
        enableInput(false);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
        hideFloatingActionButton(false);
        enableInput(true);
    }

    private void enableInput(boolean enabled){
        inputEnabled = enabled;
        float alpha = inputEnabled ? 1.0f : 0.5f;
        if(overviewAdapter!=null) {
            overviewAdapter.setAlpha(alpha);
        }
    }

    private void hideFloatingActionButton(boolean isHidden) {
        if(currentMode==ButtonMode.FLOATING){
            if(isHidden){
                floatingCreateNewListBtn.hide();
            }else if(!deleteInteractionIsPossible){
                floatingCreateNewListBtn.show();
            }
        }
    }


    private void refreshRecyclerView(){
        List<BaseList> totalLists = storageHandler.getTotalList();
        int visibility = totalLists.size()==0 ? View.VISIBLE : View.GONE;
        noListsMsgContainer.setVisibility(visibility);
        setupRecyclerView(storageHandler.getTotalList());
    }

    /**
     * Setup for the activity's recyclerview. Redoes this procedure whenever a change event occurs
     * @param lists
     */
    private void setupRecyclerView(List<BaseList> lists) {
        Log.d(TAG, "setupRecyclerView: OverviewAdapter="+overviewAdapter);
            overviewAdapter = new OverviewRecyclerAdapter<>(MainActivity.this, R.layout.overview_card_item, lists);
            overviewAdapter.setCallback(this);
            overviewLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            overviewRecyclerView.setLayoutManager(overviewLayoutManager);
            overviewRecyclerView.setAdapter(overviewAdapter);
            overviewRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_SETTLING) {
                        hideFloatingActionButton(false);
                    } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        hideFloatingActionButton(true);
                    }

                    super.onScrollStateChanged(recyclerView, newState);
                }
            });
            overviewSwipeView.setOnRefreshListener(() -> {
                if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                    NoInternetSnackbar.dismiss();
                    awsListService.readAllLists();
                    awsInvitationService.getPendingInvitations();
                }else{
                    NoInternetSnackbar.show();
                }
            });
            OverviewRecyclerItemTouchHelperCallback recyclerCallback =
                    new OverviewRecyclerItemTouchHelperCallback(overviewAdapter,this);
            ItemTouchHelper recyclerHelper = new ItemTouchHelper(recyclerCallback);
            recyclerHelper.attachToRecyclerView(overviewRecyclerView);
        }


    /**
     * Sets the color on icons in the actionbar to reflect on the rest of the app
     * @param icon
     */
    private void setColorOnIcon(Drawable icon) {
        icon.mutate();
        icon.setColorFilter(getResources().getColor(R.color.cardview_light_background), PorterDuff.Mode.SRC_IN);
    }


    /**
     * Sets up listener for the add new list functionality. Depending on the chosen ButtonMode
     * displays different UI
     */
    private void setupCreateNewListView(){
        
        class CreateNewListListener implements View.OnClickListener{
            private ICallback callback;
            private void setCallback(ICallback callback){
                this.callback=callback;
            }
            
            @Override
            public void onClick(View view) {
                callback.onStartNewActivity(ActivityType.ADD_LIST_ACTIVITY, "ADD_LIST_ACTIVITY",null);
            }
        }

        if(currentMode==ButtonMode.STANDARD) {
            ImageView icon = (ImageView) findViewById(R.id.createNewListIcon);
            icon.setImageResource(R.drawable.ic_add_circle_outline_24dp);
            icon.setColorFilter(ContextCompat.getColor(this,R.color.shopping_color));
            standardCreateNewListBtn.setBackgroundColor(Color.TRANSPARENT);
            CreateNewListListener listener = new CreateNewListListener();
            listener.setCallback(this);
            standardCreateNewListBtn.setOnClickListener(listener);
        } else if(currentMode==ButtonMode.FLOATING){
            CreateNewListListener listener = new CreateNewListListener();
            listener.setCallback(this);
            floatingCreateNewListBtn.setOnClickListener(listener);
        }
    }


    /**
     * Activity switching method. Uses an enum called ActivityType to decide what
     * activity to start
     * @param activityType
     * @param intentMessage
     * @param category
     */
    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {
        Intent intent;
        switch(activityType){
            case DISPLAY_LIST_ACTIVITY:
                intent = new Intent(this, DisplayListActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                intent.putExtra(IntentConstants.INTENT_CATEGORY, category);
                Log.d(TAG, "onStartNewActivity: Starting new DisplayListActivity");
                startActivity(intent);
                break;
            case ADD_LIST_ACTIVITY:
                intent = new Intent(this, AddListActivity.class);
                intent.putExtra(IntentConstants.INTENT_MESSAGE, intentMessage);
                Log.d(TAG, "onStartNewActivity: Starting new AddListActivity");
                startActivity(intent);
                break;
            case REGISTER_ACTIVITY:
                intent = new Intent(this, RegistrationActivity.class);
                Log.d(TAG, "onStartNewActivity: Starting new RegistrationActivity");
                startActivity(intent);
                break;
            case INVITATIONS_ACTIVITY:
                intent = new Intent(this, InvitationsActivity.class);
                Log.d(TAG, "onStartNewActivity: Starting new InvitationsActivity");
                startActivity(intent);
                break;
        }
    }

    @Override
    public Context onRefreshContext() {
        return mainView.getContext();
    }


    /**
     * EventBus method for handling events. Takes a generic AWSEvent as an argument and "unpacks"
     * it to see what kind of event it is. Depending on the event, different actions should be taken
     * @param event
     */
    @Subscribe
    public void onEvent(AWSEvent event){
        EventHandler.Handler handler = defaultHandler -> Log.d(TAG, "onEvent: No eventHandler attached");

        switch(event.getClass().getSimpleName()){
            case EventName.GET_REQUEST_RESPONSE_EVENT:
                GETRequestResponseEvent requestResponseEvent = (GETRequestResponseEvent) event;
                handler = getGETRequestResponseEventHandler(requestResponseEvent);
                break;

            case EventName.DYNAMODB_READ_ALL_LISTS_EVENT:
                DynamoDBReadAllListsEvent readAllListsEvent = (DynamoDBReadAllListsEvent) event;
                handler = getReadAllListsEventHandler(readAllListsEvent);
                break;

            case EventName.COGNITO_USER_SESSION_CREATED_EVENT:
                CognitoUserSessionCreatedEvent userSessionCreatedEvent = (CognitoUserSessionCreatedEvent) event;
                handler = getUserSessionCreatedEventHandler(userSessionCreatedEvent);
                break;

            case EventName.COGNITO_USER_DETAILS_CREATED_EVENT:
                CognitoUserDetailsCreatedEvent detailsCreatedEvent = (CognitoUserDetailsCreatedEvent) event;
                handler = getUserDetailsCreatedEventHandler(detailsCreatedEvent);
                break;

            case EventName.DYNAMODB_ACCESS_TO_LIST_REMOVED_EVENT:
                DynamoDBAccessToListRemovedEvent accessToListRemovedEvent = (DynamoDBAccessToListRemovedEvent) event;
                handler = getAccessToListRemovedEventHandler(accessToListRemovedEvent);
                break;

            case EventName.DYNAMODB_REMOVED_LIST_EVENT:
                DynamoDBRemovedListEvent removedListEvent = (DynamoDBRemovedListEvent) event;
                handler = getRemovedListEventHandler(removedListEvent);
                break;

            case EventName.DYNAMODB_ADD_USER_EVENT:
                DynamoDBAddUserEvent addUserEvent = (DynamoDBAddUserEvent) event;
                handler = getAddUserEventHandler(addUserEvent);
                break;
                
            case EventName.DYNAMODB_PENDING_INVITATIONS_FINISHED_EVENT:
                DynamoDBPendingInvitationsFinishedEvent finishedEvent = (DynamoDBPendingInvitationsFinishedEvent) event;
                handler = getInvitationsFinishedEventHandler(finishedEvent);
        }

        event.setHandler(handler);

        if(event.getHandler()!=null) {
            EventHandler.handle(event);
        }
    }


    private EventHandler.Handler getGETRequestResponseEventHandler(GETRequestResponseEvent event){
        return event.isSuccess()
                ? handler -> {
                    refreshRecyclerView();
                    hideProgressBar();
                }
                : handler -> hideProgressBar();
    }

    private EventHandler.Handler getReadAllListsEventHandler(DynamoDBReadAllListsEvent event){
        endTime = System.nanoTime();
        Log.d(TAG, "getReadAllListsEventHandler: Time from start until fetched all lists = "+(float)(endTime-startTime)/1000000000 + " seconds");
        return event.isSuccess()
                ? handler -> {
                    event.getFetchedLists().forEach(this::addToStorage); // adding list to local storage
                    refreshRecyclerView();
                    hideProgressBar();
                    overviewSwipeView.setRefreshing(false);
                }
                : handler -> {
                    hideProgressBar();
                    overviewSwipeView.setRefreshing(false);
                };
        }


    private EventHandler.Handler getUserSessionCreatedEventHandler(CognitoUserSessionCreatedEvent event){
        return event.isSuccess()
                ? handler -> Log.d(TAG, "getUserSessionCreatedEventHandler: UserSession successfully created")
                : handler -> Log.d(TAG, String.format("getUserSessionCreatedEventHandler: UserSession not created successfully : %s"
                , event.getException().getMessage()));
    }

    private EventHandler.Handler getUserDetailsCreatedEventHandler(CognitoUserDetailsCreatedEvent event){
        return event.isSuccess()
                ? handler -> {
                    CognitoUserDetails details = event.getUserDetails();
                    Map<String,String> rawAttributes = details.getAttributes().getAttributes();
                    String username = currentUser.getUserId();
                    String givenName = rawAttributes.get("given_name");
                    String email = rawAttributes.get("email");

                    User.Builder builder = new User.Builder().username(username)
                            .givenName(givenName)
                            .email(email);
                    CognitoSession.CURRENT_USER_INAPP = FactoryMaker.getUserFactory().buildUser(builder);
                    // Store user on Dynamo if not stored previously in session
                    if(!CognitoSession.CURRENT_USER_UPLOADED) {
                        AWSUserService userService = AWSServiceFactory.getAWSUserService();
                        userService.storeUser(getApplicationContext());
                    }
                }
                : handler -> Log.d(TAG, "getUserDetailsCreatedEventHandler: User details not created");
    }

    private EventHandler.Handler getAccessToListRemovedEventHandler(DynamoDBAccessToListRemovedEvent event){
        return event.isSuccess()
                ? handler -> deleteListInStorage(event.getListToDelete())
                : handler -> Log.d(TAG, String.format("getAccessToListRemovedEventHandler: Access was not removed. : %s"
                , event.getMessage()));
    }

    private EventHandler.Handler getRemovedListEventHandler(DynamoDBRemovedListEvent event){
        return event.isSuccess()
                ? handler -> deleteListInStorage(event.getDeletedList())
                : handler -> Log.d(TAG, String.format("getRemovedListEventHandler: Failed to remove list from DynamoDB : %s"
                , event.getMessage()));
    }

    private EventHandler.Handler getAddUserEventHandler(DynamoDBAddUserEvent event){
        return event.isSuccess()
                ? handler -> CognitoSession.CURRENT_USER_UPLOADED = true
                : handler -> {
                    if(event.exception().getClass().equals(NoConnectionException.class)){
                        if(NetworkStateChecker.isNetworkingEnabled(getApplicationContext())) {
                            // Try storing the user once again, if connection was lost due to
                            // short outage
                            AWSUserService userService = AWSServiceFactory.getAWSUserService();
                            userService.storeUser(getApplicationContext());
                        } else {
                            // If still no connection, display constant message to user and try upload once again when connection
                            // has returned.
                            NoInternetSnackbar.show();
                        }
                    }
                };
    }

    private EventHandler.Handler getInvitationsFinishedEventHandler(DynamoDBPendingInvitationsFinishedEvent event){
        return event.isSuccess()
                ? handler -> {
                    // Put fetched pending downloadedInvitations in helper class for easy access
                    Set<ListInvitation> validInvitations = event.getPendingInvitations()
                            .stream()
                            .filter(inv -> !inv.isInviteHandled())
                            .collect(Collectors.toSet());
                    InvitationsHelper.downloadedInvitations = validInvitations;
                    final int nrInvitations = InvitationsHelper.downloadedInvitations.size();
                    if(nrInvitations > 0) {
                        @SuppressLint("DefaultLocale") String invitationsNrMsg = String.format("%d", nrInvitations);
                        // Show snackbar with option to view new downloadedInvitations
                        Snackbar pendingInvitationsSnackbar = Snackbar.make(invSnackbarCoordLayout,
                                getString(R.string.main_activity_new_pending_invitations, invitationsNrMsg)
                                , Snackbar.LENGTH_INDEFINITE);
                        pendingInvitationsSnackbar.show();
                        Log.d(TAG, "getInvitationsFinishedEventHandler: Showing the snackbar");
                        pendingInvitationsSnackbar.setAction(getString(R.string.main_activity_view_pending_invitations),
                                onClick -> {
                                    Log.d(TAG, "getInvitationsFinishedEventHandler: CLICKED");
                                    onStartNewActivity(ActivityType.INVITATIONS_ACTIVITY, null, null);
                                    pendingInvitationsSnackbar.dismiss();
                                });

                    }
                }
                : handler -> Log.d(TAG, "getInvitationsFinishedEventHandler: Failed to fetch downloadedInvitations");
    }

    /**
     * Utils method to store a list in storage
     * @param list
     */
    private void addToStorage(BaseList list){
        ListHandler storage = ListHandlerFactory.getListHandler(getApplicationContext());
        try {
            storage.addListToStorage(list);
        } catch (StorageEditException see) {
            Log.d(TAG, "addToStorage: "+see.getMessage());
        }
    }

    private void deleteListInStorage(BaseList list){
        try{
            storageHandler.deleteListInStorage(list);
        }catch(StorageEditException see){
            Log.d(TAG, "deleteListInStorage: "+see.getMessage());
        }
    }


}

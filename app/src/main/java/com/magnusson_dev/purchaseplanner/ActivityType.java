package com.magnusson_dev.purchaseplanner;

/**
 * Enum representing the different Activities available in the app
 */
public enum ActivityType {

    DISPLAY_LIST_ACTIVITY,
    ADD_LIST_ACTIVITY,
    MAIN_ACTIVITY,
    REGISTER_ACTIVITY,
    CONFIRM_REGISTRATION_ACTIVITY,
    SHARED_USER_LOOK_UP_ACTIVITY,
    SHARE_LIST_ACTIVITY,
    INVITATIONS_ACTIVITY


}

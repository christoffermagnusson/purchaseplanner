package com.magnusson_dev.purchaseplanner;

import android.content.Context;

public interface ICallback {

    /**
     * Activity switching method. Valid activities to switch from is decided
     * by the passed Enum activityType.
     * @param activityType
     * @param intentMessage
     * @param category
     */
    void onStartNewActivity(ActivityType activityType, String intentMessage, String category);

    /**
     * Method used by classes not having their own context and therefore eventually need to
     * refresh their need for a context.
     * @return
     */
    Context onRefreshContext();
}

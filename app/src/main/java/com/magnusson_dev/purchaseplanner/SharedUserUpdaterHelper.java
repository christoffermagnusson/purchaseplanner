package com.magnusson_dev.purchaseplanner;


import com.magnusson_dev.purchaseplanner.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Used for communication between SharedUserLookUpActivity and AddListActivity
 */
public class SharedUserUpdaterHelper {


    public static List<User> updatedUsers = new ArrayList<>();

    static String listName = "";
    static int categorySelection = 0;


    public static boolean contains(User user){
        boolean contains = false;
        if(updatedUsers!=null){
            contains = updatedUsers.contains(user);
        }

        return contains;
    }

    public static void remove(User user){
        if(updatedUsers!=null){
            updatedUsers.remove(user);
        }
    }

    public static void add(User user){
        if(updatedUsers!=null){
            updatedUsers.add(user);
        }
    }



}

package com.magnusson_dev.purchaseplanner.adapters;


import android.widget.ArrayAdapter;

import com.magnusson_dev.purchaseplanner.model.ListItem;

import java.util.List;

/**
 * Class that has a utility method for ArrayAdapters.
 */
class AdapterHelper {
    @SuppressWarnings({"rawtypes", "unchecked"})
    static void update(ArrayAdapter adapter, List<ListItem> listOfItems){
        adapter.clear();
        for(Object obj : listOfItems){
            adapter.add(obj);
        }
    }



}

package com.magnusson_dev.purchaseplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.model.ListCategory;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.StorageAction;
import com.magnusson_dev.purchaseplanner.utils.ColorUtils;
import com.magnusson_dev.purchaseplanner.utils.ThemeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Adapter responsible for managing views of
 * the RecyclerView in DisplayListActivity
 * @param <T>
 */
public class DisplayListItemRecyclerAdapter<T extends ListItem> extends
        RecyclerView.Adapter<DisplayListItemRecyclerAdapter.DisplayListViewHolder>{


    /**
     * Holds the data to be displayed and gets coupled
     * with corresponding view.
     */
    class DisplayListViewHolder extends RecyclerView.ViewHolder{
        final TextView listItemNameText;
        final CheckBox listItemCheckBox;
        final TextView listItemLastUpdatedByText;
        final ConstraintLayout foreground;
        final View mainView;
        final FrameLayout root;

        DisplayListViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            root = itemView.findViewById(R.id.displayListItemRoot);
            foreground = itemView.findViewById(R.id.displayListItemForeground);
            listItemNameText = itemView.findViewById(R.id.listItemText);
            listItemCheckBox = itemView.findViewById(R.id.editListItemCheckbox);
            listItemLastUpdatedByText = itemView.findViewById(R.id.displayListItemLastUpdated_tv);
        }

        ConstraintLayout getForeground(){
            return this.foreground;
        }

    }

    private final LayoutInflater inflater;
    private List<T> listOfItems;
    private final Context context;
    private ICallback callback;
    private final ListHandler handler;
    private StorageAction storageAction;
    private ListCategory category;


    private static final String TAG = "DisplayListItemRecycler";
    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

    private List<DisplayListViewHolder> listOfViewHolders = new ArrayList<>();


    public DisplayListItemRecyclerAdapter(Context context, @LayoutRes int resource, @NonNull List<T> listOfItems){
        int layoutResource = resource;
        this.inflater=LayoutInflater.from(context);
        this.context=context;
        this.handler = ListHandlerFactory.getListHandler(context);
        this.listOfItems=listOfItems;
    }

    /**
     * Callback is in this case DisplayListActivity, and this interface is
     * used for communication between the classes
     * @param callback
     */
    public void setCallback(ICallback callback){
        this.callback=callback;
    }

    /**
     * StorageAction is in this case DisplayListActivity. This interface is used
     * when an action is needed to be performed against the local storage
     * @param storageAction
     */
    public void setStorageAction(StorageAction storageAction){
        this.storageAction=storageAction;
    }


    private void addToListOfViewholders(DisplayListViewHolder viewHolder) {
        listOfViewHolders.add(viewHolder);
    }

    /**
     * Adds a single item into the adapter
     * @param item
     */
    @SuppressWarnings("unchecked")
    public void addItem(T item){
        listOfItems.add(item);
        storageAction.updateListToStorage((List<ListItem>) listOfItems);
        sortList();
    }

    /**
     * Adds a collection of items into the adapter
     * @param items
     */
    @SuppressWarnings("unchecked")
    public void addAllItems(List<T> items){
        listOfItems.clear();
        listOfItems.addAll(items);
        storageAction.updateListToStorage((List<ListItem>) listOfItems);
        sortList();
    }


    /**
     * Removes a single item from the adapter
     * @param position
     */
    @SuppressWarnings("unchecked")
    public void removeItem(int position){
        listOfItems.remove(position);
        storageAction.updateListToStorage((List<ListItem>) listOfItems);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listOfItems.size());
    }





    @Override
    public DisplayListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View displayListItem = inflater.inflate(R.layout.activity_display_list_item_reverse,
                                                parent,
                                                false);
        DisplayListViewHolder viewHolder = new DisplayListViewHolder(displayListItem);
        addToListOfViewholders(viewHolder);
        return viewHolder;
    }



    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(final DisplayListItemRecyclerAdapter.DisplayListViewHolder holder, int position) {
        final ListItem item = listOfItems.get(position);
        holder.foreground.setBackgroundColor(context.getColor(ThemeUtils.backgroundColor()));
        holder.listItemNameText.setText(item.getName(),TextView.BufferType.SPANNABLE);
        holder.listItemNameText.setTextColor(context.getColor(ColorUtils.secondaryTextMaterialDark()));
        holder.listItemNameText.setOnClickListener(view -> {
            AlertDialog.Builder renameDialog = new AlertDialog.Builder(context);
            renameDialog.setTitle("Change name of "+item.getName());
            final AutoCompleteTextView input = new AutoCompleteTextView(context);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            renameDialog.setView(input);

            renameDialog.setPositiveButton("OK", (dialogInterface, i) -> {
                 String changedName = input.getText().toString();
                 item.setName(changedName);
                 item.setLastupdatedBy(CognitoSession.CURRENT_USER_INAPP.getUsername());
                 holder.listItemNameText.setText(changedName);
                 holder.listItemLastUpdatedByText.setText(context.getString(R.string.display_list_lastupdated_by, "you"));
                 storageAction.updateListToStorage((List<ListItem>) listOfItems);
                 sortList();
            });

            renameDialog.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            renameDialog.show();
        });
        holder.listItemCheckBox.setChecked(item.isChecked());
        crossOutItem(item.isChecked(), holder);
        holder.listItemCheckBox.setOnClickListener(view -> {
            item.setChecked(holder.listItemCheckBox.isChecked());
            item.setLastupdatedBy(CognitoSession.CURRENT_USER_INAPP.getUsername());
            holder.listItemLastUpdatedByText.setText(context.getString(R.string.display_list_lastupdated_by, "you"));
            crossOutItem(holder.listItemCheckBox.isChecked(),holder);
            storageAction.updateListToStorage((List<ListItem>) listOfItems);
            sortList();

        });
        holder.listItemCheckBox.setChecked(item.isChecked());

        String lastupdatedByUser = "Last updated by ";
        if(CognitoSession.CURRENT_USER_INAPP!=null) {
            lastupdatedByUser = item.getLastupdatedBy().equals(CognitoSession.CURRENT_USER_INAPP.getUsername())
                    ? context.getString(R.string.display_list_lastupdated_by, "you")
                    : context.getString(R.string.display_list_lastupdated_by, item.getLastupdatedBy());
        }
        holder.listItemLastUpdatedByText.setText(lastupdatedByUser);
        holder.listItemLastUpdatedByText.setTextColor(ContextCompat.getColor(context,ThemeUtils.currentThemeMainColor(category)));

    }

    public void sortList() {
        Log.d(TAG, "sortList: Before : "+listOfItems);
        listOfItems = listOfItems.stream()
                .sorted((item1,item2) -> Boolean.compare(item1.isChecked(),item2.isChecked()))
                .collect(Collectors.toList());
        Log.d(TAG, "sortList: After : "+listOfItems);
        notifyItemRangeChanged(0, listOfItems.size());
    }

    public void setCurrentCategory(ListCategory category){
        this.category=category;
    }


    @Override
    public int getItemCount() {
        return listOfItems.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public T getItem(int position){
        return listOfItems.get(position);
    }

    public List<T> getListOfItems(){
        return listOfItems;
    }


    /**
     * Crosses out an item in the list. This happens when an
     * item is checked off.
     * @param checked
     * @param holder
     */
    private void crossOutItem(boolean checked, DisplayListViewHolder holder) {
        TextView nameText = holder.listItemNameText;
        Spannable spannable = (Spannable) nameText.getText();
        int lengthToSpan = nameText.getText().toString().length();

        if(checked){
            spannable.setSpan(STRIKE_THROUGH_SPAN,
                                0,
                                lengthToSpan,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            nameText.setTextColor(context.getColor(ColorUtils.textCrossedOut()));
            holder.foreground.setBackgroundColor(context.getColor(ColorUtils.itemCrossedOutBackground()));
        }else{
            spannable.setSpan(STRIKE_THROUGH_SPAN,
                                0,
                                0,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            nameText.setTextColor(context.getColor(ColorUtils.secondaryTextMaterialDark()));
            holder.foreground.setBackgroundColor(context.getColor(ColorUtils.mainBackgroundDark()));
        }
    }


    public void setAlpha(float alpha){
        listOfViewHolders.forEach(holder -> holder.mainView.setAlpha(alpha));
    }



}

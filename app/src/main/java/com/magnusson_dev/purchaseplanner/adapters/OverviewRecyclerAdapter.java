package com.magnusson_dev.purchaseplanner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.ActivityType;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandler;
import com.magnusson_dev.purchaseplanner.storage.liststorage.ListHandlerFactory;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.ListCategory;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.utils.NetworkStateChecker;
import com.magnusson_dev.purchaseplanner.utils.ThemeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * RecyclerAdapter for MainActivity.
 * @param <T>
 */
public class OverviewRecyclerAdapter<T extends BaseList> extends RecyclerView.Adapter<OverviewRecyclerAdapter.OverviewViewHolder> {


    private boolean deleteInteractionIsPossible;
    private static int MAX_PREVIEW_ITEM_COUNT = 3;


    /**
     * Representing the data shown in each view in
     * the RecyclerView
     */
    class OverviewViewHolder extends RecyclerView.ViewHolder{
         final TextView overviewItemText;
         final TextView createdDateText;
         final TextView item_1;
         final TextView item_2;
         final TextView item_3;
         final ImageButton deleteBtn;
         final View mainView;
         final CardView cardView;
         final ProgressBar progress;
         final TextView progress_tv;

         TextView [] displayedItems = new TextView[3];
         OverviewViewHolder(View v) {
             super(v);
             mainView = v;
             cardView = v.findViewById(R.id.overview_cardview);
             overviewItemText = v.findViewById(R.id.overviewItemTextTitle);
             createdDateText = v.findViewById(R.id.createdDateText);
             item_1 = v.findViewById(R.id.overviewItemText_1);
             item_2 = v.findViewById(R.id.overviewItemText_2);
             item_3 = v.findViewById(R.id.overviewItemText_3);
             displayedItems[0]=item_1;
             displayedItems[1]=item_2;
             displayedItems[2]=item_3;
             deleteBtn = v.findViewById(R.id.deleteBtn);
             progress = v.findViewById(R.id.overviewListProgressBar);
             progress_tv = v.findViewById(R.id.overviewListProgressIndicator_tv);
         }

    }

    private final int layoutResource;
    private final LayoutInflater inflater;
    private List<T> listOfItems;
    private final Context context;
    private ICallback callback;
    private ListHandler handler;

    private static final String TAG = "OverviewRecyclerAdapter";
    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

    private List<OverviewViewHolder> listOfViewholders = new ArrayList<>();



    public OverviewRecyclerAdapter(Context context, @LayoutRes int resource, @NonNull List<T> listOfItems){
        this.layoutResource=resource;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.handler = ListHandlerFactory.getListHandler(context);
        this.listOfItems=listOfItems;
    }

    public void setCallback(ICallback callback){
        this.callback=callback;
    }




    @Override
    public OverviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View overviewItem = inflater.inflate(R.layout.overview_card_item,parent,false);
        OverviewViewHolder overviewViewHolder = new OverviewViewHolder(overviewItem);
        addToListOfViewholders(overviewViewHolder);
        return overviewViewHolder;
    }


    /**
     * Binds the data from a viewholder to a view.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(OverviewRecyclerAdapter.OverviewViewHolder holder, int position) {
        final BaseList list = listOfItems.get(position);
        holder.overviewItemText.setText(list.getTag());
        holder.createdDateText.setText(list.getDateCreated());
        setDeleteIconsVisible(holder.deleteBtn);

        for(int i=0; i<MAX_PREVIEW_ITEM_COUNT; i++) {
            setupPreviewItems(list, holder.displayedItems[i], i);
        }
        holder.mainView.setOnClickListener(view -> callback.onStartNewActivity(ActivityType.DISPLAY_LIST_ACTIVITY
                , list.getTag()
                , list.getCategoryType().toString()));

        setupListProgress(list, holder.progress, holder.progress_tv);

        setCardBackgroundColor(list.getCategoryType(),holder.cardView);

        holder.deleteBtn.setOnClickListener(view -> {
            if(NetworkStateChecker.isNetworkingEnabled(callback.onRefreshContext())) {
                AWSServiceFactory.getAWSServiceDynamoDB(callback.onRefreshContext(), callback).deleteList(list);
                listOfItems.remove(list);
            }else{
                    // If no internet tell the user that the list will only be removed from local storage
                Toast onlyLocalDeletionToast = Toast.makeText(callback.onRefreshContext(),
                         callback.onRefreshContext().getString(R.string.main_activity_local_changes_only),
                        Toast.LENGTH_LONG);
                onlyLocalDeletionToast.show();

            }
            notifyDataSetChanged();
        });


    }

    private void setupListProgress(BaseList list, ProgressBar progress, TextView progress_tv) {
        Predicate<ListItem> checkedItems = ListItem::isChecked;
        int max = list.getListItems().size();
        int nrChecked = list.getListItems()
                            .stream()
                            .filter(checkedItems)
                            .collect(Collectors.toList())
                            .size();

        progress.getProgressDrawable().setColorFilter(ContextCompat.getColor(context, R.color.progressBarColor), PorterDuff.Mode.MULTIPLY);
        progress.setMax(max);
        progress.setProgress(nrChecked);
        String progressVal = String.format(Locale.UK,"%d / %d",nrChecked,max);
        progress_tv.setText(progressVal);

    }

    private void setDeleteIconsVisible(ImageButton deleteBtn) {
        if(deleteInteractionIsPossible) {
            deleteBtn.setEnabled(true);
            deleteBtn.setVisibility(View.VISIBLE);
        }else{
            deleteBtn.setEnabled(false);
            deleteBtn.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Sets colors depending on which theme the current list has.
     * @param category
     * @param mainView
     */
    private void setCardBackgroundColor(ListCategory category, View mainView){
        switch(category){
            case SHOPPING:
                mainView.setBackgroundColor(ContextCompat.getColor(context, ThemeUtils.shoppingColor()));
                break;
            case TODO:
                mainView.setBackgroundColor(ContextCompat.getColor(context,ThemeUtils.todoColor()));
                break;
            case WISHLIST:
                mainView.setBackgroundColor(ContextCompat.getColor(context,ThemeUtils.wishlistColor()));

        }
    }

    /**
     * Sets the preview items shown in the views.
     * @param list
     * @param item
     * @param position
     */
    private void setupPreviewItems(BaseList list, TextView item, int position) {
        try{
            ListItem listItemObj = list.getListItems().get(position);
            String itemText = listItemObj.toString();
            item.setText(itemText, TextView.BufferType.SPANNABLE);
            crossOutItem(listItemObj.isChecked(),item);
        }catch(IndexOutOfBoundsException e){
            // if no items are in the list then set initial values of display to ""
            item.setText("");
        }

    }

    /**
     * Modifies the preview items and crosses out text if needed.
     * @param checked
     * @param listItemTextView
     */
    private void crossOutItem(boolean checked, TextView listItemTextView) {
        Spannable spannable = (Spannable) listItemTextView.getText();
        int lengthToSpan = listItemTextView.getText().toString().length();

        if(checked){
            spannable.setSpan(STRIKE_THROUGH_SPAN, 0, lengthToSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            listItemTextView.setTextColor(ContextCompat.getColor(context,R.color.item_crossed_out_color));
        }else{
            spannable.setSpan(STRIKE_THROUGH_SPAN, 0, 0, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            listItemTextView.setTextColor(ContextCompat.getColor(context,R.color.item_active_color));
        }
    }

    private void addToListOfViewholders(OverviewViewHolder overviewViewHolder) {
        listOfViewholders.add(overviewViewHolder);
    }

    public void setInteractionDeletePossible(boolean isPossible){
        deleteInteractionIsPossible = isPossible;
        if(isPossible) {
            listOfViewholders.forEach(this::deleteInteractionPossible);
        }else {
            listOfViewholders.forEach(this::deteteInteractionImpossible);
        }
    }

    private void deleteInteractionPossible(OverviewViewHolder holder){
        holder.deleteBtn.setVisibility(View.VISIBLE);
        holder.deleteBtn.setEnabled(true);
        holder.deleteBtn.setColorFilter(Color.RED);
        holder.mainView.setEnabled(false);
        holder.mainView.setAlpha((float) 0.5);
    }

    private void deteteInteractionImpossible(OverviewViewHolder holder){
        holder.deleteBtn.setVisibility(View.INVISIBLE);
        holder.deleteBtn.setEnabled(false);
        holder.mainView.setEnabled(true);
        holder.mainView.setAlpha((float) 1.0);
    }

    public void setAlpha(float alpha){
        listOfViewholders.forEach(holder -> holder.mainView.setAlpha(alpha));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return listOfItems.size();
    }

    public List<T> getListOfItems(){
        return listOfItems;
    }

    public T getItem(int position){
        return listOfItems.get(position);
    }



    public void removeItem(int position){
        listOfItems.remove(position);
        notifyItemRemoved(position);
    }

    public void addItem(T listToAdd){
        listOfItems.add(listToAdd);
        notifyDataSetChanged();
    }



}

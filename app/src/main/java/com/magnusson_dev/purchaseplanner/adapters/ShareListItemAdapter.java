package com.magnusson_dev.purchaseplanner.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.R;

import java.util.List;


public class ShareListItemAdapter<T extends User> extends ArrayAdapter {

    private static final String TAG = "ShareListItemAdapter";
    private LayoutInflater layoutInflater;
    private int resource;
    private Context context;
    private List<T> users;

    public ShareListItemAdapter(@NonNull Context context, int resource, @NonNull List<T> users) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
        this.layoutInflater=LayoutInflater.from(context);
        this.users=users;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView==null){
            convertView = layoutInflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final User user = users.get(position);
        viewHolder.username_tv.setText(user.getUsername());
        return convertView;

    }

    @Override
    public int getCount() {
        return users.size();
    }

    private class ViewHolder{
        final TextView username_tv;

        ViewHolder(View v){
            username_tv = v.findViewById(R.id.shareListSharedWithItem_tv);
        }
    }

    public void addUser(final T user){
        this.users.add(user);
        notifyDataSetChanged();
    }

    public List<T> getUsers(){
        return this.users;
    }
}

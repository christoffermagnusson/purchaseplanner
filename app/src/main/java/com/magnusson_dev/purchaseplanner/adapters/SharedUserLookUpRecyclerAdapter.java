package com.magnusson_dev.purchaseplanner.adapters;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.ShareListAddHandler;
import com.magnusson_dev.purchaseplanner.ShareListRemoveHandler;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandler;
import com.magnusson_dev.purchaseplanner.storage.userstorage.UserHandlerFactory;
import com.magnusson_dev.purchaseplanner.utils.DrawableComparator;

import java.util.ArrayList;
import java.util.List;

/**
 * RecyclerAdapter for SharedUserLookUpActivity
 * @param <T>
 */
public class SharedUserLookUpRecyclerAdapter<T extends User> extends RecyclerView.Adapter<SharedUserLookUpRecyclerAdapter.SharedUserViewHolder>{


    /**
     * Holds the views to binded to the data
     */
    class SharedUserViewHolder extends RecyclerView.ViewHolder{
        final TextView usernameTv;
        final TextView givenNameTv;
        final TextView emailTv;
        final ImageButton addBtn;
        final CardView cardView;
        final View mainView;

        SharedUserViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            cardView = mainView.findViewById(R.id.sharedUsersCardview);
            usernameTv = mainView.findViewById(R.id.shared_rv_username_tv);
            givenNameTv = mainView.findViewById(R.id.shared_rv_givenName_tv);
            emailTv = mainView.findViewById(R.id.shared_rv_email_tv);
            addBtn = mainView.findViewById(R.id.shared_rv_add_btn);
        }
    }

    private int resource;
    private LayoutInflater inflater;
    private List<T> listOfUsers;
    private final Context context;
    private ICallback callback;
    private UserHandler userHandler;
    private ShareListAddHandler shareAddHandler;
    private ShareListRemoveHandler shareRemoveHandler;

    private static final String TAG = "SharedUserLookUpRecycle";

    private Drawable checkGreen;
    private Drawable clearRed;



    private List<SharedUserViewHolder> listOfViewholders = new ArrayList<>();


    public SharedUserLookUpRecyclerAdapter(Context context, @LayoutRes int resource, @NonNull List<T> listOfUsers){
        this.context=context;
        this.resource=resource;
        this.inflater=LayoutInflater.from(context);
        this.userHandler = UserHandlerFactory.getUserHandler(context);
        this.listOfUsers=listOfUsers;
    }

    public void setShareAddHandler(final ShareListAddHandler shareAddHandler){
        this.shareAddHandler=shareAddHandler;
    }

    public void setShareRemoveHandler(final ShareListRemoveHandler shareRemoveHandler){
        this.shareRemoveHandler=shareRemoveHandler;
    }

    @Override
    public SharedUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: creating viewholder");
        View sharedUserRecyclerItem = inflater.inflate(R.layout.activity_shared_user_lookup_recycler_item,parent,false);
        Log.d(TAG, "onCreateViewHolder: Class of View = "+sharedUserRecyclerItem.getClass());
        SharedUserViewHolder viewHolder = new SharedUserViewHolder(sharedUserRecyclerItem);
        addToListOfViewHolders(viewHolder);
        return viewHolder;
    }

    /**
     * Binds data to a specific viewholder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(SharedUserLookUpRecyclerAdapter.SharedUserViewHolder holder, int position) {
        final User user = listOfUsers.get(position);
        Log.d(TAG, "onBindViewHolder: Setting up user : "+user.getUsername());
        holder.usernameTv.setText(user.getUsername());
        holder.givenNameTv.setText(user.getGivenName());
        holder.emailTv.setText(user.getEmail());
        // init setup of addBtn
        setupAddBtn(holder.addBtn, user);
    }

    private void setupAddBtn(ImageButton addBtn, final User user) {
        if(checkGreen==null || clearRed==null) {
            checkGreen = context.getDrawable(R.drawable.ic_check_green);
            clearRed = context.getDrawable(R.drawable.ic_clear_red_24dp);
        }

        Drawable current = shareAddHandler.userExistsInList(user) ? clearRed : checkGreen;
        addBtn.setImageDrawable(current);

        addBtn.setOnClickListener(view -> {
            if(DrawableComparator.compareDrawable(addBtn.getDrawable(), checkGreen)){
                addBtn.setImageDrawable(clearRed);
                shareAddHandler.addUser(user);
                Toast.makeText(context, context.getString(R.string.shared_lookup_added_msg,
                        user.getUsername()),
                        Toast.LENGTH_LONG).
                        show();
            }else{
                addBtn.setImageDrawable(checkGreen);
                shareRemoveHandler.removeUser(user);
                Toast.makeText(context, context.getString(R.string.shared_lookup_removed_msg,
                        user.getUsername()),
                        Toast.LENGTH_LONG).
                        show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return listOfUsers.size();
    }

    private void addToListOfViewHolders(SharedUserViewHolder viewHolder){
        listOfViewholders.add(viewHolder);
    }

    public List<T> getListOfUsers(){ return listOfUsers;}

    public void setListOfUsers(List<T> listOfUsers){
        this.listOfUsers=listOfUsers;
    }

    public void addUser(T user){
        listOfUsers.add(user);
        //notifyItemInserted(listOfUsers.size()-1);
        notifyDataSetChanged();
    }

    public void clear(){
        listOfUsers.clear();
        notifyDataSetChanged();
    }




}

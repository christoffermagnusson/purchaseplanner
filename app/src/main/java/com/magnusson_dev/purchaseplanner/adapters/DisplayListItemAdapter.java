package com.magnusson_dev.purchaseplanner.adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;


import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.StorageAction;

import java.util.List;


public class DisplayListItemAdapter<T extends ListItem> extends ArrayAdapter {

    private static final String TAG = "DisplayListItemAdapter";
    private final int layoutResource;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<T> listOfItems;
    private StorageAction storageAction;


    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();



    public DisplayListItemAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<T> listItems) {
        super(context, resource);
        this.context = context;
        this.layoutResource = resource;
        this.layoutInflater = LayoutInflater.from(context);
        this.listOfItems = listItems;
    }

    public void setStorageActionCallback(StorageAction sa){
        this.storageAction=sa;
    }



    @SuppressWarnings("unchecked")
    public void addItem(T item){
        // goes further to storage everytime..
        listOfItems.add(item);
        List<ListItem> updatedList = (List<ListItem>) listOfItems;
        AdapterHelper.update(this, updatedList);
        notifyDataSetChanged();
    }






    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final ViewHolderDisplayItem dHolder;
                if(convertView==null){
                    convertView = layoutInflater.inflate(layoutResource,parent,false);
                    dHolder = new ViewHolderDisplayItem(convertView);
                    convertView.setTag(dHolder);
                }else{
                    dHolder = (ViewHolderDisplayItem) convertView.getTag();
                }
                final T dListItem = listOfItems.get(position);
                dHolder.listItemTextView.setText(dListItem.getName(), TextView.BufferType.SPANNABLE);
                dHolder.listItemCheckBox.setChecked(dListItem.isChecked());
                crossOutItem(dListItem.isChecked(), dHolder.listItemTextView);
                dHolder.listItemCheckBox.setOnClickListener(view -> {
                    dListItem.setChecked(dHolder.listItemCheckBox.isChecked());
                    crossOutItem(dHolder.listItemCheckBox.isChecked(),dHolder.listItemTextView);
                    storageAction.updateListToStorage((List<ListItem>) listOfItems);
                });
        return convertView;
    }

    private void crossOutItem(boolean checked, TextView listItemTextView) {
        Spannable spannable = (Spannable) listItemTextView.getText();
        int lengthToSpan = listItemTextView.getText().toString().length();

        if(checked){
            spannable.setSpan(STRIKE_THROUGH_SPAN, 0, lengthToSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            listItemTextView.setTextColor(ContextCompat.getColor(context,R.color.item_crossed_out_color));
        }else{
            spannable.setSpan(STRIKE_THROUGH_SPAN, 0, 0, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            listItemTextView.setTextColor(ContextCompat.getColor(context,R.color.secondary_text_material_dark));
        }
    }


    @Override
    public int getCount() {
        return listOfItems.size();
    }

    public List<T> getListOfItems(){return listOfItems;}


    /**
     * ViewHolder pattern
     * Used to not instantiate more views than is neccessarily
     * needed.
     */

    private class ViewHolderDisplayItem{
        final TextView listItemTextView;
        final CheckBox listItemCheckBox;

        ViewHolderDisplayItem(View view){
            listItemTextView =  view.findViewById(R.id.listItemText);
            listItemCheckBox =  view.findViewById(R.id.editListItemCheckbox);
        }
    }

    public void setAlpha(float alpha){
        // to implement if needed...
    }




}

package com.magnusson_dev.purchaseplanner.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.magnusson_dev.purchaseplanner.TouchHelperListener;

import java.util.Collections;


public class OverviewRecyclerItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private OverviewRecyclerAdapter adapter;
    private TouchHelperListener listener;

    public OverviewRecyclerItemTouchHelperCallback(OverviewRecyclerAdapter adapter, TouchHelperListener listener) {
        this.adapter = adapter;
        this.listener=listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE,
                ItemTouchHelper.RIGHT );
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        int fromPosition = viewHolder.getAdapterPosition();
        int toPosition = target.getAdapterPosition();
        if(fromPosition < toPosition){
            for(int i = fromPosition; i > toPosition; i++) {
                Collections.swap(adapter.getListOfItems(), i, i+1);
            }
            viewHolder.itemView.setAlpha(1.0f);
            target.itemView.setAlpha(1.0f);
        }
        else{
            for(int i = fromPosition; i < toPosition; i--){
                Collections.swap(adapter.getListOfItems(), i, i-1);
            }
            viewHolder.itemView.setAlpha(1.0f);
            target.itemView.setAlpha(1.0f);
        }

        adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder.getAdapterPosition());
    }

    @Override
    public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder current, RecyclerView.ViewHolder target) {
        target.itemView.setAlpha(0.5f);
        return super.canDropOver(recyclerView, current, target);
    }


}


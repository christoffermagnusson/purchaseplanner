package com.magnusson_dev.purchaseplanner.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.ICallback;
import com.magnusson_dev.purchaseplanner.ProgressUpdater;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class InvitationsRecyclerAdapter<T extends ListInvitation>
        extends RecyclerView.Adapter<InvitationsRecyclerAdapter.InvitaionViewHolder> {



    class InvitaionViewHolder extends RecyclerView.ViewHolder{
        final TextView listNameTv;
        final TextView listCreatedTv;
        final TextView senderTv;
        final Button acceptBtn;
        final Button declineBtn;

        InvitaionViewHolder(View view){
            super(view);
            listNameTv = view.findViewById(R.id.invitationListName_tv);
            listCreatedTv = view.findViewById(R.id.invitationListCreated_tv);
            senderTv = view.findViewById(R.id.invitationSender_tv);
            acceptBtn = view.findViewById(R.id.invitationAcceptBtn);
            declineBtn = view.findViewById(R.id.invitationDeclineBtn);
        }
    }


    private final Context context;
    private final int resource;
    private List<ListInvitation> invitations;
    private LayoutInflater inflater;
    private AWSInvitationService invitationService;
    private ProgressUpdater updater;

    private static final String TAG = "InvitationsRecyclerAdap";

    private List<InvitaionViewHolder> listOfViewHolders = new ArrayList<>();


    public InvitationsRecyclerAdapter(Context context,
                                      int resource,
                                      @NonNull List<ListInvitation> invitations,
                                      ICallback callback) {
        this.context=context;
        this.resource=resource;
        this.invitations=invitations;
        this.inflater=LayoutInflater.from(context);
        this.invitationService=AWSServiceFactory.getAWSInvitationsService(context,callback);
    }

    public void setProgressUpdater(ProgressUpdater updater){
        this.updater=updater;
    }

    @Override
    public InvitationsRecyclerAdapter.InvitaionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View invitationItem = inflater.inflate(resource, parent, false);
        InvitaionViewHolder holder = new InvitaionViewHolder(invitationItem);
        listOfViewHolders.add(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(InvitationsRecyclerAdapter.InvitaionViewHolder holder, int position) {
        ListInvitation invitation = invitations.get(position);

        String listNamePrefix = holder.listNameTv.getText().toString();
        String listCreatedPrefix = holder.listCreatedTv.getText().toString();
        String senderPrefix = holder.senderTv.getText().toString();
        holder.listNameTv.setText(String.format(Locale.UK, "%s%s",listNamePrefix,invitation.getListName()));
        holder.listCreatedTv.setText(String.format(Locale.UK, "%s%s",listCreatedPrefix,invitation.getTimeOfCreation()));
        holder.senderTv.setText(String.format(Locale.UK, "%s%s",senderPrefix,invitation.getSender()));

        holder.acceptBtn.setOnClickListener(onClick -> {
            invitation.setInviteHandled(true);
            // update invitation
            invitationService.acceptInvitation(invitation);
            updater.startProgress();
            // upload sharedlistidentifier
        });
        holder.declineBtn.setOnClickListener(onClick -> {
            invitation.setInviteHandled(true);
            // update invitation
            invitationService.declineInvitation(invitation);
            updater.startProgress();
            // update list and remove this user
        });
    }

    @Override
    public int getItemCount() {
        return invitations.size();
    }


    public void removeInvitation(ListInvitation invitation){
        invitations.removeIf(inv -> inv.getId() == invitation.getId());
        notifyItemRemoved(invitations.indexOf(invitation));
    }

    public boolean invitesLeft(){
        return invitations.size()>0;
    }

}

package com.magnusson_dev.purchaseplanner.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.magnusson_dev.purchaseplanner.model.ListCategory;
import com.magnusson_dev.purchaseplanner.model.User;
import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.ShareListAddHandler;
import com.magnusson_dev.purchaseplanner.utils.DrawableComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class ShareListSearchUserRecyclerAdapter<T extends User> extends RecyclerView.Adapter<ShareListSearchUserRecyclerAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView username_tv;
        final TextView givenName_tv;
        final TextView email_tv;
        final ConstraintLayout container;
        final ImageButton addBtn;
        final ImageButton hideBtn;

        ViewHolder(View v){
            super(v);
            username_tv = v.findViewById(R.id.shareListSearch_rvItem_username_tv);
            givenName_tv = v.findViewById(R.id.shareListSearch_rvItem_givenName_tv);
            email_tv = v.findViewById(R.id.shareListSearc_rvItem_email_tv);
            container = v.findViewById(R.id.shareListSearch_rvItem_container);
            addBtn = v.findViewById(R.id.shareListSearch_rvItem_addBtn);
            hideBtn = v.findViewById(R.id.shareListSearch_rvItem_hideBtn);
        }
    }

    private final int layoutResource;
    private final LayoutInflater inflater;
    private List<T> users;
    private final Context context;
    private ListCategory category;
    private ShareListAddHandler handler;

    private static final String TAG = "ShareListSearchUserRecy";

    private List<ViewHolder> listOfViewHolders = new ArrayList<>();

    public ShareListSearchUserRecyclerAdapter(Context context, @LayoutRes int layoutResource, @NonNull List<T> users){
        this.layoutResource=layoutResource;
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.users=users;
    }

    public void setTheme(ListCategory category){
        this.category=category;
    }

    public void setShareListHandler(ShareListAddHandler handler){
        this.handler=handler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View recyclerItem = inflater.inflate(R.layout.activity_share_list_search_recycler_item, parent, false);
        ViewHolder holder = new ViewHolder(recyclerItem);
        listOfViewHolders.add(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShareListSearchUserRecyclerAdapter.ViewHolder holder, int position) {
        final User user = users.get(position);
        holder.username_tv.setText(user.getUsername());
        holder.givenName_tv.setText(user.getGivenName());
        holder.email_tv.setText(user.getEmail());

        hideHolderElements(holder);
        holder.hideBtn.setImageDrawable(context.getDrawable(R.drawable.ic_arrow_drop_down_light_24dp));
        holder.hideBtn.setOnClickListener(view ->{
            if(DrawableComparator.compareDrawable(holder.hideBtn.getDrawable(),context.getDrawable(R.drawable.ic_arrow_drop_down_light_24dp))){
                showHolderElements(holder);
            }else {
                hideHolderElements(holder);
            }
        });

        setAddBtn(holder.addBtn);
        holder.addBtn.setOnClickListener(view -> handler.addUser(user));
    }

    private void setAddBtn(ImageButton addBtn) {
        switch(category){
            case SHOPPING:
                addBtn.setImageDrawable(context.getDrawable(R.drawable.ic_add_circle_shopping));
                break;
            case TODO:
                addBtn.setImageDrawable(context.getDrawable(R.drawable.ic_add_circle_todo));
                break;
            case WISHLIST:
                addBtn.setImageDrawable(context.getDrawable(R.drawable.ic_add_circle_wishlist));
                break;
            default:
                addBtn.setImageDrawable(context.getDrawable(R.drawable.ic_add_circle));
                break;
        }
    }

    private void showHolderElements(ViewHolder holder) {
        holder.givenName_tv.setVisibility(View.VISIBLE);
        holder.email_tv.setVisibility(View.VISIBLE);
        holder.hideBtn.setImageDrawable(context.getDrawable(R.drawable.ic_arrow_drop_up_light_24dp));
    }

    private void hideHolderElements(ViewHolder holder) {
        holder.givenName_tv.setVisibility(View.GONE);
        holder.email_tv.setVisibility(View.GONE);
        holder.hideBtn.setImageDrawable(context.getDrawable(R.drawable.ic_arrow_drop_down_light_24dp));
    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    public void clear(){
        users.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<T> toAdd){
        users.addAll(toAdd);
        users.sort(Comparator.comparing(User::getUsername));
        notifyDataSetChanged();
    }
}

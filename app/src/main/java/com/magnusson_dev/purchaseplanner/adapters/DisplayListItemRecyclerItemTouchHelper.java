package com.magnusson_dev.purchaseplanner.adapters;

import android.graphics.Canvas;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.magnusson_dev.purchaseplanner.TouchHelperListener;


public class DisplayListItemRecyclerItemTouchHelper extends ItemTouchHelper.Callback {

    private static final String TAG = "DisplayListItemRecycler";
    private TouchHelperListener listener;

    public DisplayListItemRecyclerItemTouchHelper(TouchHelperListener listener){
        this.listener=listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE,
                        ItemTouchHelper.RIGHT);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        Log.d(TAG, "onMove: Moving!");
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        Log.d(TAG, "onSwiped: DIRECTION: "+direction);
        listener.onSwiped(viewHolder.getAdapterPosition());
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if(viewHolder != null){
            getDefaultUIUtil().onSelected(((DisplayListItemRecyclerAdapter.DisplayListViewHolder) viewHolder).getForeground());
        }
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        ConstraintLayout toSwipe = ((DisplayListItemRecyclerAdapter.DisplayListViewHolder) viewHolder).getForeground();
        getDefaultUIUtil().clearView(toSwipe);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        ConstraintLayout toSwipe = ((DisplayListItemRecyclerAdapter.DisplayListViewHolder) viewHolder).getForeground();
        getDefaultUIUtil().onDraw(c, recyclerView, toSwipe, dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                float dX, float dY, int actionState, boolean isCurrentlyActive) {
        ConstraintLayout toSwipe = ((DisplayListItemRecyclerAdapter.DisplayListViewHolder) viewHolder).getForeground();


        getDefaultUIUtil().onDrawOver(c,
                                    recyclerView,
                                    toSwipe,
                                    dX,
                                    dY,
                                    actionState,
                                    isCurrentlyActive);



    }
}

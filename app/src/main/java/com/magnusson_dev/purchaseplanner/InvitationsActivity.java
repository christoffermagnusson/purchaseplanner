package com.magnusson_dev.purchaseplanner;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.AWS.services.AWSInvitationService;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.awsdynamodb.SharedListIdentifier;
import com.magnusson_dev.purchaseplanner.adapters.InvitationsRecyclerAdapter;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationAcceptedEvent;
import com.magnusson_dev.purchaseplanner.event.DynamoDBInvitationDeclinedEvent;
import com.magnusson_dev.purchaseplanner.event.EventHandler;
import com.magnusson_dev.purchaseplanner.event.EventName;
import com.magnusson_dev.purchaseplanner.model.ListInvitation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Locale;
import java.util.function.Function;


public class InvitationsActivity extends AppCompatActivity implements ICallback, ProgressUpdater {


    private static final String TAG = "InvitationsActivity";

    private RecyclerView invitationsList;
    private ProgressBar invitationProgress;
    private ConstraintLayout invitationListHolder;
    private InvitationsRecyclerAdapter recyclerAdapter;
    private TextView noInvitesLeftMsgTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitations);
        invitationProgress = (ProgressBar) findViewById(R.id.invitationProgress);
        invitationListHolder = (ConstraintLayout) findViewById(R.id.invitationListHolder);
        noInvitesLeftMsgTv = (TextView) findViewById(R.id.invitationsNoInvitesMsg_tv);
        setupRecyclerView();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    private void setupRecyclerView() {
        invitationsList = (RecyclerView) findViewById(R.id.invitationsList);
        invitationsList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerAdapter = new InvitationsRecyclerAdapter(getApplicationContext()
                , R.layout.invitations_item_new
                , new ArrayList<>(InvitationsHelper.downloadedInvitations)
                , this);
        recyclerAdapter.setProgressUpdater(this);
        invitationsList.setAdapter(recyclerAdapter);
        displayMessage(recyclerAdapter.invitesLeft());
    }


    private void displayMessage(boolean invitesLeft) {
        int visibility = invitesLeft ? View.GONE : View.VISIBLE;
        noInvitesLeftMsgTv.setVisibility(visibility);
    }


    @Subscribe
    public void onEvent(AWSEvent event){
        EventHandler.Handler handler = defaultHandler -> Log.d(TAG, "onEvent: No eventhandler attached");

        switch(event.getClass().getSimpleName()){
            case EventName.DYNAMODB_INVITATION_ACCEPTED_EVENT:
                DynamoDBInvitationAcceptedEvent invitationAcceptedEvent = (DynamoDBInvitationAcceptedEvent) event;
                hideProgress();
                handler = getInvitationAcceptedHandler(invitationAcceptedEvent);
                break;
            case EventName.DYNAMODB_INVITATION_DECLINED_EVENT:
                DynamoDBInvitationDeclinedEvent invitationDeclinedEvent = (DynamoDBInvitationDeclinedEvent) event;
                hideProgress();
                handler = getInvitationDeclinedEvent(invitationDeclinedEvent);
                break;
        }

        event.setHandler(handler);
        EventHandler.handle(event);
    }



    private EventHandler.Handler getInvitationAcceptedHandler(DynamoDBInvitationAcceptedEvent invitationAcceptedEvent) {
        return invitationAcceptedEvent.isSuccess()
                ? handler -> {
                    AWSInvitationService invitationService = AWSServiceFactory.getAWSInvitationsService(getApplicationContext(), this);
                    SharedListIdentifier identifier = new SharedListIdentifier(invitationAcceptedEvent
                            .getAcceptedInvitation()
                            .getReceiver());
                    identifier.setListId(invitationAcceptedEvent.getAcceptedInvitation().getId());
                    invitationService.uploadIdentifier(identifier);
                    // SharedListIdentifier uploader service
                    Toast.makeText(getApplicationContext(), getString(R.string.invitations_invite_accepted), Toast.LENGTH_SHORT).show();
                    recyclerAdapter.removeInvitation(invitationAcceptedEvent.getAcceptedInvitation());
                    displayMessage(recyclerAdapter.invitesLeft());
                }
                : handler -> {
                    Log.d(TAG, "getInvitationAcceptedHandler: Couldn't handle invitation");
                    Toast.makeText(getApplicationContext(),
                            invitationAcceptedEvent.getMessage(),
                            Toast.LENGTH_LONG).
                            show();
                };
    }


    private EventHandler.Handler getInvitationDeclinedEvent(DynamoDBInvitationDeclinedEvent invitationDeclinedEvent) {
        return invitationDeclinedEvent.isSuccess()
                ? handler -> {
                    Log.d(TAG, "getInvitationDeclinedEvent: Declined invitation"); // to be handled by lambda function
                    recyclerAdapter.removeInvitation(invitationDeclinedEvent.getDeclinedInvitation());
                    Toast.makeText(getApplicationContext(), getString(R.string.invitations_invite_declined), Toast.LENGTH_SHORT).show();
                    displayMessage(recyclerAdapter.invitesLeft());
                }
                : handler -> {
                    Log.d(TAG, "getInvitationDeclinedEvent: Couldn't decline invitation");
                    Toast.makeText(getApplicationContext(),
                            invitationDeclinedEvent.getMessage(),
                            Toast.LENGTH_LONG).
                            show();
                };

    }

    @Override
    public void onStartNewActivity(ActivityType activityType, String intentMessage, String category) {

    }

    @Override
    public Context onRefreshContext() {
        return getApplicationContext();
    }

    @Override
    public void startProgress() {
        invitationProgress.setVisibility(View.VISIBLE);
        invitationListHolder.setAlpha(0.5f);
    }

    private void hideProgress(){
        invitationProgress.setVisibility(View.GONE);
        invitationListHolder.setAlpha(1.0f);
    }
}

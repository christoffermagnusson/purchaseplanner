package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.R;

public class ActivationCodeValidator implements Validator {

    private static final int VALID_CODE_LENGTH = 6;
    private final Context context;

    ActivationCodeValidator(final Context context){
        this.context=context;
    }

    @Override
    public boolean validate(String... strings) {
        String validationCode = strings[0];
        if(validationCode.equalsIgnoreCase("")){
            Toast activationCodeNotEnteredToast = Toast.makeText(context,
                    context.getString(R.string.confirm_registration_activation_code_not_entered),
                    Toast.LENGTH_LONG);
            activationCodeNotEnteredToast.show();
            return false;
        }else if(validationCode.length()<VALID_CODE_LENGTH){
            Toast activationCodeTooShortToast = Toast.makeText(context,
                    context.getString(R.string.confirm_registration_activation_code_too_short),
                    Toast.LENGTH_LONG);
            activationCodeTooShortToast.show();
            return false;
        }else if(validationCode.length()>VALID_CODE_LENGTH){
            Toast activationCodeTooLongToast = Toast.makeText(context,
                    context.getString(R.string.confirm_registration_activation_code_too_long),
                    Toast.LENGTH_LONG);
            activationCodeTooLongToast.show();
            return false;
        }
        return true;
    }

}

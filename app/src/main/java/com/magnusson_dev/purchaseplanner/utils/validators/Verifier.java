package com.magnusson_dev.purchaseplanner.utils.validators;


public interface Verifier {

    boolean verify(String toVerify);
}

package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.R;

public class GivennameValidator implements Validator {

    private final Context context;

    GivennameValidator(final Context context){
        this.context=context;
    }

    @Override
    public boolean validate(String... strings) {
        String givenname = strings[0];
        if(givenname.equalsIgnoreCase("")){
            Toast noGivennameToast = Toast.makeText(context,
                    context.getString(R.string.registration_given_name_not_entered),
                    Toast.LENGTH_SHORT);
            noGivennameToast.show();
            return false;
        }
        return true;
    }
}

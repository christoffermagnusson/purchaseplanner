package com.magnusson_dev.purchaseplanner.utils;



public class SharedPreferencesNullValueException extends Exception {


    public SharedPreferencesNullValueException(final String msg){
        super(msg);
    }

}

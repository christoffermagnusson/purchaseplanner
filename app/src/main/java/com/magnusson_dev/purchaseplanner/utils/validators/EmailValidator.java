package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.R;

public class EmailValidator implements Validator {

    private final Context context;
    private static final String TAG = "EmailValidator";

    EmailValidator(final Context context){
        this.context=context;
    }

    @Override
    public boolean validate(String... strings) {
        String email = strings[0];
        if(TextUtils.isEmpty(email)) {
            Toast noEmailToast = Toast.makeText(context,
                    context.getString(R.string.registration_email_not_entered),
                    Toast.LENGTH_LONG);
            noEmailToast.show();
            return false;
        } else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Log.d(TAG, "validate: Email not conforming to standards");
            Toast emailNotConformingToast = Toast.makeText(context,
                    context.getString(R.string.registration_email_not_conforming),
                    Toast.LENGTH_LONG);
            emailNotConformingToast.show();
            return false;
        }
        return true;
    }


}

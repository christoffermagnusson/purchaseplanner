package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.R;

public class PasswordValidator implements Validator {

    private final Context context;

    PasswordValidator(final Context context){
        this.context=context;
    }

    /** Prevalidate password so that no request is sent if there is valid password.
     *
     *  @return true if there is a password 8 characters or longer.
     */
    @Override
    public boolean validate(String... strings) {
        String password = strings[0];
        if(password.equals("")){
            Toast passwordCannotBeEmptyToast = Toast.makeText(context,
                    context.getString(R.string.login_password_cannot_be_empty_toast),
                    Toast.LENGTH_SHORT);
            passwordCannotBeEmptyToast.show();
            return false;
        }else if(password.length()<8){
            Toast passwordNotLongEnoughToast = Toast.makeText(context,
                    context.getString(R.string.login_password_not_long_enough_toast),
                    Toast.LENGTH_LONG);
            passwordNotLongEnoughToast.show();
            return false;
        }
        return true;
    }
}

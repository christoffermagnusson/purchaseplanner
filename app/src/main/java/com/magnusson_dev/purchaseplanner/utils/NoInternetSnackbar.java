package com.magnusson_dev.purchaseplanner.utils;


import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.magnusson_dev.purchaseplanner.R;

/**
 * Wrapper class for instantiating a Snackbar at chosen
 * view with preset message "No internet connection"
 */
public class NoInternetSnackbar {


    static Snackbar noInternetSnackbar;



    public static void initialize(final Context context, final View view){
        noInternetSnackbar = Snackbar.make(view, context.getString(R.string.main_activity_no_internet_snackbar), Snackbar.LENGTH_INDEFINITE);
        noInternetSnackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.snackbar_color));
    }

    public static void show(){
        noInternetSnackbar.show();
    }

    public static void dismiss(){
        noInternetSnackbar.dismiss();
    }

}

package com.magnusson_dev.purchaseplanner.utils;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Compares drawables using Bitmap representations. Used as static class
 * for optimization.
 */
public class DrawableComparator {

    static Bitmap b1;
    static Bitmap b2;

    /**
     * Compares to Drawable resources. Useful when switching between to different symbols
     * for a button.
     * @param d1
     * @param d2
     * @return
     */
    public static boolean compareDrawable(Drawable d1, Drawable d2){
        b1 = convertDrawableToBitmap(d1);
        b2 = convertDrawableToBitmap(d2);
        return b1.sameAs(b2);
    }

    /**
     * Converts a Drawable to a Bitmap representation, in order to
     * more easily and reliantly compare.
     * @param drawable
     * @return
     */
    private static Bitmap convertDrawableToBitmap(Drawable drawable){
        Bitmap bitmap = null;
        if(drawable instanceof BitmapDrawable){
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null){
                return  bitmapDrawable.getBitmap();
            }
        }

        bitmap = (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0)
                        ? Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
                        : Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0,0,canvas.getWidth(),canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}

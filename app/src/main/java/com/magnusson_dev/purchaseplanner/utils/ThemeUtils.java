package com.magnusson_dev.purchaseplanner.utils;


import com.magnusson_dev.purchaseplanner.R;
import com.magnusson_dev.purchaseplanner.model.ListCategory;

public class ThemeUtils {

    private static final int DEFAULT_THEME = R.style.AppTheme;
    private static final int BRIGHT_THEME = R.style.AppThemeBright;
    public static int CURRENT_THEME;


    public static int backgroundColor(){
        switch(CURRENT_THEME){
            case BRIGHT_THEME:
                return R.color.colorPrimary_bright;
            default:
                return R.color.cardview_dark_background;
        }
    }

    public static int shoppingTheme(){
        switch(CURRENT_THEME){
            case BRIGHT_THEME:
                return R.style.ShoppingThemeBright;
            default:
                return R.style.ShoppingTheme;
        }
    }

    public static int shoppingColor(){
        switch (CURRENT_THEME){
            case BRIGHT_THEME:
                return R.color.shopping_color_bright;
            default:
                return R.color.shopping_color;
        }
    }

    public static int todoTheme(){
        switch(CURRENT_THEME){
            case BRIGHT_THEME:
                return R.style.TodoThemeBright;
            default:
                return R.style.TodoTheme;
        }
    }

    public static int todoColor(){
        switch (CURRENT_THEME){
            case BRIGHT_THEME:
                return R.color.todo_color_bright;
            default:
                return R.color.todo_color;
        }
    }

    public static int wishlistTheme(){
        switch(CURRENT_THEME){
            case BRIGHT_THEME:
                return R.style.WishlistThemeBright;
            default:
                return R.style.WishlistTheme;
        }
    }

    public static int wishlistColor(){
        switch (CURRENT_THEME){
            case BRIGHT_THEME:
                return R.color.wishlist_color_bright;
            default:
                return R.color.wishlist_color;
        }
    }


    public static int currentThemeMainColor(ListCategory category){
        switch(category){
            case SHOPPING:
                return shoppingColor();
            case TODO:
                return todoColor();
            case WISHLIST:
                return wishlistColor();
            default:
                return 0;
        }
    }

}

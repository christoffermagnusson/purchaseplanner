package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;
import android.widget.Toast;

import com.magnusson_dev.purchaseplanner.R;

public class UsernameValidator implements Validator {

    private final Context context;

    UsernameValidator(final Context context){
        this.context=context;
    }


    @Override
    public boolean validate(String... strings) {
        String username = strings[0];
        if(username.equalsIgnoreCase("")){
            Toast noUsernameToast = Toast.makeText(context,
                    context.getString(R.string.registration_username_not_entered),
                    Toast.LENGTH_SHORT);
            noUsernameToast.show();
            return false;
        }
        return true;

    }


}

package com.magnusson_dev.purchaseplanner.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSServiceFactory;
import com.magnusson_dev.purchaseplanner.AWS.services.AWSUserService;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoCredentialsProviderCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoSession;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserPoolCreator;
import com.magnusson_dev.purchaseplanner.AWS.services.cognito.CognitoUserSessionCreator;
import com.magnusson_dev.purchaseplanner.event.AWSEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoCredentialsProviderCreatedEvent;
import com.magnusson_dev.purchaseplanner.event.CognitoUserSessionCreatedEvent;
import com.magnusson_dev.purchaseplanner.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class NetworkStateChecker {


    private static boolean networkStateChecked;
    private static final String TAG = "NetworkStateChecker";

    /**
     * Checking if there is a working internet connection. Displays a Toast
     * the first time this happens not to irritate the user.
     * @param context
     * @return
     */
    public static boolean isNetworkingEnabled(final Context context){
        Log.d(TAG, "isNetworkingEnabled: networkStateChecked already checked : "+networkStateChecked);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo==null && !networkStateChecked){
            displayNetworkNotFoundMsg(context);
            networkStateChecked = true;
            return false;
        }else if(networkInfo!=null && !networkInfo.isConnected() && !networkStateChecked){
            displayNetworkNotConnectedMsg(context);
            networkStateChecked = true;
            return false;
        }else if(networkInfo != null && networkInfo.isConnected()){
            networkStateChecked = false;
            initiateSession(context);
            return true;
        }
        Log.d(TAG, "isNetworkingEnabled: No internet connection available. Defaulting to offline behaviour");
        return false;
    }



    private static void displayNetworkNotFoundMsg(final Context context){
        Toast networkNotFoundToast = Toast.makeText(context, context.getString(R.string.network_state_checker_not_found), Toast.LENGTH_LONG);
        networkNotFoundToast.show();
    }

    private static void displayNetworkNotConnectedMsg(final Context context){
        Toast networkNotConnectedToast = Toast.makeText(context, context.getString(R.string.network_state_checker_not_connectet), Toast.LENGTH_LONG);
        networkNotConnectedToast.show();
    }

    /**
     * Initiates a session if the app is started in offline mode and
     * therefore has no instantiated session or cognito provider,
     * thus enabling online functionality
     * @param context
     */
    private static void initiateSession(final Context context){
        EventBus.getDefault().register(new Object(){
            @Subscribe
            public void onEvent(AWSEvent event) {
                if (event.getClass().equals(CognitoUserSessionCreatedEvent.class)) {
                    CognitoUserSessionCreatedEvent userSessionCreatedEvent = (CognitoUserSessionCreatedEvent) event;
                    if (userSessionCreatedEvent.isSuccess()) {
                        Log.d(TAG, "onEvent: User session started");
                        // Check if CredentialsProvider is instantiated
                        if(CognitoSession.CREDENTIALS_PROVIDER==null){
                            CognitoCredentialsProviderCreator.createCognitoCredentialsProvider(context,CognitoSession.USER_SESSION);
                        }
                        // Check if user has been uploaded
                        if(!CognitoSession.CURRENT_USER_UPLOADED){
                            // If user has not yet been uploaded due to connection outage
                            // try here once again to finalize the process
                            AWSUserService userService = AWSServiceFactory.getAWSUserService();
                            userService.storeUser(context);
                        }
                    } else {
                        Log.d(TAG, "onEvent: " + userSessionCreatedEvent.getMessage());
                    }
                }
                else if(event.getClass().equals(CognitoCredentialsProviderCreatedEvent.class)){
                    CognitoCredentialsProviderCreatedEvent credentialsProviderCreatedEvent = (CognitoCredentialsProviderCreatedEvent) event;
                    if(credentialsProviderCreatedEvent.isSuccess()){
                        // Credentials are now instantiated
                        EventBus.getDefault().unregister(this);
                    }else{
                        Log.d(TAG, "onEvent: "+credentialsProviderCreatedEvent.getMessage());
                    }
                }

            }
        });
        if(CognitoSession.USER_SESSION==null){
            CognitoUserPool userPool = CognitoUserPoolCreator.createUserPool(context);
            CognitoUser currentUser = userPool.getCurrentUser();
            CognitoUserSessionCreator.createCognitoUserSessionAsync(currentUser);
        }
    }


}

package com.magnusson_dev.purchaseplanner.utils;



import com.magnusson_dev.purchaseplanner.R;

public class ColorUtils {

    public static int secondaryTextMaterialDark(){
        return R.color.secondary_text_material_dark;
    }

    public static int textCrossedOut(){
        return R.color.item_crossed_out_color;
    }

    public static int cardviewLightBackground(){
        return R.color.cardview_light_background;
    }

    public static int itemCrossedOutBackground(){ return  R.color.crossed_out_background;}

    public static int mainBackgroundDark(){ return R.color.cardview_dark_background;}

}

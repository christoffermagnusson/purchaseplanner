package com.magnusson_dev.purchaseplanner.utils;


import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    /**
     * TODO
     * Add a DateFormatter handler of sorts to handle different Locales.
     * Now only handles EU.
     */

    private static String currentDateFormat = null;
    private static final String TAG = "DateFormatter";

    public static void setCurrentDateFormat(String dateFormat){
        currentDateFormat=dateFormat;
    }

    public static Date parseToDate(String toParse){
        DateFormat df = null;
        try{
        df = new SimpleDateFormat(currentDateFormat, Locale.UK);
            return df.parse(toParse);
        }catch(NullPointerException npe){
            Log.d(TAG, "parseToDate: No currentDateFormat value, defaulting to date_format_eu");
            currentDateFormat = "dd/MM/yyyy HH:mm";
            return parseToDate(toParse);
        }catch(ParseException pe){
            Log.d(TAG, "parseToDate: Somethin went wrong when parsing..");
            Log.d(TAG, "parseToDate: "+pe.getMessage());
            Log.d(TAG, "parseToDate: Returning null");
            return null;
        }

    }

    public static String parseToString(Date toParse){
        DateFormat df = null;
        try{
            df = new SimpleDateFormat(currentDateFormat, Locale.UK);
            return df.format(toParse);
        }catch(NullPointerException npe){
            Log.d(TAG, "parseToString: No currentDateFormat value, defaulting to date_format_eu");
            currentDateFormat = "dd/MM/YYYY HH:mm";
            return parseToString(toParse);
        }
    }
}

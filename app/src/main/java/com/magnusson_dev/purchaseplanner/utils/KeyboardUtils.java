package com.magnusson_dev.purchaseplanner.utils;


import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {


    public static void hideKeyboard(AppCompatActivity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View currentFocus =  activity.getCurrentFocus();
        if(currentFocus==null){
            currentFocus = new View(activity);
        }
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

}

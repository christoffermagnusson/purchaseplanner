package com.magnusson_dev.purchaseplanner.utils;


public class IntentConstants {


    public static final String INTENT_MESSAGE = "MESSAGE";
    public static final String INTENT_CATEGORY = "CATEGORY";



    public static final String LIST_TAG = "LIST_TAG";
    public static final String LIST_ID = "LIST_ID";

    public static final String ACTION = "ACTION";
    public static final String FILEPATH = "FILEPATH";
    public static final String KEY = "KEY";

    public static final String ENDPOINT = "ENDPOINT";
    public static final String INITIAL_DOWNLOAD = "YES/NO";

    // COGNITO
    public static final String EMAIL = "EMAIL";
    public static final String USERNAME = "USERNAME";


    // DynamoDBUserModel
    public static final String DYNAMO_USERNAME = "USERNAME";
    public static final String DYNAMO_GIVEN_NAME = "GIVEN_NAME";
    public static final String DYNAMO_EMAIL = "EMAIL";

    //
    public static final String SHARED_LIST_USERNAME = "USERNAME";
    public static final String SHARED_LIST_ID = "0";


}

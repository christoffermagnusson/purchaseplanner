package com.magnusson_dev.purchaseplanner.utils.validators;


/**
 * Interface representing validation of text elements
 */
public interface Validator {

    /**
     * To validate specified elements of text and return a boolean
     * value depending on the validation criteriums
     * @param strings
     * @return
     */
    boolean validate(final String ... strings);
}

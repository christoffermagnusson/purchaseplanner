package com.magnusson_dev.purchaseplanner.utils.validators;


import android.content.Context;

public class ValidatorFactory {

    public static Validator createUsernameValidator(final Context context){
        return new UsernameValidator(context);
    }

    public static Validator createGivennameValidator(final Context context){
        return new GivennameValidator(context);
    }

    public static Validator createEmailValidator(final Context context){
        return new EmailValidator(context);
    }

    public static Validator createPasswordValidator(final Context context){
        return new PasswordValidator(context);
    }

    public static Validator createActivationCodeValidator(final Context context){
        return new ActivationCodeValidator(context);
    }
}

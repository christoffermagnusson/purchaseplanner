package com.magnusson_dev.purchaseplanner.utils;


import android.content.Context;
import android.content.SharedPreferences;

import com.magnusson_dev.purchaseplanner.R;

public class SharedPreferencesWrapper {

    private static String SHARED_PREFERENCES_KEY = "com.magnusson_dev.purchaseplanner.shared_preferences";

    public static void putString(final String key, final String valueToStore, final Context context){
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,valueToStore);
        editor.apply();
    }

    public static String getString(final String key, final Context context)throws SharedPreferencesNullValueException{
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        String value = preferences.getString(key, null);
        if(value==null){
            throw new SharedPreferencesNullValueException(context.getString(R.string.shared_preferences_no_value_found));
        }
        return value;
    }
}

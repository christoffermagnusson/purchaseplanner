package com.magnusson_dev.purchaseplanner;

import com.magnusson_dev.purchaseplanner.model.User;

public interface ShareListAddHandler {

    void addUser(User user);

    boolean userExistsInList(User user);

}

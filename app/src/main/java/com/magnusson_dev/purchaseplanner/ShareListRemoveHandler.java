package com.magnusson_dev.purchaseplanner;


import com.magnusson_dev.purchaseplanner.model.User;

public interface ShareListRemoveHandler {

    void removeUser(User user);

}

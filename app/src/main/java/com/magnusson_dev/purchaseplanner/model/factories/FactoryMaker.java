package com.magnusson_dev.purchaseplanner.model.factories;

/**
 * Maker of factories!
 */
public class FactoryMaker {


    public static AbstractListFactory getListFactory(){
        return new ListFactory();
    }

    public static AbstractListItemFactory getListItemFactory(){
        return new ListItemFactory();
    }

    public static AbstractUserFactory getUserFactory(){ return new UserFactory();}

}

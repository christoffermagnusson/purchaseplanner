package com.magnusson_dev.purchaseplanner.model;



public class WishListItem extends ListItem {

    public WishListItem(String name) {
        super(name);
    }

    public WishListItem(Builder builder) {
        super(builder);
    }
}

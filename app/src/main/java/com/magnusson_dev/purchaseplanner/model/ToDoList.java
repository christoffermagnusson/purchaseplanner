package com.magnusson_dev.purchaseplanner.model;



public class ToDoList extends BaseList {
    public ToDoList(String tag) {
        super(tag);
    }

    public ToDoList(String tag, String categoryStr) {
        super(tag, categoryStr);
    }

    public ToDoList(Builder builder) {
        super(builder);
    }
}

package com.magnusson_dev.purchaseplanner.model.factories;

import com.magnusson_dev.purchaseplanner.model.BaseList;
import com.magnusson_dev.purchaseplanner.model.ShoppingList;
import com.magnusson_dev.purchaseplanner.model.ToDoList;
import com.magnusson_dev.purchaseplanner.model.WishList;

public class ListFactory extends AbstractListFactory {

    @Override
    public BaseList createList(String tag,String category) {
        category = category.toUpperCase();
        BaseList list = null;
        switch(category){
            case "SHOPPING":
                list = new ShoppingList(tag,category);
            case "TODO":
                list = new ToDoList(tag,category);
            case "WISHLIST":
                list = new WishList(tag,category);
        }
        return list;
    }

    @Override
    public BaseList createQueryList(int id) {
        return new BaseList(id);
    }

    @Override
    public BaseList buildList(BaseList.Builder builder) {
        return builder.build();
    }


}

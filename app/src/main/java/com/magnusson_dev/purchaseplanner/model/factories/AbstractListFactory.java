package com.magnusson_dev.purchaseplanner.model.factories;

import com.magnusson_dev.purchaseplanner.model.BaseList;

public abstract class AbstractListFactory {

    /**
     * The basic create method for lists
     * @param tag
     * @param category
     * @return
     */
    public abstract BaseList createList(String tag,String category);

    /**
     * Creates a list solely to be used as a query object against DynamoDB
     * @param id
     * @return
     */
    public abstract BaseList createQueryList(int id);

    /**
     * Creates a list using the Builder pattern
     * @param builder
     * @return
     */
    public abstract BaseList buildList(BaseList.Builder builder);

}

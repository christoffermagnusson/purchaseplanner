package com.magnusson_dev.purchaseplanner.model.factories;

import com.magnusson_dev.purchaseplanner.model.ListItem;

public abstract class AbstractListItemFactory {

    public abstract ListItem createListItem(String category, String name);

    /**
     * Creates a listitem using the Builder pattern
     * @param builder
     * @param category
     * @return
     */
    public abstract ListItem buildListItem(ListItem.Builder builder, String category);

}

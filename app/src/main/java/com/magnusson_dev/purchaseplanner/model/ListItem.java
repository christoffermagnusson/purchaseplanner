package com.magnusson_dev.purchaseplanner.model;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBDocument;

/**
 * A basic ListItem class.
 */
@DynamoDBDocument
public class ListItem {


    private String name;
    private boolean isChecked;
    private String lastupdatedBy;

    public ListItem(){}

    public ListItem(String name){this.name=name;}

    public ListItem(String name, boolean isChecked){this.name=name; this.isChecked=isChecked;}

    public String getName() {return name;}

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){return this.name;}

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getLastupdatedBy() {
        return lastupdatedBy;
    }

    public void setLastupdatedBy(String lastupdatedBy) {
        this.lastupdatedBy = lastupdatedBy;
    }

    public static class Builder{
        private String name;
        private boolean isChecked;
        private String lastUpdatedBy;

        public Builder name(String name){
            this.name=name;
            return this;
        }

        public Builder isChecked(boolean isChecked){
            this.isChecked=isChecked;
            return this;
        }

        public Builder lastUpdatedBy(String lastUpdatedBy){
            this.lastUpdatedBy=lastUpdatedBy;
            return this;
        }

        public ListItem build(String category){
            ListItem item = null;
            switch(category.toUpperCase()){
                case "SHOPPING":
                    item = new ShoppingListItem(this);
                    break;
                case "TODO":
                    item = new ToDoListItem(this);
                    break;
                case "WISHLIST":
                    item = new WishListItem(this);
                    break;
                default:
                    item = new ListItem(this);
                    break;
            }
            return item;
        }
    }

    public ListItem(ListItem.Builder builder){
        this.name=builder.name;
        this.isChecked=builder.isChecked;
        this.lastupdatedBy=builder.lastUpdatedBy;
    }
}

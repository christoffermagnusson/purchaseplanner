package com.magnusson_dev.purchaseplanner.model;



public class ShoppingListItem extends ListItem {




    public ShoppingListItem(String name) {
        super(name);
    }

    public ShoppingListItem(Builder builder) {
        super(builder);
    }

    public String toString(){ return super.getName(); }
}

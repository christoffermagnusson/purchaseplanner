package com.magnusson_dev.purchaseplanner.model.factories;


import com.magnusson_dev.purchaseplanner.model.ListItem;
import com.magnusson_dev.purchaseplanner.model.ShoppingListItem;
import com.magnusson_dev.purchaseplanner.model.ToDoListItem;
import com.magnusson_dev.purchaseplanner.model.WishListItem;

public class ListItemFactory extends AbstractListItemFactory {
    @Override
    public ListItem createListItem(String category, String name) {
        category = category.toUpperCase();
        switch(category){
            case "SHOPPING":
                return new ShoppingListItem(name);
            case "TODO":
                return new ToDoListItem(name);
            case "WISHLIST":
                return new WishListItem(name);
        }

        return new ListItem(name);
    }

    @Override
    public ListItem buildListItem(ListItem.Builder builder, String category) {
        return builder.build(category);
    }


}

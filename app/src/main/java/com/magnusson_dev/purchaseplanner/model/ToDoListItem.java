package com.magnusson_dev.purchaseplanner.model;

public class ToDoListItem extends ListItem {
    public ToDoListItem(String name) {
        super(name);
    }

    public ToDoListItem(Builder builder) {
        super(builder);
    }
}

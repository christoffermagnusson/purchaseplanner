package com.magnusson_dev.purchaseplanner.model;



public class WishList extends BaseList {
    public WishList(String tag, String categoryStr) {
        super(tag, categoryStr);
    }

    public WishList(Builder builder) {
        super(builder);
    }
}

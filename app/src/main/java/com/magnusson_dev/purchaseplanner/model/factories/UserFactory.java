package com.magnusson_dev.purchaseplanner.model.factories;

import com.magnusson_dev.purchaseplanner.model.User;



public class UserFactory extends AbstractUserFactory {

    @Override
    public User createUser(String username) {
        return new User(username);
    }

    @Override
    public User buildUser(User.Builder builder) {
        return builder.build();
    }
}

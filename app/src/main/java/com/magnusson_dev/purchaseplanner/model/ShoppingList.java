package com.magnusson_dev.purchaseplanner.model;


public class ShoppingList extends BaseList{

    public ShoppingList(String tag){super(tag);}
    public ShoppingList(String tag, String categoryStr) {
        super(tag, categoryStr);
    }

    public ShoppingList(Builder builder) {
        super(builder);
    }
}

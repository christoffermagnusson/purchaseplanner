package com.magnusson_dev.purchaseplanner.model;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBDocument;

/**
 * Class representing a domain object of a user.
 */
@DynamoDBDocument
public class User {

    String username;
    String givenName;
    String email;

    public User(){}

    public User(final String username){
        this.username=username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.getUsername();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        else if(!obj.getClass().equals(User.class)){
            return false;
        }
        User that = (User) obj;
        if(this.getUsername().equals(that.getUsername())){
            return true;
        }

        return super.equals(obj);
    }

    public static class Builder{
        private String username;
        private String givenName;
        private String email;

        public Builder username(final String username){
            this.username=username;
            return this;
        }

        public Builder givenName(final String givenName){
            this.givenName=givenName;
            return this;
        }

        public Builder email(final String email){
            this.email=email;
            return this;
        }

        public User build(){
            return new User(this);
        }

    }

    public User(User.Builder builder){
        this.username=builder.username;
        this.givenName=builder.givenName;
        this.email=builder.email;
    }
}

package com.magnusson_dev.purchaseplanner.model;


import android.annotation.SuppressLint;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

@DynamoDBTable(tableName = "invites_Plnner")
public class ListInvitation {

    public ListInvitation(){} // need empty constructor for DynamoDB mapping

    public ListInvitation(String receiver){
        this.receiver=receiver;
    }

    private String listName;
    private String sender;
    private String receiver;
    private int id;
    private String timeOfCreation;
    private boolean inviteHandled;

    @DynamoDBAttribute(attributeName = "listName")
    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    @DynamoDBAttribute(attributeName = "sender")
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @DynamoDBAttribute(attributeName = "id")
    @DynamoDBRangeKey
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "timeOfCreation")
    public String getTimeOfCreation() {
        return timeOfCreation;
    }

    public void setTimeOfCreation(String timeOfCreation) {
        this.timeOfCreation = timeOfCreation;
    }

    @DynamoDBHashKey(attributeName = "receiver")
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @DynamoDBAttribute(attributeName = "inviteHandled")
    public boolean isInviteHandled() {
        return inviteHandled;
    }

    public void setInviteHandled(boolean inviteHandled) {
        this.inviteHandled = inviteHandled;
    }

    public static class Builder{

        private String listName;
        private String sender;
        private String receiver;
        private int id;
        private String timeOfCreation;
        private boolean inviteHandled;

        public Builder listName(final String listName){
            this.listName=listName;
            return this;
        }

        public Builder sender(final String sender){
            this.sender=sender;
            return this;
        }

        public Builder receiver(final String receiver){
            this.receiver=receiver;
            return this;
        }

        public Builder id(final int id){
            this.id=id;
            return this;
        }

        public Builder timeOfCreation(final String timeOfCreation){
            this.timeOfCreation=timeOfCreation;
            return this;
        }

        public Builder inviteHandled(final boolean inviteHandled){
            this.inviteHandled=inviteHandled;
            return this;
        }


        public ListInvitation build(){
            return new ListInvitation(this);
        }
    }


    public ListInvitation(Builder builder){
        this.listName=builder.listName;
        this.id=builder.id;
        this.receiver=builder.receiver;
        this.sender=builder.sender;
        this.timeOfCreation=builder.timeOfCreation;
        this.inviteHandled=builder.inviteHandled;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public String toString(){
        return String.format("Name: %s \nID: %d \nSender: %s \nCreated: %s ",this.listName, this.id,this.sender, this.timeOfCreation);
    }



}

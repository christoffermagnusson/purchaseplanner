package com.magnusson_dev.purchaseplanner.model;

import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIgnore;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;
import com.magnusson_dev.purchaseplanner.utils.DateFormatter;

import java.util.Date;
import java.util.List;

/**
 * A model object of a basic list. Is mapped against DynamoDB with annotations
 */
@DynamoDBTable(tableName = "lists_Plnner")
public class BaseList {

    private int id;
    private List<ListItem> listItems;
    private String dateCreated;
    private String dateUpdated;
    private String tag;
    private ListCategory category;
    private List<User> sharedUsers;
    private boolean invitesSent;

    private ListCategory categoryType;

    public enum DateType{
        DATE_CREATED,
        DATE_UPDATED
    }

    private static final String TAG = "BaseList";


    public BaseList(){}

    public BaseList(int id){this.id=id;}

    public BaseList(String tag){ this.tag=tag;}

    public BaseList(String tag, String categoryStr){
        this.tag=tag;
        setCategory(categoryStr);
    }

    @DynamoDBHashKey(attributeName = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @DynamoDBAttribute
    public List<ListItem> getListItems(){return  listItems;}


    public void setListItems(List<ListItem> listItems) {
        this.listItems = listItems;
    }

    @DynamoDBAttribute
    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated){
        this.dateCreated=dateCreated;
    }

    public void setFormatDateCreated(Date dateCreatedDateType) {
        this.dateCreated = DateFormatter.parseToString(dateCreatedDateType);
    }

    public Date getDateOriginalType(DateType dt){

            switch (dt) {
                case DATE_CREATED:
                    return DateFormatter.parseToDate(dateCreated);
                case DATE_UPDATED:
                    return DateFormatter.parseToDate(dateUpdated);
                default:
                    Log.d(TAG, "getDateOriginalType: No valid DateTypes passed, choose either DateType.DATE_CREATED or DateType.DATE_UPDATED");
                    return null;
            }
    }

    @DynamoDBAttribute
    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated){
        this.dateUpdated=dateUpdated;
    }

    public void setFormatDateUpdated(Date dateUpdatedDateType) {
        this.dateUpdated = DateFormatter.parseToString(dateUpdatedDateType);
    }

    @DynamoDBAttribute
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }



    @DynamoDBIgnore
    public ListCategory getCategoryType() {
        return category;
    }

    public void setCategoryType(ListCategory categoryType){
        this.categoryType=categoryType;
    }

    public void setCategory(String categoryStr) {
        ListCategory category = ListCategory.DEFAULT;
        categoryStr = categoryStr.toUpperCase();
        switch(categoryStr){
            case "SHOPPING":
                category = ListCategory.SHOPPING;
                break;
            case "TODO":
                category = ListCategory.TODO;
                break;
            case "WISHLIST":
                category = ListCategory.WISHLIST;
        }
        this.category = category;
    }

    @DynamoDBAttribute
    public String getCategory(){
        return category.toString();
    }

    @DynamoDBAttribute
    public List<User> getSharedUsers() {
        return sharedUsers;
    }

    public void setSharedUsers(List<User> sharedUsers) {
        this.sharedUsers = sharedUsers;
    }

    public boolean isInvitesSent() {
        return invitesSent;
    }

    @DynamoDBAttribute
    public void setInvitesSent(boolean invitesSent) {
        this.invitesSent = invitesSent;
    }

    public String toString(){
        return "{\"tag\":\""+this.tag+"\""+"\n \"category\":\""+this.category+"\"}";
    }


    /**
     * Trying out Builder pattern
     */
    public static class Builder{
        private String tag;
        private int id;
        private String category;
        private boolean invitesSent;
        private Date dateCreated;
        private Date dateUpdated;
        private List<ListItem> listItems;
        private List<User> users;

        public Builder tag(final String tag){
            this.tag=tag;
            return this;
        }

        public Builder id(final int id){
            this.id=id;
            return this;
        }

        public Builder category(final String category){
            this.category=category;
            return this;
        }

        public Builder invitesSent(final boolean invitesSent){
            this.invitesSent=invitesSent;
            return this;
        }

        public Builder dateCreated(final Date dateCreated){
            this.dateCreated=dateCreated;
            return this;
        }

        public Builder dateUpdated(final Date dateUpdated){
            this.dateUpdated=dateUpdated;
            return this;
        }

        public Builder listItems(final List<ListItem> listItems){
            this.listItems=listItems;
            return this;
        }

        public Builder users(final List<User> users){
            this.users=users;
            return this;
        }

        public BaseList build(){
            BaseList list = null;
            switch(category.toUpperCase()){
                case "SHOPPING":
                    list = new ShoppingList(this);
                    break;
                case "TODO":
                    list = new ToDoList(this);
                    break;
                case "WISHLIST":
                    list = new WishList(this);
                    break;
                default:
                    list = new BaseList(this);
            }
            return list;
        }

        public String exposeCategory(){
            return this.category;
        }
    }

    public BaseList(Builder builder){
        this.tag=builder.tag;
        this.id=builder.id;
        this.setCategory(builder.category);
        this.invitesSent=builder.invitesSent;
        this.setFormatDateCreated(builder.dateCreated);
        this.setFormatDateUpdated(builder.dateUpdated);
        this.listItems=builder.listItems;
        this.sharedUsers=builder.users;
    }
}

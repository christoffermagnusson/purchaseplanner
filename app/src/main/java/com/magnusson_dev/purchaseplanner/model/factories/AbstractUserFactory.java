package com.magnusson_dev.purchaseplanner.model.factories;


import com.magnusson_dev.purchaseplanner.model.User;

public abstract class AbstractUserFactory {

    public abstract User createUser(String username);

    public abstract User buildUser(User.Builder builder);
}
